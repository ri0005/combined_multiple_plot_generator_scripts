import matplotlib.pyplot as plt
import fastmesareader as fmr

plotting_data = 'star_age radius_cm star_mass model_number v_surf  time_step_sec surf_escape_v' #String data columns names input with space delimeter 
# plotting_data = 'star_age,v_surf,surf_escape_v'               								#String data columns names input with comma delimeter
# plotting_data = 'star_age,v_surf,surf_escape_v'               								#String data columns names input with space and comma delimeter
# plotting_data = 'star_age:v_surf:surf_escape_v'	
# plotting_data = ['star_age', 'v_surf', 'surf_escape_v']										#list data columns names input 
# plotting_data = ("star_age", "v_surf", "surf_escape_v")										#tuple data columns names input 
# Only use of the the tyes above.

a = fmr.FastMesaReader('LOGS/history.data', plotting_data)
# Path here is an example, change to wherever your history.data or profilexx.data data file is.

fig = plt.figure()
ax = fig.add_subplot(111)


ax.plot(a.star_age, a.v_surf, linewidth='1', label='Surface Velocity')
ax.plot(a.star_age, a.surf_escape_v, linewidth='1', label='Escape Velocity')

ax.set_xlabel('Star Age (day)')
ax.set_ylabel('velocity (cm/s)')
ax.legend()

plt.show()