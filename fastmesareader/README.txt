__fastmesareader__ 
The fastmesareader script is designed for reading history.data and profile.data files quickly as the whole data files are not loaded into memory at once. Instead, the data files are parsed line by line, and only the relevant columns of information are saved, making the reader tool very effcient and fast.

Use Instructions: 
fastmesareader reads a data file by the user calling an instance of the FastMesaReader object. In the instance call, two pieces of information are required: the path to the data file, and the data column names to be read. An example is below, in which an instance call 'a' is being made to read a history.data file in the LOGS directory, and the star_age and star_mass columns will be saved to memory as instance attributes. If the data columns names are declared as a string, then the delimiters supported in separating the data columns names are: ', . - : ; |' as well as just a space. If a delimiter is used instead of a space in a string declaration, a space separating the column names and the delimiters is also supported. The data column names can also be declared as a list or as a tuple.
a = fastmesareader.FastMesaReader('LOGS/history.data', 'star_age star_mass')
The instance attributes star_age and star_mass can then be retrieved as arrays by using 'a.star_age' and 'a.star_mass'.

To Do:
Find a better way of detecting delimiters, current solution is not very elegant.
Make it an installable package.
