from os.path import join
import numpy as np
from collections import defaultdict

class FastMesaReader:

    def __init__(self, file_name=join('.', 'LOGS', 'history.data'),
                 plot_columns='star_age star_mass'):

        self.file_name = file_name
        self.plot_columns = plot_columns
        self.header_names = None
        self.header_vals = None
        self.header_data = None
        self.bulk_names = None
        self.col_numb = None
        self.bulk_data = None
        self.read_model_data()


    def read_model_data(self):
        #def pythonize_number(num_string):  Has been removed as obsolete, all vals now have E+/- format in mesa 
        self.plot_columns_names = []
        self.header_names = []
        self.header_vals = []
        self.header_data = {}

        self.bulk_numbers = []
        self.bulk_names = []
        self.col_numb = {}
        self.bulk_data = {}
        temp_data = []

        if isinstance(self.plot_columns,str):
            if ',' in self.plot_columns:
                self.plot_columns_names = self.plot_columns.split(',')  #The inputted "plot_columns" string is split into its constituent parts
            elif '-' in self.plot_columns:
                self.plot_columns_names = self.plot_columns.split('-') 
            elif ':' in self.plot_columns:
                self.plot_columns_names = self.plot_columns.split(':') 
            elif ';' in self.plot_columns:
                self.plot_columns_names = self.plot_columns.split(';') 
            elif '.' in self.plot_columns:
                self.plot_columns_names = self.plot_columns.split('.') 
            elif '|' in self.plot_columns:
                self.plot_columns_names = self.plot_columns.split('|')  
            else:
                self.plot_columns_names = self.plot_columns.split()
        elif isinstance(self.plot_columns,tuple):
            self.plot_columns_names = list(self.plot_columns)
        else:
            self.plot_columns_names = self.plot_columns
            # If self.plot_columns is a list then it is already in the format required so no need to do anything.
        self.plot_columns_names = [x.strip(' ') for x in self.plot_columns_names]  #remove any spaces in the plot_columns_names list elements
        self.plot_columns_names = list(dict.fromkeys(self.plot_columns_names)) #remove repeats

        #header stuff
        file = open(self.file_name, 'r')                   #open log file 
        file.readline()                                    #line 1 is just numbers, don't need
        self.header_names = file.readline().split()        #line 2 log file header names 
        self.header_vals = file.readline().split()         #line 3 log file header values 
        self.header_data = dict(zip(self.header_names, self.header_vals))  #make a dictionary containg header info
        file.readline()                                    #line 4 blank
        self.bulk_numbers = file.readline().split()        #line 5 data column numbers
        self.bulk_names = file.readline().split()          #line 6 data column headers

        for name in self.plot_columns_names:    #loop to make sure atributes inputted by the user to plot exist, 
            if name not in self.bulk_names:     #otherwise atribute error is flagged early on
                raise AttributeError(name)

        for i, name in enumerate(self.bulk_names):                 #Find the column numbers of the "plot_columns" inputted
            if name in self.plot_columns_names:                    #by user, -1 as python counts from zero
                self.col_numb[name] = int(self.bulk_numbers[i])-1   
                self.bulk_data[name] = []         #assigns key of dictionary as plot_column value input by 
                                                  #user, and assigns value of the key as a list to store data columns in
        for line in file:                         #loads each line of data file into memory one at a time 
            temp_data = line.split()              #splits line into constituent data column values
            for name in self.plot_columns_names:  #for each of the inputted "plot_column" names, retrieve data from line 
                self.bulk_data[name].append(float(temp_data[self.col_numb[name]])) #once correct index found append to dict lists

        file.close()    #finished reading file, not necessary but good practice 

    def __getattr__(self, method_name):
        if self.in_header(method_name):
            return self.header(method_name)
        elif self.in_data(method_name):
            return self.data(method_name)
        else:
            raise AttributeError(method_name)

    def in_header(self, key):
        return key in self.header_names

    def header(self, key):
        return self.header_data[key]

    def in_data(self, key):
        return key in self.bulk_names

    def data(self, key):
        return np.asarray(self.bulk_data[key])  #convert the dictionary list "values" into 1D numpy arrays to return
        # return self.bulk_data[key]