import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
from matplotlib.lines import Line2D
import fastmesareader as fmr

plotting_tings = 'ejected_h1 ejected_h2 ejected_h3 ejected_he3 ejected_he4 ejected_li6 ejected_li7 ejected_be7 ejected_be9 ejected_b8 ejected_b10 ejected_b11 ejected_c9 ejected_c11 ejected_c12 ejected_c13 ejected_n12 ejected_n13 ejected_n14 ejected_n15 ejected_o13 ejected_o14 ejected_o15 ejected_o16 ejected_o17 ejected_o18 ejected_f17 ejected_f18 ejected_f19 ejected_ne18 ejected_ne19 ejected_ne20 ejected_ne21 ejected_ne22 ejected_na20 ejected_na21 ejected_na22 ejected_na23 ejected_mg21 ejected_mg22 ejected_mg23 ejected_mg24 ejected_mg25 ejected_mg26 ejected_al23 ejected_al24 ejected_al25 ejected_al26 ejected_al27 ejected_si25 ejected_si26 ejected_si27 ejected_si28 ejected_si29 ejected_si30 ejected_p27 ejected_p28 ejected_p29 ejected_p30 ejected_p31 ejected_s29 ejected_s30 ejected_s31 ejected_s32 ejected_s33 ejected_s34 ejected_s35 ejected_s36 ejected_cl31 ejected_cl32 ejected_cl33 ejected_cl34 ejected_cl35 ejected_cl36 ejected_cl37 ejected_ar32 ejected_ar33 ejected_ar34 ejected_ar35 ejected_ar36 ejected_ar37 ejected_ar38 ejected_ar39 ejected_ar40 ejected_k35 ejected_k36 ejected_k37 ejected_k38 ejected_k39 ejected_k40 ejected_ca36 ejected_ca37 ejected_ca38 ejected_ca39 ejected_ca40 ejected_sc40'
h = fmr.FastMesaReader('LOGS/history.data', plotting_tings)    
burst2_index = -1 # the row of data in history.data to plot from.
burst1_index = -1 # the row of data in history.data to plot from.


fig, ax = plt.subplots(2, 1, sharex=True, figsize=(12.8,14.4), gridspec_kw={'height_ratios': [2, 1]})
fig.subplots_adjust(hspace=0)
fig.tight_layout()
plt.gcf().subplots_adjust(hspace=0,bottom=0.05, left=0.12)

x_centre_offset = -0.2
y_offset_top = 0.5
y_offset_bot = 0.7


# From Asplund et. al. 2009
H1_solar_mass_frac = 0.738085238
He3_solar_mass_frac = 4.1251e-05
He4_solar_mass_frac = 0.248458749
Li7_solar_mass_frac = 5.347553867866217e-11
C12_solar_mass_frac = 0.0023479921849246885
C13_solar_mass_frac = 2.6264022162801824e-05
N14_solar_mass_frac = 0.0006943429738573174
N15_solar_mass_frac = 1.5936949716182629e-06
O16_solar_mass_frac = 0.005745050422480951
O17_solar_mass_frac = 2.182566435670741e-06
O18_solar_mass_frac = 1.1517500979792827e-05
F19_solar_mass_frac = 5.070647693536659e-07
Ne20_solar_mass_frac = 0.0011625972594930384
Ne21_solar_mass_frac = 2.7869381311259143e-06
Ne22_solar_mass_frac = 8.548569964958532e-05
Na23_solar_mass_frac = 2.9368444780430237e-05
Mg24_solar_mass_frac = 0.0005544455826402614
Mg25_solar_mass_frac = 7.019187019119655e-05
Mg26_solar_mass_frac = 7.72812490805074e-05
Al27_solar_mass_frac = 5.5900159597141475e-05
Si28_solar_mass_frac = 0.0006137862206959798
Si29_solar_mass_frac = 3.116657246812484e-05
Si30_solar_mass_frac = 2.054523456687628e-05
P31_solar_mass_frac = 5.852484710740299e-06
S32_solar_mass_frac = 0.0002941173102666167
S33_solar_mass_frac = 2.354673504715355e-06
S34_solar_mass_frac = 1.3291512283195887e-05
Cl35_solar_mass_frac = 6.160022812039695e-06
Cl37_solar_mass_frac = 1.9688011679546237e-06
Ar36_solar_mass_frac = 5.618233360211754e-05
Ar38_solar_mass_frac = 1.021494559543339e-05
K39_solar_mass_frac = 2.85828500433324e-06
Ca40_solar_mass_frac = 6.230278628930155e-05



# Burst 2

# H1
H1_2 = (h.ejected_h1[burst2_index])/H1_solar_mass_frac
ax[0].plot(1, H1_2, color='black', marker='^', markersize=8)

# He3=H3+He3 & He4 = B8+He4
He3_2 = (h.ejected_h3[burst2_index]+h.ejected_he3[burst2_index])/He3_solar_mass_frac
He4_2 = (h.ejected_b8[burst2_index]+h.ejected_he4[burst2_index])/He4_solar_mass_frac
ax[0].plot((3,4),(He3_2,He4_2),linewidth='1',color='black',marker='^', markersize=8)

# Li7= Li7+Be7
Li7_2 = (h.ejected_li7[burst2_index]+h.ejected_be7[burst2_index])/Li7_solar_mass_frac
ax[0].plot(7,Li7_2,color='black',marker='^', markersize=8)

# C12 = N12+C12 & C13 = N13+O13+C13
C12_2 = (h.ejected_n12[burst2_index]+h.ejected_c12[burst2_index])/C12_solar_mass_frac
C13_2 = (h.ejected_n13[burst2_index]+h.ejected_o13[burst2_index]+h.ejected_c13[burst2_index])/C13_solar_mass_frac
ax[0].plot((12,13),(C12_2,C13_2),linewidth='1',color='black',marker='^', markersize=8)

# N14 = O14+N14 & N15 = O15+N15
N14_2 = (h.ejected_o14[burst2_index]+h.ejected_n14[burst2_index])/N14_solar_mass_frac
N15_2 = (h.ejected_o15[burst2_index]+h.ejected_n15[burst2_index])/N15_solar_mass_frac
ax[0].plot((14,15),(N14_2,N15_2),linewidth='1',color='black',marker='^', markersize=8)


# O16 & O17 = F17+O17 & O18 = F18+Ne18+O18
O16_2 = (h.ejected_o16[burst2_index])/O16_solar_mass_frac
O17_2 = (h.ejected_f17[burst2_index]+h.ejected_o17[burst2_index])/O17_solar_mass_frac
O18_2 = (h.ejected_f18[burst2_index]+h.ejected_ne18[burst2_index]+h.ejected_o18[burst2_index])/O18_solar_mass_frac
ax[0].plot((16,17,18),(O16_2,O17_2,O18_2),linewidth='1',color='black',marker='^', markersize=8)

# F19 = Ne19+F19
F19_2 = (h.ejected_ne19[burst2_index]+h.ejected_f19[burst2_index])/F19_solar_mass_frac
ax[0].plot(19, F19_2, color='black', marker='^', markersize=8) # F19 = Ne19+F19

# Ne20 = Na20+ne20 & Ne21 = Na21+Mg21+Ne21 & Ne22 = Mg22+Na22+Ne22
Ne20_2 = (h.ejected_na20[burst2_index]+h.ejected_ne20[burst2_index])/Ne20_solar_mass_frac
Ne21_2 = (h.ejected_na21[burst2_index]+h.ejected_mg21[burst2_index]+h.ejected_ne21[burst2_index])/Ne21_solar_mass_frac
Ne22_2 = (h.ejected_mg22[burst2_index]+h.ejected_na22[burst2_index]+h.ejected_ne22[burst2_index])/Ne22_solar_mass_frac
ax[0].plot((20,21,22),(Ne20_2,Ne21_2,Ne22_2),linewidth='1',color='black',marker='^', markersize=8)

# Na23 = Al23+Mg23+Na23
Na23_2 = (h.ejected_al23[burst2_index]+h.ejected_mg23[burst2_index]+h.ejected_na23[burst2_index])/Na23_solar_mass_frac
ax[0].plot(23, Na23_2, color='black', marker='^', markersize=8)

# Mg24 = Al24+Mg24 &  Mg25 = Al25+Si25+Mg25 & Mg26 = Al26+Si26+Mg26
Mg24_2 = (h.ejected_al24[burst2_index]+h.ejected_mg24[burst2_index])/Mg24_solar_mass_frac
Mg25_2 = (h.ejected_al25[burst2_index]+h.ejected_si25[burst2_index]+h.ejected_mg25[burst2_index])/Mg25_solar_mass_frac
Mg26_2 = (h.ejected_mg26[burst2_index]+h.ejected_al26[burst2_index]+h.ejected_si26[burst2_index])/Mg26_solar_mass_frac
ax[0].plot((24,25,26),(Mg24_2,Mg25_2,Mg26_2),linewidth='1',color='black',marker='^', markersize=8)

# Al27 = Si27+P27+Al27
Al27_2 = (h.ejected_si27[burst2_index]+h.ejected_p27[burst2_index]+h.ejected_al27[burst2_index])/Al27_solar_mass_frac
ax[0].plot(27, Al27_2, color='black', marker='^', markersize=8) 

# Si28 = P28+Si28 & Si29 = P29+S29+Si29 & Si30 = P30+S30+Si30
Si28_2 = (h.ejected_p28[burst2_index]+h.ejected_si28[burst2_index])/Si28_solar_mass_frac
Si29_2 = (h.ejected_p29[burst2_index]+h.ejected_s29[burst2_index]+h.ejected_si29[burst2_index])/Si29_solar_mass_frac
Si30_2 = (h.ejected_p30[burst2_index]+h.ejected_s30[burst2_index]+h.ejected_si30[burst2_index])/Si30_solar_mass_frac
ax[0].plot((28,29,30),(Si28_2,Si29_2,Si30_2),linewidth='1',color='black',marker='^', markersize=8)

# P31 = S31+Cl31+P31
P31_2 = (h.ejected_s31[burst2_index]+h.ejected_cl31[burst2_index]+h.ejected_p31[burst2_index])/P31_solar_mass_frac
ax[0].plot(31, P31_2, color='black', marker='^', markersize=8) 

# S32 = Cl32+Ar32+S32 & S33 = Cl33+Ar33+S33 & S34 = Cl34+Ar34+S34
S32_2 = (h.ejected_cl32[burst2_index]+h.ejected_ar32[burst2_index]+h.ejected_s32[burst2_index])/S32_solar_mass_frac
S33_2 = (h.ejected_cl33[burst2_index]+h.ejected_ar33[burst2_index]+h.ejected_s33[burst2_index])/S33_solar_mass_frac
S34_2 = (h.ejected_cl34[burst2_index]+h.ejected_ar34[burst2_index]+h.ejected_s34[burst2_index])/S34_solar_mass_frac
ax[0].plot((32,33,34),(S32_2,S33_2,S34_2),linewidth='1',color='black',marker='^', markersize=8)


# Cl35 = Ar35+K35+S35+Cl35 &  Cl37 = Ar37+K37+Ca37+Cl37
Cl35_2 = (h.ejected_ar35[burst2_index]+h.ejected_k35[burst2_index]+h.ejected_s35[burst2_index]+h.ejected_cl35[burst2_index])/Cl35_solar_mass_frac
Cl37_2 = (h.ejected_ar37[burst2_index]+h.ejected_k37[burst2_index]+h.ejected_ca37[burst2_index]+h.ejected_cl37[burst2_index])/Cl37_solar_mass_frac
ax[0].plot((35,37),(Cl35_2,Cl37_2),linewidth='1',color='black',marker='^', markersize=8)

# Ar36 = K36+Ca36+Ar36+Cl36 & Ar38 = K38+Ca38+Ar38 
Ar36_2 = (h.ejected_k36[burst2_index]+h.ejected_ca36[burst2_index]+h.ejected_ar36[burst2_index]+h.ejected_cl36[burst2_index])/Ar36_solar_mass_frac
Ar38_2 = (h.ejected_k38[burst2_index]+h.ejected_ca38[burst2_index]+h.ejected_ar38[burst2_index])/Ar38_solar_mass_frac
ax[0].plot((36,38),(Ar36_2,Ar38_2),linewidth='1',color='black',marker='^', markersize=8)

# K39 = Ca39+K39+Ar39
K39_2 = (h.ejected_ca39[burst2_index]+h.ejected_k39[burst2_index]+h.ejected_ar39[burst2_index])/K39_solar_mass_frac
ax[0].plot(39,K39_2,linewidth='1',color='black',marker='^', markersize=8)

# Ca40 = Sc40+K40+Ca40
Ca40_2 = (h.ejected_ca40[burst2_index]+h.ejected_k40[burst2_index]+h.ejected_sc40[burst2_index])/Ca40_solar_mass_frac
ax[0].plot(40,Ca40_2,linewidth='1',color='black',marker='^', markersize=8)


# Burst 1

# plotting_tings = 'ejected_h1 ejected_h2 ejected_h3 ejected_he3 ejected_he4 ejected_li6 ejected_li7 ejected_be7 ejected_be9 ejected_b8 ejected_b10 ejected_b11 ejected_c9 ejected_c11 ejected_c12 ejected_c13 ejected_n12 ejected_n13 ejected_n14 ejected_n15 ejected_o13 ejected_o14 ejected_o15 ejected_o16 ejected_o17 ejected_o18 ejected_f17 ejected_f18 ejected_f19 ejected_ne18 ejected_ne19 ejected_ne20 ejected_ne21 ejected_ne22 ejected_na20 ejected_na21 ejected_na22 ejected_na23 ejected_mg21 ejected_mg22 ejected_mg23 ejected_mg24 ejected_mg25 ejected_mg26 ejected_al23 ejected_al24 ejected_al25 ejected_al26 ejected_al27 ejected_si25 ejected_si26 ejected_si27 ejected_si28 ejected_si29 ejected_si30 ejected_p27 ejected_p28 ejected_p29 ejected_p30 ejected_p31 ejected_s29 ejected_s30 ejected_s31 ejected_s32 ejected_s33 ejected_s34 ejected_s35 ejected_s36 ejected_cl31 ejected_cl32 ejected_cl33 ejected_cl34 ejected_cl35 ejected_cl36 ejected_cl37 ejected_ar32 ejected_ar33 ejected_ar34 ejected_ar35 ejected_ar36 ejected_ar37 ejected_ar38 ejected_ar39 ejected_ar40 ejected_k35 ejected_k36 ejected_k37 ejected_k38 ejected_k39 ejected_k40 ejected_ca36 ejected_ca37 ejected_ca38 ejected_ca39 ejected_ca40 ejected_sc40'
# h = fmr.FastMesaReader('../20pcZ_ONe_1p25Msun/LOGS/history.data', plotting_tings)    
# burst2_index = -1 # the row of data in history.data to plot from.
# burst1_index = -1 # the row of data in history.data to plot from.

# H1
H1_1 = (h.ejected_h1[burst1_index])/H1_solar_mass_frac
ax[0].plot(1, H1_1, color='#0343df', marker='.', markersize=12)

# He3=H3+He3 & He4 = B8+He4
He3_1 = (h.ejected_h3[burst1_index]+h.ejected_he3[burst1_index])/He3_solar_mass_frac
He4_1 = (h.ejected_b8[burst1_index]+h.ejected_he4[burst1_index])/He4_solar_mass_frac
ax[0].plot((3,4),(He3_1,He4_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# Li7= Li7+Be7
Li7_1 = (h.ejected_li7[burst1_index]+h.ejected_be7[burst1_index])/Li7_solar_mass_frac
ax[0].plot(7,Li7_1,color='#0343df',marker='.', markersize=12)

# C12 = N12+C12 & C13 = N13+O13+C13
C12_1 = (h.ejected_n12[burst1_index]+h.ejected_c12[burst1_index])/C12_solar_mass_frac
C13_1 = (h.ejected_n13[burst1_index]+h.ejected_o13[burst1_index]+h.ejected_c13[burst1_index])/C13_solar_mass_frac
ax[0].plot((12,13),(C12_1,C13_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# N14 = O14+N14 & N15 = O15+N15
N14_1 = (h.ejected_o14[burst1_index]+h.ejected_n14[burst1_index])/N14_solar_mass_frac
N15_1 = (h.ejected_o15[burst1_index]+h.ejected_n15[burst1_index])/N15_solar_mass_frac
ax[0].plot((14,15),(N14_1,N15_1),linewidth='1',color='#0343df',marker='.', markersize=12)


# O16 & O17 = F17+O17 & O18 = F18+Ne18+O18
O16_1 = (h.ejected_o16[burst1_index])/O16_solar_mass_frac
O17_1 = (h.ejected_f17[burst1_index]+h.ejected_o17[burst1_index])/O17_solar_mass_frac
O18_1 = (h.ejected_f18[burst1_index]+h.ejected_ne18[burst1_index]+h.ejected_o18[burst1_index])/O18_solar_mass_frac
ax[0].plot((16,17,18),(O16_1,O17_1,O18_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# F19 = Ne19+F19
F19_1 = (h.ejected_ne19[burst1_index]+h.ejected_f19[burst1_index])/F19_solar_mass_frac
ax[0].plot(19, F19_1, color='#0343df', marker='.', markersize=12) # F19 = Ne19+F19

# Ne20 = Na20+ne20 & Ne21 = Na21+Mg21+Ne21 & Ne22 = Mg22+Na22+Ne22
Ne20_1 = (h.ejected_na20[burst1_index]+h.ejected_ne20[burst1_index])/Ne20_solar_mass_frac
Ne21_1 = (h.ejected_na21[burst1_index]+h.ejected_mg21[burst1_index]+h.ejected_ne21[burst1_index])/Ne21_solar_mass_frac
Ne22_1 = (h.ejected_mg22[burst1_index]+h.ejected_na22[burst1_index]+h.ejected_ne22[burst1_index])/Ne22_solar_mass_frac
ax[0].plot((20,21,22),(Ne20_1,Ne21_1,Ne22_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# Na23 = Al23+Mg23+Na23
Na23_1 = (h.ejected_al23[burst1_index]+h.ejected_mg23[burst1_index]+h.ejected_na23[burst1_index])/Na23_solar_mass_frac
ax[0].plot(23, Na23_1, color='#0343df', marker='.', markersize=12)

# Mg24 = Al24+Mg24 &  Mg25 = Al25+Si25+Mg25 & Mg26 = Al26+Si26+Mg26
Mg24_1 = (h.ejected_al24[burst1_index]+h.ejected_mg24[burst1_index])/Mg24_solar_mass_frac
Mg25_1 = (h.ejected_al25[burst1_index]+h.ejected_si25[burst1_index]+h.ejected_mg25[burst1_index])/Mg25_solar_mass_frac
Mg26_1 = (h.ejected_mg26[burst1_index]+h.ejected_al26[burst1_index]+h.ejected_si26[burst1_index])/Mg26_solar_mass_frac
ax[0].plot((24,25,26),(Mg24_1,Mg25_1,Mg26_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# Al27 = Si27+P27+Al27
Al27_1 = (h.ejected_si27[burst1_index]+h.ejected_p27[burst1_index]+h.ejected_al27[burst1_index])/Al27_solar_mass_frac
ax[0].plot(27, Al27_1, color='#0343df', marker='.', markersize=12) 

# Si28 = P28+Si28 & Si29 = P29+S29+Si29 & Si30 = P30+S30+Si30
Si28_1 = (h.ejected_p28[burst1_index]+h.ejected_si28[burst1_index])/Si28_solar_mass_frac
Si29_1 = (h.ejected_p29[burst1_index]+h.ejected_s29[burst1_index]+h.ejected_si29[burst1_index])/Si29_solar_mass_frac
Si30_1 = (h.ejected_p30[burst1_index]+h.ejected_s30[burst1_index]+h.ejected_si30[burst1_index])/Si30_solar_mass_frac
ax[0].plot((28,29,30),(Si28_1,Si29_1,Si30_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# P31 = S31+Cl31+P31
P31_1 = (h.ejected_s31[burst1_index]+h.ejected_cl31[burst1_index]+h.ejected_p31[burst1_index])/P31_solar_mass_frac
ax[0].plot(31, P31_1, color='#0343df', marker='.', markersize=12) 

# S32 = Cl32+Ar32+S32 & S33 = Cl33+Ar33+S33 & S34 = Cl34+Ar34+S34
S32_1 = (h.ejected_cl32[burst1_index]+h.ejected_ar32[burst1_index]+h.ejected_s32[burst1_index])/S32_solar_mass_frac
S33_1 = (h.ejected_cl33[burst1_index]+h.ejected_ar33[burst1_index]+h.ejected_s33[burst1_index])/S33_solar_mass_frac
S34_1 = (h.ejected_cl34[burst1_index]+h.ejected_ar34[burst1_index]+h.ejected_s34[burst1_index])/S34_solar_mass_frac
ax[0].plot((32,33,34),(S32_1,S33_1,S34_1),linewidth='1',color='#0343df',marker='.', markersize=12)


# Cl35 = Ar35+K35+S35+Cl35 &  Cl37 = Ar37+K37+Ca37+Cl37
Cl35_1 = (h.ejected_ar35[burst1_index]+h.ejected_k35[burst1_index]+h.ejected_s35[burst1_index]+h.ejected_cl35[burst1_index])/Cl35_solar_mass_frac
Cl37_1 = (h.ejected_ar37[burst1_index]+h.ejected_k37[burst1_index]+h.ejected_ca37[burst1_index]+h.ejected_cl37[burst1_index])/Cl37_solar_mass_frac
ax[0].plot((35,37),(Cl35_1,Cl37_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# Ar36 = K36+Ca36+Ar36+Cl36 & Ar38 = K38+Ca38+Ar38 
Ar36_1 = (h.ejected_k36[burst1_index]+h.ejected_ca36[burst1_index]+h.ejected_ar36[burst1_index]+h.ejected_cl36[burst1_index])/Ar36_solar_mass_frac
Ar38_1 = (h.ejected_k38[burst1_index]+h.ejected_ca38[burst1_index]+h.ejected_ar38[burst1_index])/Ar38_solar_mass_frac
ax[0].plot((36,38),(Ar36_1,Ar38_1),linewidth='1',color='#0343df',marker='.', markersize=12)

# K39 = Ca39+K39+Ar39
K39_1 = (h.ejected_ca39[burst1_index]+h.ejected_k39[burst1_index]+h.ejected_ar39[burst1_index])/K39_solar_mass_frac
ax[0].plot(39,K39_1,linewidth='1',color='#0343df',marker='.', markersize=12)

# Ca40 = Sc40+K40+Ca40
Ca40_1 = (h.ejected_ca40[burst1_index]+h.ejected_k40[burst1_index]+h.ejected_sc40[burst1_index])/Ca40_solar_mass_frac
ax[0].plot(40,Ca40_1,linewidth='1',color='#0343df',marker='.', markersize=12)


ax[0].text(1+x_centre_offset, H1_1+H1_1*y_offset_top, 'H', fontsize=13)
ax[0].text(4+x_centre_offset, He4_1+He4_1*y_offset_top, 'He', fontsize=13)
ax[0].text(7+x_centre_offset, Li7_1+Li7_1*y_offset_top, 'Li', fontsize=13)
ax[0].text(13+x_centre_offset, C13_1+C13_1*y_offset_top, 'C', fontsize=13)
ax[0].text(15+x_centre_offset, N15_1+N15_1*y_offset_top, 'N', fontsize=13)
ax[0].text(17+x_centre_offset, O17_1+O17_1*y_offset_top, 'O', fontsize=13)
ax[0].text(19+x_centre_offset, F19_1+F19_1*y_offset_top, 'F', fontsize=13)
ax[0].text(21+x_centre_offset, Ne21_1-Ne21_1*y_offset_top, 'Ne', fontsize=13)
ax[0].text(23+x_centre_offset, Na23_1+Na23_1*y_offset_top, 'Na', fontsize=13)
ax[0].text(25+x_centre_offset, Mg25_1+Mg25_1*y_offset_top, 'Mg', fontsize=13)
ax[0].text(27+x_centre_offset, Al27_1+Al27_1*y_offset_top, 'Al', fontsize=13)
ax[0].text(29+x_centre_offset, Si29_1-Si29_1*y_offset_top, 'Si', fontsize=13)
ax[0].text(31+x_centre_offset, P31_1-P31_1*y_offset_top, 'P', fontsize=13)
ax[0].text(33+x_centre_offset, S33_1-S33_1*y_offset_top, 'S', fontsize=13)
ax[0].text(37+x_centre_offset, Cl37_1+Cl37_1*y_offset_top, 'Cl', fontsize=13)
ax[0].text(38+x_centre_offset, Ar38_1-Ar38_1*y_offset_top, 'Ar', fontsize=13)
ax[0].text(39+x_centre_offset, K39_1-K39_1*y_offset_top, 'K', fontsize=13)
ax[0].text(40+x_centre_offset, Ca40_1+Ca40_1*y_offset_top, 'Ca', fontsize=13)


legend_elements = [Line2D([0], [0], marker='.', color='#0343df', label='Burst 1',
                          markersize=10),
                   Line2D([0], [0], marker='^', color='black', label='Burst 2',
                          markersize=10)]
ax[0].legend(handles=legend_elements, frameon=False, loc='lower right',prop={'size': 18})

# legend_elements = [Line2D([0], [0], marker='.', color='#0343df', label='Burst 1',
#                           markersize=5)]
# ax.legend(handles=legend_elements, loc='lower right')

ax[1].plot(1, H1_1/H1_2, color='#0343df', marker='.', markersize=12)
ax[1].plot((3,4),(He3_1/He3_2,He4_1/He4_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot(7,Li7_1/Li7_2,color='#0343df',marker='.', markersize=12)
ax[1].plot((12,13),(C12_1/C12_2,C13_1/C13_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot((14,15),(N14_1/N14_2,N15_1/N15_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot((16,17,18),(O16_1/O16_2,O17_1/O17_2,O18_1/O18_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot(19, F19_1/F19_2, color='#0343df', marker='.', markersize=12)
ax[1].plot((20,21,22),(Ne20_1/Ne20_2,Ne21_1/Ne21_2,Ne22_1/Ne22_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot(23, Na23_1/Na23_2, color='#0343df', marker='.', markersize=12)
ax[1].plot((24,25,26),(Mg24_1/Mg24_2,Mg25_1/Mg25_2,Mg26_1/Mg26_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot(27, Al27_1/Al27_2, color='#0343df', marker='.', markersize=12)
ax[1].plot((28,29,30),(Si28_1/Si28_2,Si29_1/Si29_2,Si30_1/Si30_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot(31, P31_1/P31_2, color='#0343df', marker='.', markersize=12)
ax[1].plot((32,33,34),(S32_1/S32_2,S33_1/S33_2,S34_1/S34_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot((35,37),(Cl35_1/Cl35_2,Cl37_1/Cl37_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot((36,38),(Ar36_1/Ar36_2,Ar38_1/Ar38_2),linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot(39,K39_1/K39_2,linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].plot(40,Ca40_1/Ca40_2,linewidth='1',color='#0343df',marker='.', markersize=12)
ax[1].text(1+x_centre_offset, H1_1/H1_2+(H1_1/H1_2)*y_offset_top, 'H', fontsize=13)
ax[1].text(4+x_centre_offset, He4_1/He4_2+(He4_1/He4_2)*y_offset_top, 'He', fontsize=13)
ax[1].text(7+x_centre_offset, Li7_1/Li7_2+(Li7_1/Li7_2)*y_offset_top, 'Li', fontsize=13)
ax[1].text(13+x_centre_offset, C13_1/C13_2+(C13_1/C13_2)*y_offset_top, 'C', fontsize=13)
ax[1].text(15+x_centre_offset, N15_1/N15_2+(N15_1/N15_2)*y_offset_top, 'N', fontsize=13)
ax[1].text(17+x_centre_offset, O17_1/O17_2-(O17_1/O17_2)*y_offset_top, 'O', fontsize=13)
ax[1].text(19+x_centre_offset, F19_1/F19_2+(F19_1/F19_2)*y_offset_top, 'F', fontsize=13)
ax[1].text(21+x_centre_offset, Ne21_1/Ne21_2-(Ne21_1/Ne21_2)*y_offset_top, 'Ne', fontsize=13)
ax[1].text(23+x_centre_offset, Na23_1/Na23_2+(Na23_1/Na23_2)*y_offset_top, 'Na', fontsize=13)
ax[1].text(25+x_centre_offset, Mg25_1/Mg25_2+(Mg25_1/Mg25_2)*y_offset_top, 'Mg', fontsize=13)
ax[1].text(27+x_centre_offset, Al27_1/Al27_2+(Al27_1/Al27_2)*y_offset_top, 'Al', fontsize=13)
ax[1].text(29+x_centre_offset, Si29_1/Si29_2-(Si29_1/Si29_2)*y_offset_top, 'Si', fontsize=13)
ax[1].text(31+x_centre_offset, P31_1/P31_2+(P31_1/P31_2)*y_offset_top, 'P', fontsize=13)
ax[1].text(33+x_centre_offset, S33_1/S33_2-(S33_1/S33_2)*y_offset_bot, 'S', fontsize=13)
ax[1].text(37+x_centre_offset, Cl37_1/Cl37_2+(Cl37_1/Cl37_2)*y_offset_top, 'Cl', fontsize=13)
ax[1].text(38+x_centre_offset, Ar38_1/Ar38_2+(Ar38_1/Ar38_2)*y_offset_top, 'Ar', fontsize=13)
ax[1].text(39+x_centre_offset, K39_1/K39_2+(K39_1/K39_2)*y_offset_top, 'K', fontsize=13)
ax[1].text(40+x_centre_offset, Ca40_1/Ca40_2+(Ca40_1/Ca40_2)*y_offset_top, 'Ca', fontsize=13)


ax[1].set_xlim(0,41)
ax[1].tick_params(axis='x', labelsize=16)
ax[1].set_xscale('linear')
ax[0].set_yscale('log')
ax[1].set_yscale('log')
ax[0].set_yticks([1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1,1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10]) #replaces tick labels from '10^x' to 'x'
ax[0].set_yticklabels(['-8','-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10']) #replaces tick labels from '10^x' to 'x'
ax[1].set_yticks([1e-2,1e-1,1,1e1,1e2]) #replaces tick labels from '10^x' to 'x'
ax[1].set_yticklabels(['-2','-1','0','1','2']) #replaces tick labels from '10^x' to 'x'
ax[0].tick_params(axis='y', labelsize=16)
ax[1].tick_params(axis='y', labelsize=16)
ax[0].xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax[0].grid(which='major', linestyle=':', linewidth='1')
plt.minorticks_on()
ax[0].grid(which='minor', linestyle=':', linewidth='0.4')
ax[0].hlines(1, 0, 50, color='#000000', linestyle='--', linewidth=0.5)

ax[1].xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax[1].grid(which='major', linestyle=':', linewidth='1')
ax[1].grid(which='minor', linestyle=':', linewidth='0.4')

ax[1].set_xlabel('Mass number', fontsize=20)
ax[0].set_ylabel('Mean overproduction\nfactor $\\mathrm{log_{10}}\\left(\\dfrac{X_{i}}{X_{i,\\odot}}\\right)$', fontsize=20)
ax[1].set_ylabel('$\\mathrm{log_{10}}\\left(\\dfrac{X_{i,\\mathrm{burst \\ 1}}}{X_{i,\\mathrm{burst \\ 2}}}\\right)$', fontsize=20)

# ax[0].set_ylim(0,0)
# ax[1].set_ylim(0,0)

# plt.show()
plt.savefig('overproduction_comparison_plot.pdf', dpi=500)






# Elements not considered when graphing due to not being interested in their abundance
# h2 - Stable
# li6 -  Stable
# be9 - Stable
# b10 - Stable
# b11 - Stable
# c9 - decays to B9 which decyas to Be9 which we're not interested in
# c11 - T1/2 = 30.364 min, decays to B11 which we're not interested in
# s36 - Stable
# ar40 - Stable



# Solar Composition Values:
# Taken from Lodders09.data within $MESA_dir/data/chem_data/Lodders09.data
# H1 = 7.0945477357E-01
# He3 = 8.4641515456E-05
# He4 = 2.7501644504E-01
# Li7 = 9.8556690166E-09
# C12 = 2.3370919413E-03
# C13 = 2.8452149548E-05
# N14 = 8.1299682160E-04
# N15 = 3.1966552925E-06
# O16 = 6.8808895413E-03
# O17 = 2.7474252428E-06
# O18 = 1.5531307205E-05
# F19 = 4.1844135602E-07
# Ne21 = 4.2164622894E-06
# Na23 = 3.6352024324E-05
# Mg24 = 5.3250196132E-04
# Mg25 = 7.0534596214E-05
# Al27 = 6.2568980455E-05
# Si29 = 3.7176525818E-05
# Si30 = 2.5392454637E-05
# P31 = 7.0479812061E-06
# S32 = 3.5061857535E-04
# S33 = 2.8564457061E-06
# S34 = 1.6577684516E-05
# Cl35 = 3.7581928546E-06
# Cl37 = 1.2668835242E-06
# Ar36 = 7.7311395865E-05
# Ar38 = 1.4884854207E-05
# K39 = 3.7390184012E-06
# Ca40 = 6.4097458307E-05