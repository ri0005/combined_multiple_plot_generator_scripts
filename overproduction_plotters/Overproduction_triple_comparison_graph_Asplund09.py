import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
from matplotlib.lines import Line2D
import fastmesareader as fmr

plotting_tings = 'ejected_h1 ejected_h2 ejected_h3 ejected_he3 ejected_he4 ejected_li6 ejected_li7 ejected_be7 ejected_be9 ejected_b8 ejected_b10 ejected_b11 ejected_c9 ejected_c11 ejected_c12 ejected_c13 ejected_n12 ejected_n13 ejected_n14 ejected_n15 ejected_o13 ejected_o14 ejected_o15 ejected_o16 ejected_o17 ejected_o18 ejected_f17 ejected_f18 ejected_f19 ejected_ne18 ejected_ne19 ejected_ne20 ejected_ne21 ejected_ne22 ejected_na20 ejected_na21 ejected_na22 ejected_na23 ejected_mg21 ejected_mg22 ejected_mg23 ejected_mg24 ejected_mg25 ejected_mg26 ejected_al23 ejected_al24 ejected_al25 ejected_al26 ejected_al27 ejected_si25 ejected_si26 ejected_si27 ejected_si28 ejected_si29 ejected_si30 ejected_p27 ejected_p28 ejected_p29 ejected_p30 ejected_p31 ejected_s29 ejected_s30 ejected_s31 ejected_s32 ejected_s33 ejected_s34 ejected_s35 ejected_s36 ejected_cl31 ejected_cl32 ejected_cl33 ejected_cl34 ejected_cl35 ejected_cl36 ejected_cl37 ejected_ar32 ejected_ar33 ejected_ar34 ejected_ar35 ejected_ar36 ejected_ar37 ejected_ar38 ejected_ar39 ejected_ar40 ejected_k35 ejected_k36 ejected_k37 ejected_k38 ejected_k39 ejected_k40 ejected_ca36 ejected_ca37 ejected_ca38 ejected_ca39 ejected_ca40 ejected_sc40'
a = fmr.FastMesaReader('LOGS/history.data', plotting_tings) 
b = fmr.FastMesaReader('LOGS/history.data', plotting_tings)    
c = fmr.FastMesaReader('LOGS/history.data', plotting_tings)    

plota_index = -1
plotb_index = -1
plotc_index = -1

fig = plt.figure(figsize=(12.8,9.6))
ax = fig.add_subplot(111)	

x_centre_offset = -0.3 #-0.2
x_centre_offset_2 = -0.2
y_offset_top = 0.5
y_offset_bot = 0.55

# From Asplund et. al. 2009
H1_solar_mass_frac = 0.738085238
He3_solar_mass_frac = 4.1251e-05
He4_solar_mass_frac = 0.248458749
Li7_solar_mass_frac = 5.347553867866217e-11
C12_solar_mass_frac = 0.0023479921849246885
C13_solar_mass_frac = 2.6264022162801824e-05
N14_solar_mass_frac = 0.0006943429738573174
N15_solar_mass_frac = 1.5936949716182629e-06
O16_solar_mass_frac = 0.005745050422480951
O17_solar_mass_frac = 2.182566435670741e-06
O18_solar_mass_frac = 1.1517500979792827e-05
F19_solar_mass_frac = 5.070647693536659e-07
Ne20_solar_mass_frac = 0.0011625972594930384
Ne21_solar_mass_frac = 2.7869381311259143e-06
Ne22_solar_mass_frac = 8.548569964958532e-05
Na23_solar_mass_frac = 2.9368444780430237e-05
Mg24_solar_mass_frac = 0.0005544455826402614
Mg25_solar_mass_frac = 7.019187019119655e-05
Mg26_solar_mass_frac = 7.72812490805074e-05
Al27_solar_mass_frac = 5.5900159597141475e-05
Si28_solar_mass_frac = 0.0006137862206959798
Si29_solar_mass_frac = 3.116657246812484e-05
Si30_solar_mass_frac = 2.054523456687628e-05
P31_solar_mass_frac = 5.852484710740299e-06
S32_solar_mass_frac = 0.0002941173102666167
S33_solar_mass_frac = 2.354673504715355e-06
S34_solar_mass_frac = 1.3291512283195887e-05
Cl35_solar_mass_frac = 6.160022812039695e-06
Cl37_solar_mass_frac = 1.9688011679546237e-06
Ar36_solar_mass_frac = 5.618233360211754e-05
Ar38_solar_mass_frac = 1.021494559543339e-05
K39_solar_mass_frac = 2.85828500433324e-06
Ca40_solar_mass_frac = 6.230278628930155e-05


H1_1 = (a.ejected_h1[plota_index])/H1_solar_mass_frac
ax.plot(1, H1_1, color='tab:orange', marker='s', markersize=6)

# He3=H3+He3 & He4 = B8+He4
He3_1 = (a.ejected_h3[plota_index]+a.ejected_he3[plota_index])/He3_solar_mass_frac
He4_1 = (a.ejected_b8[plota_index]+a.ejected_he4[plota_index])/He4_solar_mass_frac
ax.plot((3,4),(He3_1,He4_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# Li7= Li7+Be7
Li7_1 = (a.ejected_li7[plota_index]+a.ejected_be7[plota_index])/Li7_solar_mass_frac
ax.plot(7,Li7_1,color='tab:orange',marker='s', markersize=6)

# C12 = N12+C12 & C13 = N13+O13+C13
C12_1 = (a.ejected_n12[plota_index]+a.ejected_c12[plota_index])/C12_solar_mass_frac
C13_1 = (a.ejected_n13[plota_index]+a.ejected_o13[plota_index]+a.ejected_c13[plota_index])/C13_solar_mass_frac
ax.plot((12,13),(C12_1,C13_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# N14 = O14+N14 & N15 = O15+N15
N14_1 = (a.ejected_o14[plota_index]+a.ejected_n14[plota_index])/N14_solar_mass_frac
N15_1 = (a.ejected_o15[plota_index]+a.ejected_n15[plota_index])/N15_solar_mass_frac
ax.plot((14,15),(N14_1,N15_1),linewidth='1',color='tab:orange',marker='s', markersize=6)


# O16 & O17 = F17+O17 & O18 = F18+Ne18+O18
O16_1 = (a.ejected_o16[plota_index])/O16_solar_mass_frac
O17_1 = (a.ejected_f17[plota_index]+a.ejected_o17[plota_index])/O17_solar_mass_frac
O18_1 = (a.ejected_f18[plota_index]+a.ejected_ne18[plota_index]+a.ejected_o18[plota_index])/O18_solar_mass_frac
ax.plot((16,17,18),(O16_1,O17_1,O18_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# F19 = Ne19+F19
F19_1 = (a.ejected_ne19[plota_index]+a.ejected_f19[plota_index])/F19_solar_mass_frac
ax.plot(19, F19_1, color='tab:orange', marker='s', markersize=6) # F19 = Ne19+F19

# Ne20 = Na20+ne20 & Ne21 = Na21+Mg21+Ne21 & Ne22 = Mg22+Na22+Ne22
Ne20_1 = (a.ejected_na20[plota_index]+a.ejected_ne20[plota_index])/Ne20_solar_mass_frac
Ne21_1 = (a.ejected_na21[plota_index]+a.ejected_mg21[plota_index]+a.ejected_ne21[plota_index])/Ne21_solar_mass_frac
Ne22_1 = (a.ejected_mg22[plota_index]+a.ejected_na22[plota_index]+a.ejected_ne22[plota_index])/Ne22_solar_mass_frac
ax.plot((20,21,22),(Ne20_1,Ne21_1,Ne22_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# Na23 = Al23+Mg23+Na23
Na23_1 = (a.ejected_al23[plota_index]+a.ejected_mg23[plota_index]+a.ejected_na23[plota_index])/Na23_solar_mass_frac
ax.plot(23, Na23_1, color='tab:orange', marker='s', markersize=6)

# Mg24 = Al24+Mg24 &  Mg25 = Al25+Si25+Mg25 & Mg26 = Al26+Si26+Mg26
Mg24_1 = (a.ejected_al24[plota_index]+a.ejected_mg24[plota_index])/Mg24_solar_mass_frac
Mg25_1 = (a.ejected_al25[plota_index]+a.ejected_si25[plota_index]+a.ejected_mg25[plota_index])/Mg25_solar_mass_frac
Mg26_1 = (a.ejected_mg26[plota_index]+a.ejected_al26[plota_index]+a.ejected_si26[plota_index])/Mg26_solar_mass_frac
ax.plot((24,25,26),(Mg24_1,Mg25_1,Mg26_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# Al27 = Si27+P27+Al27
Al27_1 = (a.ejected_si27[plota_index]+a.ejected_p27[plota_index]+a.ejected_al27[plota_index])/Al27_solar_mass_frac
ax.plot(27, Al27_1, color='tab:orange', marker='s', markersize=6) 

# Si28 = P28+Si28 & Si29 = P29+S29+Si29 & Si30 = P30+S30+Si30
Si28_1 = (a.ejected_p28[plota_index]+a.ejected_si28[plota_index])/Si28_solar_mass_frac
Si29_1 = (a.ejected_p29[plota_index]+a.ejected_s29[plota_index]+a.ejected_si29[plota_index])/Si29_solar_mass_frac
Si30_1 = (a.ejected_p30[plota_index]+a.ejected_s30[plota_index]+a.ejected_si30[plota_index])/Si30_solar_mass_frac
ax.plot((28,29,30),(Si28_1,Si29_1,Si30_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# P31 = S31+Cl31+P31
P31_1 = (a.ejected_s31[plota_index]+a.ejected_cl31[plota_index]+a.ejected_p31[plota_index])/P31_solar_mass_frac
ax.plot(31, P31_1, color='tab:orange', marker='s', markersize=6) 

# S32 = Cl32+Ar32+S32 & S33 = Cl33+Ar33+S33 & S34 = Cl34+Ar34+S34
S32_1 = (a.ejected_cl32[plota_index]+a.ejected_ar32[plota_index]+a.ejected_s32[plota_index])/S32_solar_mass_frac
S33_1 = (a.ejected_cl33[plota_index]+a.ejected_ar33[plota_index]+a.ejected_s33[plota_index])/S33_solar_mass_frac
S34_1 = (a.ejected_cl34[plota_index]+a.ejected_ar34[plota_index]+a.ejected_s34[plota_index])/S34_solar_mass_frac
ax.plot((32,33,34),(S32_1,S33_1,S34_1),linewidth='1',color='tab:orange',marker='s', markersize=6)


# Cl35 = Ar35+K35+S35+Cl35 &  Cl37 = Ar37+K37+Ca37+Cl37
Cl35_1 = (a.ejected_ar35[plota_index]+a.ejected_k35[plota_index]+a.ejected_s35[plota_index]+a.ejected_cl35[plota_index])/Cl35_solar_mass_frac
Cl37_1 = (a.ejected_ar37[plota_index]+a.ejected_k37[plota_index]+a.ejected_ca37[plota_index]+a.ejected_cl37[plota_index])/Cl37_solar_mass_frac
ax.plot((35,37),(Cl35_1,Cl37_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# Ar36 = K36+Ca36+Ar36+Cl36 & Ar38 = K38+Ca38+Ar38 
Ar36_1 = (a.ejected_k36[plota_index]+a.ejected_ca36[plota_index]+a.ejected_ar36[plota_index]+a.ejected_cl36[plota_index])/Ar36_solar_mass_frac
Ar38_1 = (a.ejected_k38[plota_index]+a.ejected_ca38[plota_index]+a.ejected_ar38[plota_index])/Ar38_solar_mass_frac
ax.plot((36,38),(Ar36_1,Ar38_1),linewidth='1',color='tab:orange',marker='s', markersize=6)

# K39 = Ca39+K39+Ar39
K39_1 = (a.ejected_ca39[plota_index]+a.ejected_k39[plota_index]+a.ejected_ar39[plota_index])/K39_solar_mass_frac
ax.plot(39,K39_1,linewidth='1',color='tab:orange',marker='s', markersize=6)

# Ca40 = Sc40+K40+Ca40
Ca40_1 = (a.ejected_ca40[plota_index]+a.ejected_k40[plota_index]+a.ejected_sc40[plota_index])/Ca40_solar_mass_frac
ax.plot(40,Ca40_1,linewidth='1',color='tab:orange',marker='s', markersize=6)

# ------------------------------------------------------------------------------------------------------------------------------------------------------


# H1
H1_2 = (b.ejected_h1[plotb_index])/H1_solar_mass_frac
ax.plot(1, H1_2, color='black', marker='^', markersize=8)

# He3=H3+He3 & He4 = B8+He4
He3_2 = (b.ejected_h3[plotb_index]+b.ejected_he3[plotb_index])/He3_solar_mass_frac
He4_2 = (b.ejected_b8[plotb_index]+b.ejected_he4[plotb_index])/He4_solar_mass_frac
ax.plot((3,4),(He3_2,He4_2),linewidth='1',color='black',marker='^', markersize=8)

# Li7= Li7+Be7
Li7_2 = (b.ejected_li7[plotb_index]+b.ejected_be7[plotb_index])/Li7_solar_mass_frac
ax.plot(7,Li7_2,color='black',marker='^', markersize=8)

# C12 = N12+C12 & C13 = N13+O13+C13
C12_2 = (b.ejected_n12[plotb_index]+b.ejected_c12[plotb_index])/C12_solar_mass_frac
C13_2 = (b.ejected_n13[plotb_index]+b.ejected_o13[plotb_index]+b.ejected_c13[plotb_index])/C13_solar_mass_frac
ax.plot((12,13),(C12_2,C13_2),linewidth='1',color='black',marker='^', markersize=8)

# N14 = O14+N14 & N15 = O15+N15
N14_2 = (b.ejected_o14[plotb_index]+b.ejected_n14[plotb_index])/N14_solar_mass_frac
N15_2 = (b.ejected_o15[plotb_index]+b.ejected_n15[plotb_index])/N15_solar_mass_frac
ax.plot((14,15),(N14_2,N15_2),linewidth='1',color='black',marker='^', markersize=8)


# O16 & O17 = F17+O17 & O18 = F18+Ne18+O18
O16_2 = (b.ejected_o16[plotb_index])/O16_solar_mass_frac
O17_2 = (b.ejected_f17[plotb_index]+b.ejected_o17[plotb_index])/O17_solar_mass_frac
O18_2 = (b.ejected_f18[plotb_index]+b.ejected_ne18[plotb_index]+b.ejected_o18[plotb_index])/O18_solar_mass_frac
ax.plot((16,17,18),(O16_2,O17_2,O18_2),linewidth='1',color='black',marker='^', markersize=8)

# F19 = Ne19+F19
F19_2 = (b.ejected_ne19[plotb_index]+b.ejected_f19[plotb_index])/F19_solar_mass_frac
ax.plot(19, F19_2, color='black', marker='^', markersize=8) # F19 = Ne19+F19

# Ne20 = Na20+ne20 & Ne21 = Na21+Mg21+Ne21 & Ne22 = Mg22+Na22+Ne22
Ne20_2 = (b.ejected_na20[plotb_index]+b.ejected_ne20[plotb_index])/Ne20_solar_mass_frac
Ne21_2 = (b.ejected_na21[plotb_index]+b.ejected_mg21[plotb_index]+b.ejected_ne21[plotb_index])/Ne21_solar_mass_frac
Ne22_2 = (b.ejected_mg22[plotb_index]+b.ejected_na22[plotb_index]+b.ejected_ne22[plotb_index])/Ne22_solar_mass_frac
ax.plot((20,21,22),(Ne20_2,Ne21_2,Ne22_2),linewidth='1',color='black',marker='^', markersize=8)

# Na23 = Al23+Mg23+Na23
Na23_2 = (b.ejected_al23[plotb_index]+b.ejected_mg23[plotb_index]+b.ejected_na23[plotb_index])/Na23_solar_mass_frac
ax.plot(23, Na23_2, color='black', marker='^', markersize=8)

# Mg24 = Al24+Mg24 &  Mg25 = Al25+Si25+Mg25 & Mg26 = Al26+Si26+Mg26
Mg24_2 = (b.ejected_al24[plotb_index]+b.ejected_mg24[plotb_index])/Mg24_solar_mass_frac
Mg25_2 = (b.ejected_al25[plotb_index]+b.ejected_si25[plotb_index]+b.ejected_mg25[plotb_index])/Mg25_solar_mass_frac
Mg26_2 = (b.ejected_mg26[plotb_index]+b.ejected_al26[plotb_index]+b.ejected_si26[plotb_index])/Mg26_solar_mass_frac
ax.plot((24,25,26),(Mg24_2,Mg25_2,Mg26_2),linewidth='1',color='black',marker='^', markersize=8)

# Al27 = Si27+P27+Al27
Al27_2 = (b.ejected_si27[plotb_index]+b.ejected_p27[plotb_index]+b.ejected_al27[plotb_index])/Al27_solar_mass_frac
ax.plot(27, Al27_2, color='black', marker='^', markersize=8) 

# Si28 = P28+Si28 & Si29 = P29+S29+Si29 & Si30 = P30+S30+Si30
Si28_2 = (b.ejected_p28[plotb_index]+b.ejected_si28[plotb_index])/Si28_solar_mass_frac
Si29_2 = (b.ejected_p29[plotb_index]+b.ejected_s29[plotb_index]+b.ejected_si29[plotb_index])/Si29_solar_mass_frac
Si30_2 = (b.ejected_p30[plotb_index]+b.ejected_s30[plotb_index]+b.ejected_si30[plotb_index])/Si30_solar_mass_frac
ax.plot((28,29,30),(Si28_2,Si29_2,Si30_2),linewidth='1',color='black',marker='^', markersize=8)

# P31 = S31+Cl31+P31
P31_2 = (b.ejected_s31[plotb_index]+b.ejected_cl31[plotb_index]+b.ejected_p31[plotb_index])/P31_solar_mass_frac
ax.plot(31, P31_2, color='black', marker='^', markersize=8) 

# S32 = Cl32+Ar32+S32 & S33 = Cl33+Ar33+S33 & S34 = Cl34+Ar34+S34
S32_2 = (b.ejected_cl32[plotb_index]+b.ejected_ar32[plotb_index]+b.ejected_s32[plotb_index])/S32_solar_mass_frac
S33_2 = (b.ejected_cl33[plotb_index]+b.ejected_ar33[plotb_index]+b.ejected_s33[plotb_index])/S33_solar_mass_frac
S34_2 = (b.ejected_cl34[plotb_index]+b.ejected_ar34[plotb_index]+b.ejected_s34[plotb_index])/S34_solar_mass_frac
ax.plot((32,33,34),(S32_2,S33_2,S34_2),linewidth='1',color='black',marker='^', markersize=8)


# Cl35 = Ar35+K35+S35+Cl35 &  Cl37 = Ar37+K37+Ca37+Cl37
Cl35_2 = (b.ejected_ar35[plotb_index]+b.ejected_k35[plotb_index]+b.ejected_s35[plotb_index]+b.ejected_cl35[plotb_index])/Cl35_solar_mass_frac
Cl37_2 = (b.ejected_ar37[plotb_index]+b.ejected_k37[plotb_index]+b.ejected_ca37[plotb_index]+b.ejected_cl37[plotb_index])/Cl37_solar_mass_frac
ax.plot((35,37),(Cl35_2,Cl37_2),linewidth='1',color='black',marker='^', markersize=8)

# Ar36 = K36+Ca36+Ar36+Cl36 & Ar38 = K38+Ca38+Ar38 
Ar36_2 = (b.ejected_k36[plotb_index]+b.ejected_ca36[plotb_index]+b.ejected_ar36[plotb_index]+b.ejected_cl36[plotb_index])/Ar36_solar_mass_frac
Ar38_2 = (b.ejected_k38[plotb_index]+b.ejected_ca38[plotb_index]+b.ejected_ar38[plotb_index])/Ar38_solar_mass_frac
ax.plot((36,38),(Ar36_2,Ar38_2),linewidth='1',color='black',marker='^', markersize=8)

# K39 = Ca39+K39+Ar39
K39_2 = (b.ejected_ca39[plotb_index]+b.ejected_k39[plotb_index]+b.ejected_ar39[plotb_index])/K39_solar_mass_frac
ax.plot(39,K39_2,linewidth='1',color='black',marker='^', markersize=8)

# Ca40 = Sc40+K40+Ca40
Ca40_2 = (b.ejected_ca40[plotb_index]+b.ejected_k40[plotb_index]+b.ejected_sc40[plotb_index])/Ca40_solar_mass_frac
ax.plot(40,Ca40_2,linewidth='1',color='black',marker='^', markersize=8)

# ------------------------------------------------------------------------------------------------------------------------------------------------------

H1_3 = (c.ejected_h1[plotc_index])/H1_solar_mass_frac
ax.plot(1, H1_3, color='#0343df', marker='.', markersize=12)

# He3=H3+He3 & He4 = B8+He4
He3_3 = (c.ejected_h3[plotc_index]+c.ejected_he3[plotc_index])/He3_solar_mass_frac
He4_3 = (c.ejected_b8[plotc_index]+c.ejected_he4[plotc_index])/He4_solar_mass_frac
ax.plot((3,4),(He3_3,He4_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# Li7= Li7+Be7
Li7_3 = (c.ejected_li7[plotc_index]+c.ejected_be7[plotc_index])/Li7_solar_mass_frac
ax.plot(7,Li7_3,color='#0343df',marker='.', markersize=12)

# C12 = N12+C12 & C13 = N13+O13+C13
C12_3 = (c.ejected_n12[plotc_index]+c.ejected_c12[plotc_index])/C12_solar_mass_frac
C13_3 = (c.ejected_n13[plotc_index]+c.ejected_o13[plotc_index]+c.ejected_c13[plotc_index])/C13_solar_mass_frac
ax.plot((12,13),(C12_3,C13_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# N14 = O14+N14 & N15 = O15+N15
N14_3 = (c.ejected_o14[plotc_index]+c.ejected_n14[plotc_index])/N14_solar_mass_frac
N15_3 = (c.ejected_o15[plotc_index]+c.ejected_n15[plotc_index])/N15_solar_mass_frac
ax.plot((14,15),(N14_3,N15_3),linewidth='1',color='#0343df',marker='.', markersize=12)


# O16 & O17 = F17+O17 & O18 = F18+Ne18+O18
O16_3 = (c.ejected_o16[plotc_index])/O16_solar_mass_frac
O17_3 = (c.ejected_f17[plotc_index]+c.ejected_o17[plotc_index])/O17_solar_mass_frac
O18_3 = (c.ejected_f18[plotc_index]+c.ejected_ne18[plotc_index]+c.ejected_o18[plotc_index])/O18_solar_mass_frac
ax.plot((16,17,18),(O16_3,O17_3,O18_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# F19 = Ne19+F19
F19_3 = (c.ejected_ne19[plotc_index]+c.ejected_f19[plotc_index])/F19_solar_mass_frac
ax.plot(19, F19_3, color='#0343df', marker='.', markersize=12) # F19 = Ne19+F19

# Ne20 = Na20+ne20 & Ne21 = Na21+Mg21+Ne21 & Ne22 = Mg22+Na22+Ne22
Ne20_3 = (c.ejected_na20[plotc_index]+c.ejected_ne20[plotc_index])/Ne20_solar_mass_frac
Ne21_3 = (c.ejected_na21[plotc_index]+c.ejected_mg21[plotc_index]+c.ejected_ne21[plotc_index])/Ne21_solar_mass_frac
Ne22_3 = (c.ejected_mg22[plotc_index]+c.ejected_na22[plotc_index]+c.ejected_ne22[plotc_index])/Ne22_solar_mass_frac
ax.plot((20,21,22),(Ne20_3,Ne21_3,Ne22_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# Na23 = Al23+Mg23+Na23
Na23_3 = (c.ejected_al23[plotc_index]+c.ejected_mg23[plotc_index]+c.ejected_na23[plotc_index])/Na23_solar_mass_frac
ax.plot(23, Na23_3, color='#0343df', marker='.', markersize=12)

# Mg24 = Al24+Mg24 &  Mg25 = Al25+Si25+Mg25 & Mg26 = Al26+Si26+Mg26
Mg24_3 = (c.ejected_al24[plotc_index]+c.ejected_mg24[plotc_index])/Mg24_solar_mass_frac
Mg25_3 = (c.ejected_al25[plotc_index]+c.ejected_si25[plotc_index]+c.ejected_mg25[plotc_index])/Mg25_solar_mass_frac
Mg26_3 = (c.ejected_mg26[plotc_index]+c.ejected_al26[plotc_index]+c.ejected_si26[plotc_index])/Mg26_solar_mass_frac
ax.plot((24,25,26),(Mg24_3,Mg25_3,Mg26_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# Al27 = Si27+P27+Al27
Al27_3 = (c.ejected_si27[plotc_index]+c.ejected_p27[plotc_index]+c.ejected_al27[plotc_index])/Al27_solar_mass_frac
ax.plot(27, Al27_3, color='#0343df', marker='.', markersize=12) 

# Si28 = P28+Si28 & Si29 = P29+S29+Si29 & Si30 = P30+S30+Si30
Si28_3 = (c.ejected_p28[plotc_index]+c.ejected_si28[plotc_index])/Si28_solar_mass_frac
Si29_3 = (c.ejected_p29[plotc_index]+c.ejected_s29[plotc_index]+c.ejected_si29[plotc_index])/Si29_solar_mass_frac
Si30_3 = (c.ejected_p30[plotc_index]+c.ejected_s30[plotc_index]+c.ejected_si30[plotc_index])/Si30_solar_mass_frac
ax.plot((28,29,30),(Si28_3,Si29_3,Si30_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# P31 = S31+Cl31+P31
P31_3 = (c.ejected_s31[plotc_index]+c.ejected_cl31[plotc_index]+c.ejected_p31[plotc_index])/P31_solar_mass_frac
ax.plot(31, P31_3, color='#0343df', marker='.', markersize=12) 

# S32 = Cl32+Ar32+S32 & S33 = Cl33+Ar33+S33 & S34 = Cl34+Ar34+S34
S32_3 = (c.ejected_cl32[plotc_index]+c.ejected_ar32[plotc_index]+c.ejected_s32[plotc_index])/S32_solar_mass_frac
S33_3 = (c.ejected_cl33[plotc_index]+c.ejected_ar33[plotc_index]+c.ejected_s33[plotc_index])/S33_solar_mass_frac
S34_3 = (c.ejected_cl34[plotc_index]+c.ejected_ar34[plotc_index]+c.ejected_s34[plotc_index])/S34_solar_mass_frac
ax.plot((32,33,34),(S32_3,S33_3,S34_3),linewidth='1',color='#0343df',marker='.', markersize=12)


# Cl35 = Ar35+K35+S35+Cl35 &  Cl37 = Ar37+K37+Ca37+Cl37
Cl35_3 = (c.ejected_ar35[plotc_index]+c.ejected_k35[plotc_index]+c.ejected_s35[plotc_index]+c.ejected_cl35[plotc_index])/Cl35_solar_mass_frac
Cl37_3 = (c.ejected_ar37[plotc_index]+c.ejected_k37[plotc_index]+c.ejected_ca37[plotc_index]+c.ejected_cl37[plotc_index])/Cl37_solar_mass_frac
ax.plot((35,37),(Cl35_3,Cl37_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# Ar36 = K36+Ca36+Ar36+Cl36 & Ar38 = K38+Ca38+Ar38 
Ar36_3 = (c.ejected_k36[plotc_index]+c.ejected_ca36[plotc_index]+c.ejected_ar36[plotc_index]+c.ejected_cl36[plotc_index])/Ar36_solar_mass_frac
Ar38_3 = (c.ejected_k38[plotc_index]+c.ejected_ca38[plotc_index]+c.ejected_ar38[plotc_index])/Ar38_solar_mass_frac
ax.plot((36,38),(Ar36_3,Ar38_3),linewidth='1',color='#0343df',marker='.', markersize=12)

# K39 = Ca39+K39+Ar39
K39_3 = (c.ejected_ca39[plotc_index]+c.ejected_k39[plotc_index]+c.ejected_ar39[plotc_index])/K39_solar_mass_frac
ax.plot(39,K39_3,linewidth='1',color='#0343df',marker='.', markersize=12)

# Ca40 = Sc40+K40+Ca40
Ca40_3 = (c.ejected_ca40[plotc_index]+c.ejected_k40[plotc_index]+c.ejected_sc40[plotc_index])/Ca40_solar_mass_frac
ax.plot(40,Ca40_3,linewidth='1',color='#0343df',marker='.', markersize=12)


# ------------------------------------------------------------------------------------------------------------------------------------------------------



H1_list = [H1_1,H1_2,H1_3]
H1 = max(H1_list)
ax.text(1+x_centre_offset, H1+H1*y_offset_top, 'H', fontsize=13)
He4_list = [He4_1,He4_2,He4_3]
He4 = max(He4_list)
ax.text(4+x_centre_offset, He4+He4*y_offset_top, 'He', fontsize=13)
Li7_list = [Li7_1,Li7_2,Li7_3]
Li7 = max(Li7_list)
ax.text(7+x_centre_offset, Li7+Li7*y_offset_top, 'Li', fontsize=13)
C13_list = [C13_1,C13_2,C13_3]
C13 = max(C13_list)
ax.text(13+x_centre_offset_2, C13+C13*y_offset_top, 'C', fontsize=13)
N15_list = [N15_1,N15_2,N15_3]
N15 = max(N15_list)
ax.text(15+x_centre_offset_2, N15+N15*y_offset_top, 'N', fontsize=13)
O17_list = [O17_1,O17_2,O17_3]
O17 = max(O17_list)
ax.text(17+x_centre_offset, O17+O17*y_offset_top, 'O', fontsize=13)
F19_list = [F19_1,F19_2,F19_3]
F19 = max(F19_list)
ax.text(19+x_centre_offset_2, F19+F19*y_offset_top, 'F', fontsize=13)
Ne21_list = [Ne21_1,Ne21_2,Ne21_3]
Ne21 = min(Ne21_list)
ax.text(21+x_centre_offset, Ne21-Ne21*y_offset_bot, 'Ne', fontsize=13)
Na23_list = [Na23_1,Na23_2,Na23_3]
Na23 = max(Na23_list)
ax.text(23+x_centre_offset, Na23+Na23*y_offset_top, 'Na', fontsize=13)
Mg25_list = [Mg25_1,Mg25_2,Mg25_3]
Mg25 = max(Mg25_list)
ax.text(25+x_centre_offset, Mg25+Mg25*y_offset_top, 'Mg', fontsize=13)
Al27_list = [Al27_1,Al27_2,Al27_3]
Al27 = max(Al27_list)
ax.text(27+x_centre_offset, Al27+Al27*y_offset_top, 'Al', fontsize=13)
Si29_list = [Si29_1,Si29_2,Si29_3]
Si29 = min(Si29_list)
ax.text(29+x_centre_offset, Si29-Si29*y_offset_bot, 'Si', fontsize=13)
P31_list = [P31_1,P31_2,P31_3]
P31 = max(P31_list)
ax.text(31+x_centre_offset, P31+P31*y_offset_top, 'P', fontsize=13)
S33_list = [S33_1,S33_2,S33_3]
S33 = max(S33_list)
ax.text(33+x_centre_offset, S33+S33*y_offset_top, 'S', fontsize=13)
Cl37_list = [Cl37_1,Cl37_2,Cl37_3]
Cl37 = max(Cl37_list)
ax.text(37+x_centre_offset, Cl37+Cl37*y_offset_top, 'Cl', fontsize=13)
Ar38_list = [Ar38_1,Ar38_2,Ar38_3]
Ar38 = max(Ar38_list)
ax.text(38+x_centre_offset, Ar38+Ar38*y_offset_top, 'Ar', fontsize=13)
K39_list = [K39_1,K39_2,K39_3]
K39 = max(K39_list)
ax.text(39+x_centre_offset, K39+K39*y_offset_top, 'K', fontsize=13)
Ca40_list = [Ca40_1,Ca40_2,Ca40_3]
Ca40 = max(Ca40_list)
ax.text(40+x_centre_offset, Ca40+Ca40*y_offset_top, 'Ca', fontsize=13)




legend_elements = [Line2D([0], [0], marker='s', color='tab:orange', label='Burst 1',
                          markersize=7),
                   Line2D([0], [0], marker='^', color='black', label='Burst 2',
                          markersize=9),
				   Line2D([0], [0], marker='.', color='#0343df', label='Burst 3',
                          markersize=13)]
ax.legend(handles=legend_elements, frameon=False, loc='lower right',prop={'size': 18})


ax.set_xscale('linear')
ax.set_yscale('log')
ax.set_xlim(0,41)
ax.set_yticks([1e-10,1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1,1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10]) #replaces tick labels from '10^x' to 'x'
ax.set_yticklabels(['-10','-9','-8','-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10']) #replaces tick labels from '10^x' to 'x'
ax.tick_params(axis='x', labelsize=16)
ax.tick_params(axis='y', labelsize=16)
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.grid(which='major', linestyle=':', linewidth='1')
plt.minorticks_on()
ax.grid(which='minor', linestyle=':', linewidth='0.4')
ax.hlines(1, 0, 50, color='#000000', linestyle='--', linewidth=0.5)

ax.set_xlabel('Mass number', fontsize=20)
ax.set_ylabel('Mean overproduction factor $\\mathrm{log_{10}}\\left(\\dfrac{X_{i}}{X_{i,\\odot}}\\right)$', fontsize=20)
# ax.set_ylabel('Mean Overproduction Factor $\\mathrm{\\frac{X_{i}}{X_{i,\\odot}}}$', fontsize=16)


plt.show()
# plt.savefig('Asplund_overproduction_plot_triple_comparison.pdf', dpi=500)






# Elements not considered when graphing due to not being interested in their abundance
# h2 - Stable
# li6 -  Stable
# be9 - Stable
# b10 - Stable
# b11 - Stable
# c9 - decays to B9 which decyas to Be9 which we're not interested in
# c11 - T1/2 = 30.364 min, decays to B11 which we're not interested in
# s36 - Stable
# ar40 - Stable

