import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
from matplotlib.lines import Line2D
import fastmesareader as fmr

plotting_tings = 'ejected_h1 ejected_h2 ejected_h3 ejected_he3 ejected_he4 ejected_li6 ejected_li7 ejected_be7 ejected_be9 ejected_b8 ejected_b10 ejected_b11 ejected_c9 ejected_c11 ejected_c12 ejected_c13 ejected_n12 ejected_n13 ejected_n14 ejected_n15 ejected_o13 ejected_o14 ejected_o15 ejected_o16 ejected_o17 ejected_o18 ejected_f17 ejected_f18 ejected_f19 ejected_ne18 ejected_ne19 ejected_ne20 ejected_ne21 ejected_ne22 ejected_na20 ejected_na21 ejected_na22 ejected_na23 ejected_mg21 ejected_mg22 ejected_mg23 ejected_mg24 ejected_mg25 ejected_mg26 ejected_al23 ejected_al24 ejected_al25 ejected_al26 ejected_al27 ejected_si25 ejected_si26 ejected_si27 ejected_si28 ejected_si29 ejected_si30 ejected_p27 ejected_p28 ejected_p29 ejected_p30 ejected_p31 ejected_s29 ejected_s30 ejected_s31 ejected_s32 ejected_s33 ejected_s34 ejected_s35 ejected_s36 ejected_cl31 ejected_cl32 ejected_cl33 ejected_cl34 ejected_cl35 ejected_cl36 ejected_cl37 ejected_ar32 ejected_ar33 ejected_ar34 ejected_ar35 ejected_ar36 ejected_ar37 ejected_ar38 ejected_ar39 ejected_ar40 ejected_k35 ejected_k36 ejected_k37 ejected_k38 ejected_k39 ejected_k40 ejected_ca36 ejected_ca37 ejected_ca38 ejected_ca39 ejected_ca40 ejected_sc40'
h = fmr.FastMesaReader('LOGS/history.data', plotting_tings)    
row_index = -1 # the row of data in history.data to plot from.

fig = plt.figure(figsize=(12.8,9.6))
ax = fig.add_subplot(111)	

x_centre_offset = -0.2
y_offset_top = 0.4

# H1
H1 = (h.ejected_h1[row_index])/7.0945477357E-01
ax.plot(1, H1, color='#0343df', marker='.', markersize=12)
ax.text(1+x_centre_offset, H1+H1*y_offset_top, 'H', fontsize=13)

# He3=H3+He3 & He4 = B8+He4
He3 = (h.ejected_h3[row_index]+h.ejected_he3[row_index])/8.4641515456E-05
He4 = (h.ejected_b8[row_index]+h.ejected_he4[row_index])/2.7501644504E-01
ax.plot((3,4),(He3,He4),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(4+x_centre_offset, He4+He4*y_offset_top, 'He', fontsize=13)

# Li7= Li7+Be7
Li7 = (h.ejected_li7[row_index]+h.ejected_be7[row_index])/9.8556690166E-09
ax.plot(7,Li7,color='#0343df',marker='.', markersize=12)
ax.text(7+x_centre_offset, Li7+Li7*y_offset_top, 'Li', fontsize=13)

# C12 = N12+C12 & C13 = N13+O13+C13
C12 = (h.ejected_n12[row_index]+h.ejected_c12[row_index])/2.3370919413E-03
C13 = (h.ejected_n13[row_index]+h.ejected_o13[row_index]+h.ejected_c13[row_index])/2.8452149548E-05
ax.plot((12,13),(C12,C13),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(13+x_centre_offset, C13+C13*y_offset_top, 'C', fontsize=13)

# N14 = O14+N14 & N15 = O15+N15
N14 = (h.ejected_o14[row_index]+h.ejected_n14[row_index])/8.1299682160E-04
N15 = (h.ejected_o15[row_index]+h.ejected_n15[row_index])/3.1966552925E-06
ax.plot((14,15),(N14,N15),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(15+x_centre_offset, N15-N15*y_offset_top, 'N', fontsize=13)


# O16 & O17 = F17+O17 & O18 = F18+Ne18+O18
O16 = (h.ejected_o16[row_index])/6.8808895413E-03
O17 = (h.ejected_f17[row_index]+h.ejected_o17[row_index])/2.7474252428E-06
O18 = (h.ejected_f18[row_index]+h.ejected_ne18[row_index]+h.ejected_o18[row_index])/1.5531307205E-05
ax.plot((16,17,18),(O16,O17,O18),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(17+x_centre_offset, O17+O17*y_offset_top, 'O', fontsize=13)

# F19 = Ne19+F19
F19 = (h.ejected_ne19[row_index]+h.ejected_f19[row_index])/4.1844135602E-07
ax.plot(19, F19, color='#0343df', marker='.', markersize=12) # F19 = Ne19+F19
ax.text(19+x_centre_offset, F19+F19*y_offset_top, 'F', fontsize=13)

# Ne20 = Na20+ne20 & Ne21 = Na21+Mg21+Ne21 & Ne22 = Mg22+Na22+Ne22
Ne20 = (h.ejected_na20[row_index]+h.ejected_ne20[row_index])/1.6763950634E-03
Ne21 = (h.ejected_na21[row_index]+h.ejected_mg21[row_index]+h.ejected_ne21[row_index])/4.2164622894E-06
Ne22 = (h.ejected_mg22[row_index]+h.ejected_na22[row_index]+h.ejected_ne22[row_index])/1.3559077719E-04
ax.plot((20,21,22),(Ne20,Ne21,Ne22),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(21+x_centre_offset, Ne21-Ne21*y_offset_top, 'Ne', fontsize=13)

# Na23 = Al23+Mg23+Na23
Na23 = (h.ejected_al23[row_index]+h.ejected_mg23[row_index]+h.ejected_na23[row_index])/3.6352024324E-05
ax.plot(23, Na23, color='#0343df', marker='.', markersize=12)
ax.text(23+x_centre_offset, Na23+Na23*y_offset_top, 'Na', fontsize=13)

# Mg24 = Al24+Mg24 &  Mg25 = Al25+Si25+Mg25 & Mg26 = Al26+Si26+Mg26
Mg24 = (h.ejected_al24[row_index]+h.ejected_mg24[row_index])/5.3250196132E-04
Mg25 = (h.ejected_al25[row_index]+h.ejected_si25[row_index]+h.ejected_mg25[row_index])/7.0534596214E-05
Mg26 = (h.ejected_mg26[row_index]+h.ejected_al26[row_index]+h.ejected_si26[row_index])/8.0477919874E-05
ax.plot((24,25,26),(Mg24,Mg25,Mg26),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(25+x_centre_offset, Mg25+Mg25*y_offset_top, 'Mg', fontsize=13)

# Al27 = Si27+P27+Al27
Al27 = (h.ejected_si27[row_index]+h.ejected_p27[row_index]+h.ejected_al27[row_index])/6.2568980455E-05
ax.plot(27, Al27, color='#0343df', marker='.', markersize=12) 
ax.text(27+x_centre_offset, Al27+Al27*y_offset_top, 'Al', fontsize=13)

# Si28 = P28+Si28 & Si29 = P29+S29+Si29 & Si30 = P30+S30+Si30
Si28 = (h.ejected_p28[row_index]+h.ejected_si28[row_index])/7.0715383917E-04
Si29 = (h.ejected_p29[row_index]+h.ejected_s29[row_index]+h.ejected_si29[row_index])/3.7176525818E-05
Si30 = (h.ejected_p30[row_index]+h.ejected_s30[row_index]+h.ejected_si30[row_index])/2.5392454637E-05
ax.plot((28,29,30),(Si28,Si29,Si30),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(29+x_centre_offset, Si29-Si29*y_offset_top, 'Si', fontsize=13)

# P31 = S31+Cl31+P31
P31 = (h.ejected_s31[row_index]+h.ejected_cl31[row_index]+h.ejected_p31[row_index])/7.0479812061E-06
ax.plot(31, P31, color='#0343df', marker='.', markersize=12) 
ax.text(31+x_centre_offset, P31-P31*y_offset_top, 'P', fontsize=13)

# S32 = Cl32+Ar32+S32 & S33 = Cl33+Ar33+S33 & S34 = Cl34+Ar34+S34
S32 = (h.ejected_cl32[row_index]+h.ejected_ar32[row_index]+h.ejected_s32[row_index])/3.5061857535E-04
S33 = (h.ejected_cl33[row_index]+h.ejected_ar33[row_index]+h.ejected_s33[row_index])/2.8564457061E-06
S34 = (h.ejected_cl34[row_index]+h.ejected_ar34[row_index]+h.ejected_s34[row_index])/1.6577684516E-05
ax.plot((32,33,34),(S32,S33,S34),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(33+x_centre_offset, S33-S33*y_offset_top, 'S', fontsize=13)


# Cl35 = Ar35+K35+S35+Cl35 &  Cl37 = Ar37+K37+Ca37+Cl37
Cl35 = (h.ejected_ar35[row_index]+h.ejected_k35[row_index]+h.ejected_s35[row_index]+h.ejected_cl35[row_index])/3.7581928546E-06
Cl37 = (h.ejected_ar37[row_index]+h.ejected_k37[row_index]+h.ejected_ca37[row_index]+h.ejected_cl37[row_index])/1.2668835242E-06
ax.plot((35,37),(Cl35,Cl37),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(37+x_centre_offset, Cl37+Cl37*y_offset_top, 'Cl', fontsize=13)

# Ar36 = K36+Ca36+Ar36+Cl36 & Ar38 = K38+Ca38+Ar38 
Ar36 = (h.ejected_k36[row_index]+h.ejected_ca36[row_index]+h.ejected_ar36[row_index]+h.ejected_cl36[row_index])/7.7311395865E-05
Ar38 = (h.ejected_k38[row_index]+h.ejected_ca38[row_index]+h.ejected_ar38[row_index])/1.4884854207E-05
ax.plot((36,38),(Ar36,Ar38),linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(38+x_centre_offset, Ar38+Ar38*y_offset_top, 'Ar', fontsize=13)

# K39 = Ca39+K39+Ar39
K39 = (h.ejected_ca39[row_index]+h.ejected_k39[row_index]+h.ejected_ar39[row_index])/3.7390184012E-06
ax.plot(39,K39,linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(39+x_centre_offset, K39+K39*y_offset_top, 'K', fontsize=13)

# Ca40 = Sc40+K40+Ca40
Ca40 = (h.ejected_ca40[row_index]+h.ejected_k40[row_index]+h.ejected_sc40[row_index])/6.4097458307E-05
ax.plot(40,Ca40,linewidth='1',color='#0343df',marker='.', markersize=12)
ax.text(40+x_centre_offset, Ca40+Ca40*y_offset_top, 'Ca', fontsize=13)


# legend_elements = [Line2D([0], [0], marker='.', color='#0343df', label='Burst 1',
#                           markersize=5)]
# ax.legend(handles=legend_elements, loc='lower right')

ax.set_xscale('linear')
ax.set_yscale('log')
ax.set_xlim(0,41)
ax.set_yticks([1e-10,1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1,1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10]) #replaces tick labels from '10^x' to 'x'
ax.set_yticklabels(['-10','-9','-8','-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10']) #replaces tick labels from '10^x' to 'x'
ax.tick_params(axis='x', labelsize=16)
ax.tick_params(axis='y', labelsize=16)
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.grid(which='major', linestyle=':', linewidth='1')
plt.minorticks_on()
ax.grid(which='minor', linestyle=':', linewidth='0.4')
ax.hlines(1, 0, 50, color='#000000', linestyle='--', linewidth=0.5)

ax.set_xlabel('Mass Number', fontsize=20)
ax.set_ylabel('Mean Overproduction Factor $\\mathrm{log_{10}}\\left(\\dfrac{X_{i}}{X_{i,\\odot}}\\right)$', fontsize=20)
# ax.set_ylabel('Mean Overproduction Factor $\\mathrm{\\frac{X_{i}}{X_{i,\\odot}}}$', fontsize=16)


# plt.show()
plt.savefig('Lodders09_overproduction_plot.pdf', dpi=500)






# Elements not considered when graphing due to not being interested in their abundance
# h2 - Stable
# li6 -  Stable
# be9 - Stable
# b10 - Stable
# b11 - Stable
# c9 - decays to B9 which decyas to Be9 which we're not interested in
# c11 - T1/2 = 30.364 min, decays to B11 which we're not interested in
# s36 - Stable
# ar40 - Stable



# Solar Composition Values:
# Taken from Lodders09.data within $MESA_dir/data/chem_data/Lodders09.data
# H1 = 7.0945477357E-01
# He3 = 8.4641515456E-05
# He4 = 2.7501644504E-01
# Li7 = 9.8556690166E-09
# C12 = 2.3370919413E-03
# C13 = 2.8452149548E-05
# N14 = 8.1299682160E-04
# N15 = 3.1966552925E-06
# O16 = 6.8808895413E-03
# O17 = 2.7474252428E-06
# O18 = 1.5531307205E-05
# F19 = 4.1844135602E-07
# Ne21 = 4.2164622894E-06
# Na23 = 3.6352024324E-05
# Mg24 = 5.3250196132E-04
# Mg25 = 7.0534596214E-05
# Al27 = 6.2568980455E-05
# Si29 = 3.7176525818E-05
# Si30 = 2.5392454637E-05
# P31 = 7.0479812061E-06
# S32 = 3.5061857535E-04
# S33 = 2.8564457061E-06
# S34 = 1.6577684516E-05
# Cl35 = 3.7581928546E-06
# Cl37 = 1.2668835242E-06
# Ar36 = 7.7311395865E-05
# Ar38 = 1.4884854207E-05
# K39 = 3.7390184012E-06
# Ca40 = 6.4097458307E-05