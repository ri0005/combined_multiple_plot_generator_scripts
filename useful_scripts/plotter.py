import standard_plotting_package
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr

plotting_vars = ''
a = fmr.FastMesaReader('LOGS/history.data', plotting_vars)

fig = plt.figure()
ax = fig.add_subplot(111)   

ax.plot(a. , a. , label='') # linestyle=, color= ,

# fig.tight_layout()
ax.grid(which='major', linestyle=':', linewidth='0.8')
ax.grid(which='minor', linestyle=':', linewidth='0.3')

# ax.set_xscale('log')
# ax.set_yscale('log')

ax.set_xlabel('')
ax.set_xlabel('')
# ax.set_title('')
# ax.legend() #title='', loc='upper left', ncol=2


# OPTIONALS
# --- Replaces tick labels from '10^x' to 'x' ---
# ax.set_xticks([1e-10,1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1,1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10]) 
# ax.set_xticklabels(['-10','-9','-8','-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10'])
# ax.set_yticks([1e-10,1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1,1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10]) 
# ax.set_yticklabels(['-10','-9','-8','-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10'])

# --- Stop sig figs at top of plot ---
# ax.ticklabel_format(axis='both', style='plain', scilimits=(0,0), useOffset=False)

# --- Make X-axis ticks not visible ---
# plt.setp(ax.get_xticklabels(), visible=False)

# --- Make Y-axis ticks not visible ---
# plt.setp(ax.get_yticklabels(), visible=False)

# --- To have Y-axis ticks in scientific notation ---
# f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
# g = lambda x,pos : "${}$".format(f._formatSciNotation('%1.10e' % x))
# plt.gca().yaxis.set_major_formatter(mticker.FuncFormatter(g))

# --- Colorblind friendly pallete ---
# CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a', '#f781bf', '#a65628', '#984ea3', '#999999', '#e41a1c', '#dede00']

# ax.set_xlim()
# ax.set_ylim()

plt.show()
# plt.savefig('xxx.pdf', dpi=500)