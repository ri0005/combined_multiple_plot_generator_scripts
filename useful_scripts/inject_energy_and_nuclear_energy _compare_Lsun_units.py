import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr

plotting_tings = 'star_age luminosity radius_cm star_mass model_number max_T v_surf mass_conv_core cz_scale_height max_eps_nuc total_energy_erg total_extra_energy_erg'

a = fmr.FastMesaReader('../LOGS_inject_energy/history.data', plotting_tings)	
b = fmr.FastMesaReader('../LOGS_do_mix/history.data', plotting_tings)	
# a = fmr.FastMesaReader('LOGS_extra_energy_1d12/history.data', plotting_tings)	

# a = fmr.FastMesaReader('../standard_MLT_test/LOGS/history.data', plotting_tings)	


fig = plt.figure(figsize=(12.8,9.6))
ax = fig.add_subplot(111)

do_mix_energy_list = b.total_energy_erg
total_do_mix_energy = do_mix_energy_list[-1]

star_age_list = b.star_age
star_age_sec = star_age_list[-1]*365*24*3600

ax.plot(b.star_age*365*24, ((b.total_energy_erg)/star_age_sec)/3.828e33, color='k', linewidth='1')
ax.plot(a.star_age*365*24, ((a.total_energy_erg+total_do_mix_energy)/star_age_sec)/3.828e33, color='k', linewidth='1', label='$\\sum \\epsilon_{\\mathrm{nuc}}$')
ax.plot(a.star_age*365*24, (a.total_extra_energy_erg/star_age_sec)/3.828e33, linewidth='1', label='$\\sum E_{\\mathrm{inject}}$')


ax.legend(prop={'size': 15}) #(loc='upper left')

ax.set_xscale('log')
ax.set_yscale('log')

ax.set_xlabel('Star Age after Diffusive Convective Mixing [hr]', fontsize=16)
ax.set_ylabel('Luminosity [$\\mathrm{L_{\\odot}}$]', fontsize=16)

# ax.set_xlim(0,1)
# ax.set_ylim(0,1.1)

ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.grid(which='major', linestyle=':', linewidth='0.8')
plt.minorticks_on()
ax.grid(which='minor', linestyle=':', linewidth='0.4')

# plt.show()
plt.savefig('sum_nuclear_generated_energy_and_sum_injected_energy_in_Lsun.pdf', dpi=500)
