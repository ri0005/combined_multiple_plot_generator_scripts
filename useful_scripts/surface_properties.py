import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr

plotting_tings = 'star_age photosphere_cell_density photosphere_cell_T photosphere_cell_opacity'

a = fmr.FastMesaReader('LOGS/history.data', plotting_tings)	
# b = fmr.FastMesaReader('../standard_MLT_test/LOGS/history.data', plotting_tings)	


fig = plt.figure(figsize=(12.8,9.6))
ax = fig.add_subplot(111)


ax.ticklabel_format(axis='both', style='plain', scilimits=(0,0), useOffset=False)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.grid(which='major', linestyle=':', linewidth='0.8')
plt.minorticks_on()
ax.grid(which='minor', linestyle=':', linewidth='0.4')

# cm = plt.cm.get_cmap('viridis')
# sc = ax.scatter(np.log10(a.photosphere_cell_density), np.log10(a.photosphere_cell_T), c=a.star_age*525600, cmap=cm)
# ax.plot(np.log10(a.photosphere_cell_density), np.log10(a.photosphere_cell_T), linewidth='0.5', color='k')
# cbar = plt.colorbar(sc)
# ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
# ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
# cbar.set_label('Time Since Imposed Diffusive Mixing [min]', fontsize=14)
# plt.savefig('surface_graphs/surafce_temperature_vs_density_vs_time.png', dpi=500)

# cm = plt.cm.get_cmap('viridis')
# sc = ax.scatter(np.log10(a.photosphere_cell_density), np.log10(a.photosphere_cell_T), c=np.log10(a.photosphere_cell_opacity), cmap=cm)
# ax.plot(np.log10(a.photosphere_cell_density), np.log10(a.photosphere_cell_T), linewidth='0.5', color='k')
# cbar = plt.colorbar(sc)
# cbar.ax.minorticks_on()
# ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
# ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
# cbar.set_label('$\\mathrm{Log_{10}}(\kappa)$ [$\\mathrm{cm^{2}\,g^{-1}}$]', fontsize=14)
# plt.savefig('surface_graphs/surafce_opacity_vs_temperature_vs_density.png', dpi=500)

cm = plt.cm.get_cmap('viridis')
sc = ax.scatter(np.log10(a.photosphere_cell_density), np.log10(a.photosphere_cell_T), c=np.log10(a.photosphere_cell_opacity), cmap=cm)
ax.plot(np.log10(a.photosphere_cell_density), np.log10(a.photosphere_cell_T), linewidth='0.5', color='k')
cbar = plt.colorbar(sc)
cbar.ax.minorticks_on()
ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
cbar.set_label('', fontsize=14)
plt.savefig('surface_graphs/temperature_vs_opacity.png', dpi=500)




# plt.show()
