import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr
import pandas as pd


fig = plt.figure(figsize=(16,9.6))
ax = fig.add_subplot(111)

# df = pd.read_csv('before_opacity_bump.data', delim_whitespace=True, index_col=0)
# log_rho = np.asarray(df.columns, dtype=float)
# log_T = df.index.values
# opacity_data_in_df = df.to_numpy()

# cs = ax.contourf(log_rho, log_T, np.log10(opacity_data_in_df), levels=100, cmap='viridis', antialiased=True)
# for c in cs.collections:
#     c.set_rasterized(True)
# bar = fig.colorbar(cs, pad=0.02)
# bar.set_label('$\\mathrm{Log_{10}}(\kappa)$ [$\\mathrm{cm^{2}\,g^{-1}}$]', fontsize=14)
# bar.outline.set_visible(False)

# ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
# ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
# ax.set_title('Surface Z~0.20341, Before Surface Opacity Spike', fontsize=16)

# ax.tick_params(axis='x', labelsize=12)
# ax.tick_params(axis='y', labelsize=12)
# ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
# plt.minorticks_on()
# plt.savefig('surface_graphs/opacity_graphs_before_surface_opacity_spike.png', dpi=500)


# plotting_tings = 'star_age photosphere_cell_density photosphere_cell_T photosphere_cell_opacity'
# a = fmr.FastMesaReader('LOGS/history.data', plotting_tings) 
# df = pd.read_csv('after_opacity_bump.data', delim_whitespace=True, index_col=0)
# log_rho = np.asarray(df.columns, dtype=float)
# log_T = df.index.values
# opacity_data_in_df = df.to_numpy()

# cs = ax.contourf(log_rho, log_T, np.log10(opacity_data_in_df), levels=100, cmap='viridis', antialiased=True)
# for c in cs.collections:
#     c.set_rasterized(True)
# bar = fig.colorbar(cs, pad=0.02)
# bar.set_label('$\\mathrm{Log_{10}}(\kappa)$ [$\\mathrm{cm^{2}\,g^{-1}}$]', fontsize=14)
# bar.outline.set_visible(False)

# ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
# ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
# ax.set_title('Surface Z~0.20426, $\\mathrm{P_{extra}}=1$', fontsize=16)

# ax.tick_params(axis='x', labelsize=12)
# ax.tick_params(axis='y', labelsize=12)
# ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
# plt.minorticks_on()

# ax.scatter(x=-9.6714411174554655, y=4.9939351437534620, marker='x', color='black', s=100, label='Density Inversion Occurs')
# ax.plot(np.log10(a.photosphere_cell_density), np.log10(a.photosphere_cell_T), linewidth='1', color='black', label='Surface')
# ax.legend()
# plt.savefig('surface_graphs/opacity_graphs_with_surface_properties_overlayed.png', dpi=500)


# plotting_tings = 'density temperature'
# a = fmr.FastMesaReader('LOGS/profile56.data', plotting_tings) 
# df = pd.read_csv('before_opacity_bump.data', delim_whitespace=True, index_col=0)
# log_rho = np.asarray(df.columns, dtype=float)
# log_T = df.index.values
# opacity_data_in_df = df.to_numpy()
# cs = ax.contourf(log_rho, log_T, np.log10(opacity_data_in_df), levels=1000, cmap='viridis', antialiased=True)
# for c in cs.collections:
#     c.set_rasterized(True)
# bar = fig.colorbar(cs, pad=0.02)
# bar.set_label('$\\mathrm{Log_{10}}(\kappa)$ [$\\mathrm{cm^{2}\,g^{-1}}$]', fontsize=14)
# bar.outline.set_visible(False)
# ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
# ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
# ax.set_title('Surface Z~0.20426, $\\mathrm{P_{extra}}=1$', fontsize=16)
# ax.tick_params(axis='x', labelsize=12)
# ax.tick_params(axis='y', labelsize=12)
# ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
# plt.minorticks_on()
# # ax.scatter(x=-9.6714411174554655, y=4.9939351437534620, marker='x', color='black', s=100, label='Density Inversion Occurs')
# ax.plot(np.log10(a.density), np.log10(a.temperature), linewidth='1', color='black', label='Star Track')
# ax.legend()
# # plt.savefig('surface_graphs/opacity_graph_with_star_profile_before_inversion.png', dpi=500)
# ax.set_xlim(-10.5,-9)
# ax.set_ylim(4.8,5.5)
# plt.savefig('surface_graphs/opacity_graph_with_star_profile_before_inversion_zoom_on_surface.png', dpi=500)


# plotting_tings = 'density temperature'
# a = fmr.FastMesaReader('LOGS/profile57.data', plotting_tings) 
# df = pd.read_csv('after_opacity_bump.data', delim_whitespace=True, index_col=0)
# log_rho = np.asarray(df.columns, dtype=float)
# log_T = df.index.values
# opacity_data_in_df = df.to_numpy()
# cs = ax.contourf(log_rho, log_T, np.log10(opacity_data_in_df), levels=1000, cmap='viridis', antialiased=True)
# for c in cs.collections:
#     c.set_rasterized(True)
# bar = fig.colorbar(cs, pad=0.02)
# bar.set_label('$\\mathrm{Log_{10}}(\kappa)$ [$\\mathrm{cm^{2}\,g^{-1}}$]', fontsize=14)
# bar.outline.set_visible(False)
# ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
# ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
# ax.set_title('Surface Z~0.20426, $\\mathrm{P_{extra}}=1$', fontsize=16)
# ax.tick_params(axis='x', labelsize=12)
# ax.tick_params(axis='y', labelsize=12)
# ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
# plt.minorticks_on()
# # ax.scatter(x=-9.6714411174554655, y=4.9939351437534620, marker='x', color='black', s=100, label='Density Inversion Occurs')
# ax.plot(np.log10(a.density), np.log10(a.temperature), linewidth='1', color='black', label='Star Track')
# ax.legend()
# # plt.savefig('surface_graphs/opacity_graph_with_star_profile_after_inversion.png', dpi=500)
# ax.set_xlim(-10.5,-9)
# ax.set_ylim(4.8,5.5)
# plt.savefig('surface_graphs/opacity_graph_with_star_profile_after_inversion_zoom_on_surface.png', dpi=500)


plotting_tings = 'density temperature'
a = fmr.FastMesaReader('LOGS/profile56.data', plotting_tings) 
b = fmr.FastMesaReader('LOGS/profile57.data', plotting_tings) 
df = pd.read_csv('before_opacity_bump.data', delim_whitespace=True, index_col=0)
log_rho = np.asarray(df.columns, dtype=float)
log_T = df.index.values
opacity_data_in_df = df.to_numpy()
cs = ax.contourf(log_rho, log_T, np.log10(opacity_data_in_df), levels=1000, cmap='viridis', antialiased=True)
for c in cs.collections:
    c.set_rasterized(True)
bar = fig.colorbar(cs, pad=0.02)
bar.set_label('$\\mathrm{Log_{10}}(\kappa)$ [$\\mathrm{cm^{2}\,g^{-1}}$]', fontsize=14)
bar.outline.set_visible(False)
ax.set_xlabel('$\\mathrm{Log_{10}}(\\rho)$ [$\\mathrm{g\,cm^{-3}}$]', fontsize=14)
ax.set_ylabel('$\\mathrm{Log_{10}(T)}$ [K]', fontsize=14)
ax.set_title('Surface Z~0.20426, $\\mathrm{P_{extra}}=1$', fontsize=16)
ax.tick_params(axis='x', labelsize=12)
ax.tick_params(axis='y', labelsize=12)
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
plt.minorticks_on()
ax.plot(np.log10(a.density), np.log10(a.temperature), linewidth='1', color='black', label='before $\\rho$ inversion')
ax.plot(np.log10(b.density), np.log10(b.temperature), linewidth='1', color='red', label='after $\\rho$ inversion')
ax.legend()
# plt.savefig('surface_graphs/opacity_graph_before_after_inversion_compare.png', dpi=500)
ax.set_xlim(-10.5,-9)
ax.set_ylim(4.8,5.5)
cs.clim(vmin=0.3, vmax=1)
bar.clim(vmin=0.3, vmax=1)  
# plt.savefig('surface_graphs/opacity_graph_before_after_inversion_compare_zoom_on_surface.png', dpi=500)



plt.show()


'''

Density inversion at profle57.data. profile56.data does not have an inversion 
profile57.data:
    Age: 3.4986998091985647E-005
    Radius: 4.8326851229081075E-001
    model_number: 275
    logT: 4.9939351437534620
    logRho: -9.6714411174554655 

'''