import standard_plotting_package
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr

plotting_vars = 'star_age model_number total_extra_energy_erg total_energy_erg star_mass'
a = fmr.FastMesaReader('../LOGS/history.data', plotting_vars)
# b = fmr.FastMesaReader('../../no_dt_limit_and_standard_spatial_resolution_envelope_test/LOGS/history.data', plotting_vars)	


fig = plt.figure()
ax = fig.add_subplot(111)   

# re_index = 17800
# star_age = a.star_age[:re_index]
# total_extra_energy_erg = a.total_extra_energy_erg[:re_index]
# total_energy_erg = a.total_energy_erg[:re_index]

# arr_length = len(a.total_extra_energy_erg)
# total_extra_energy_erg = np.zeros(re_index)
# total_energy_erg = np.zeros(re_index)
# star_age = np.zeros(re_index)

# for i in range(re_index):
# 	total_extra_energy_erg[i] = a.total_extra_energy_erg[i]
# 	total_energy_erg[i] = a.total_energy_erg[i]
# 	star_age[i] = a.star_age[i]

# ax.plot(star_age*365.25, (total_extra_energy_erg)/(total_energy_erg), linewidth=1)
# ax.plot(a.star_age*365.25, (a.total_extra_energy_erg)/(a.total_energy_erg), linewidth=1)
ax.plot(a.model_number, a.total_extra_energy_erg/(a.total_energy_erg), linewidth=1)

# ax.plot(a.model_number, (a.star_mass), linewidth=1, label='a')
# ax.plot(b.model_number, (b.star_mass), linewidth=1, label='b')

# ax.plot(a.model_number, (a.total_extra_energy_erg), linewidth=1, label='a')
# ax.plot(b.model_number, (b.total_extra_energy_erg), linewidth=1, label='b')

ax.legend() #(loc='upper left')


# fig.tight_layout()
ax.grid(which='major', linestyle=':', linewidth='0.8')
ax.grid(which='minor', linestyle=':', linewidth='0.3')

# ax.set_xscale('log')
ax.set_yscale('log')

ax.set_xlabel('Star Age since IDF [day]')
ax.set_ylabel('$\\mathrm{Log_{10}}\\left( \\sum E_{\\mathrm{inject}}\\,/\\,\\sum \\epsilon_{\\mathrm{nuc}} \\right)$')

# ax.set_title('')
# ax.legend() #title='', loc='upper left', ncol=2


# OPTIONALS
# --- Replaces tick labels from '10^x' to 'x' ---
# ax.set_xticks([1e-10,1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,1,1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10]) 
# ax.set_xticklabels(['-10','-9','-8','-7','-6','-5','-4','-3','-2','-1','0','1','2','3','4','5','6','7','8','9','10'])
# ax.set_yticks([1e-9,1e-8,1e-7,1e-6,1e-5,1e-4,1e-3]) 
# ax.set_yticklabels(['-9','-8','-7','-6','-5','-4','-3'])

# --- Stop sig figs at top of plot ---
# ax.ticklabel_format(axis='both', style='plain', scilimits=(0,0), useOffset=False)

# --- Make X-axis ticks not visible ---
# plt.setp(ax.get_xticklabels(), visible=False)

# --- Make Y-axis ticks not visible ---
# plt.setp(ax.get_yticklabels(), visible=False)

# --- To have Y-axis ticks in scientific notation ---
# f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
# g = lambda x,pos : "${}$".format(f._formatSciNotation('%1.10e' % x))
# plt.gca().yaxis.set_major_formatter(mticker.FuncFormatter(g))

# --- Colorblind friendly pallete ---
# CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a', '#f781bf', '#a65628', '#984ea3', '#999999', '#e41a1c', '#dede00']

# ax.set_xlim()
# ax.set_ylim()

plt.show()
# plt.savefig('injected_energy_div_nuclear_energy.pdf', dpi=500)