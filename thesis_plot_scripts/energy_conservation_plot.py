import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr
from matplotlib.ticker import FormatStrFormatter

plot_vars = 'star_age model_number total_energy_start_of_step total_energy_of_model_after_mass_cut total_energy_of_cut_cells total_energy_old'

a = fmr.FastMesaReader('LOGS/history.data', plot_vars)	

fig = plt.figure(figsize=(12.8,9.6))
ax = fig.add_subplot(111)


# ax.plot(a.model_number-6000, ((a.total_energy_start_of_step-a.total_energy_of_cut_cells)/a.total_energy_old)-1e0, linewidth='1', label='Energy Beginning of Step')
# ax.set_ylabel('$\mathrm{\\frac{E_{start \\ model}-E_{cut \\ cells}}{E_{model \\ after \\ cut}}}-1$', fontsize=22)


ax.plot(a.model_number-6000, (1e0-(a.total_energy_of_model_after_mass_cut/a.total_energy_old)), linewidth='1.5')

ax.set_ylabel('$1-\mathrm{\\dfrac{estimated \\ energy \\ after \\ cut}{ actual \\ energy \\ after \\ cut}}$', fontsize=20)
# ax.set_ylabel('1-(calculated energy after cut / actual energy after cut)', fontsize=20)
ax.set_xlabel('Timestep number', fontsize=20)

ax.tick_params(axis='x', labelsize=16)
ax.tick_params(axis='y', labelsize=16)

# ax.ticklabel_format(axis='y', style='sci', scilimits=(0,0), useOffset=False)
ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.grid(which='major', linestyle=':', linewidth='0.8')
plt.minorticks_on()
ax.grid(which='minor', linestyle=':', linewidth='0.4')

f = mticker.ScalarFormatter(useOffset=False, useMathText=True)
g = lambda x,pos : "${}$".format(f._formatSciNotation('%1.10e' % x))
plt.gca().yaxis.set_major_formatter(mticker.FuncFormatter(g))

# ax.xaxis.set_minor_formatter(mticker.LogFormatterSciNotation(base=10.0, labelOnlyBase=False, minor_thresholds=(np.inf, np.inf)))
# plt.setp(ax.xaxis.get_minorticklabels(), rotation=90)

fig.tight_layout()
# plt.show()
# plt.savefig('energy_comparison.pdf', dpi=500)
plt.savefig('star_cut_hypothesised_vs_actual_energy_comparison.pdf', dpi=500)

