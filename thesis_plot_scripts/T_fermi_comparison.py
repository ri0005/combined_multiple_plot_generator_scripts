import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr

plotting_columns = 'mass Fermi_temperature abar zbar bernoulli_param q v_div_cs dm radius dr free_e dv_dt ionization_c12 energy tau total_energy v_div_vesc zone gradT dq pressure prad pgas radius_cm eps_nuc gradr grada luminosity log_Ledd velocity vel_km_per_s csound conv_vel mlt_mixing_type h1 he4 c12 n13 o16 temperature opacity density x_mass_fraction_H y_mass_fraction_He z_mass_fraction_metals cell_KE inject_energy'

melec = 9.1093837015e-31 #electron mass kg
mhyd = 1.673532838345678e-27 #mass of hydrogen in kg: mass in amu = 1.6605390671738466e-27 kg
hbar2 = 1.1121217172106814e-68 # J^2 s^2 hbar = 1.054571817d-34
boltzman_const = 1.3806485279e-23 #m^2 kg^ s^-2 K^-1
pi = 3.1415926535897932384626433832795028841971693993751e0
pi2 = pi * pi

#------------------------------------------------------------------------------------------------------------------------------
a = fmr.FastMesaReader('LOGS/profile128.data',  plotting_columns) #T = 7.213275583043994
mass_a = a.mass
c12_a = a.c12
counter_a = 0
for i, val in enumerate(mass_a):
	if c12_a[i] > 4.8e-1:
		WD_surface_a = mass_a[i+1]
		counter_a = i+1
		break
env_mass_a = mass_a[0]-WD_surface_a
print('WD_surface_a', WD_surface_a, 'env_mass_a', env_mass_a, 'max_T_a', max(a.temperature), 'counter_a', counter_a)
T_fermi_a = np.zeros(counter_a)
temperature_a = np.zeros(counter_a)
q_a = np.zeros(counter_a)
for i in range(counter_a):
	T_fermi_a[i] = ( (hbar2/(2*melec)) * ((3*pi2*(a.zbar[i]/a.abar[i])*a.density[i]*1e3 ) / mhyd)**(2e0/3e0) ) / boltzman_const
	temperature_a[i] = a.temperature[i]
	q_a[i] = (a.mass[i]-WD_surface_a)/env_mass_a

#------------------------------------------------------------------------------------------------------------------------------

b = fmr.FastMesaReader('LOGS/profile177.data',  plotting_columns) #T = 7.213275583043994
mass_b = b.mass
c12_b = b.c12
counter_b = 0
for i, val in enumerate(mass_b):
	if c12_b[i] > 4.8e-1:
		WD_surface_b = mass_b[i+1]
		counter_b = i+1
		break
env_mass_b = mass_b[0]-WD_surface_b
print('WD_surface_b', WD_surface_b, 'env_mass_b', env_mass_b, 'max_T_b', max(b.temperature), 'counter_b', counter_b)
T_fermi_b = np.zeros(counter_b)
temperature_b = np.zeros(counter_b)
q_b = np.zeros(counter_b)
for i in range(counter_b):
	T_fermi_b[i] = ( (hbar2/(2*melec)) * ((3*pi2*(b.zbar[i]/b.abar[i])*b.density[i]*1e3 ) / mhyd)**(2e0/3e0) ) / boltzman_const
	temperature_b[i] = b.temperature[i]
	q_b[i] = (b.mass[i]-WD_surface_b)/env_mass_b


#------------------------------------------------------------------------------------------------------------------------------

# fig, (ax1,ax2) = plt.subplots(2, sharex=True, figsize=(12.8,9.6)) 

# ax1.plot(1e0-q_a, T_fermi_a, linewidth='2', linestyle='dashed', label='$T_{\\mathrm{F}}$')
# ax1.plot(1e0-q_a, temperature_a, linewidth='2', label='$T_{\\mathrm{env}}$')

# ax2.plot(1e0-q_b, T_fermi_b, linewidth='2', linestyle='dashed', label='$T_{\\mathrm{F}}$')
# ax2.plot(1e0-q_b, temperature_b, linewidth='2', label='$T_{\\mathrm{env}}$')

# ax2.set_xlabel('Normalised envelope mass coordinate $\\left[\\frac{m}{M_{\\mathrm{env}}}\\right]$', fontsize=16)
# ax1.set_ylabel('Temperature [K]', fontsize=16)
# ax2.set_ylabel('Temperature [K]', fontsize=16)

# ax1.set_title('$T_{\\mathrm{env,max}}=1.6\\times10^{7}\,\\mathrm{K}$, $M_{\\mathrm{env}}=7.33\\times10^{-5}\,\\mathrm{M_{\\odot}}$', y=0.1, pad=-14, fontsize=16)
# ax2.set_title('$T_{\\mathrm{env,max}}=8\\times10^{8}\,\\mathrm{K}$, $M_{\\mathrm{env}}=8.23\\times10^{-5}\,\\mathrm{M_{\\odot}}$', y=0.1, pad=-14, fontsize=16)

# ax2.legend(fontsize=16, loc='lower left')
# # ax2.legend(fontsize=12) #(loc='upper left')

# ax1.set_xscale('log')
# ax2.set_xscale('log')
# ax1.set_yscale('log')
# ax2.set_yscale('log')

# # ax2.annotate('local max', xy=(1, 1), xycoords='figure fraction')

# # ax1.set_xlim(-0.01,1.01)
# # ax2.set_xlim(-0.01,1.01)
# # ax1.set_ylim(1e5,1.2e8)
# # ax2.set_ylim(1e5,1.2e8)

# fig.tight_layout()
# # plt.setp(ax1.get_xticklabels(), visible=False)
# ax1.tick_params(axis='y', labelsize=14)
# ax2.tick_params(axis='x', labelsize=14)
# ax2.tick_params(axis='y', labelsize=14)

# ax1.xaxis.set_minor_locator(mticker.AutoMinorLocator())
# ax1.grid(which='major', linestyle=':', linewidth='0.8')
# ax1.grid(which='minor', linestyle=':', linewidth='0.3')
# ax2.xaxis.set_minor_locator(mticker.AutoMinorLocator())
# ax2.grid(which='major', linestyle=':', linewidth='0.8')
# ax2.grid(which='minor', linestyle=':', linewidth='0.3')
# plt.minorticks_on()

# plt.show()
# # plt.savefig('envelope_fermi_temperatue_comparison_before_and_during_TNR.pdf', dpi=500)


fig = plt.figure(figsize=(12.8,9.6))
ax = fig.add_subplot(111)	

T_ferm_deg, = ax.plot(1e0-q_a, T_fermi_a, linewidth='2', color='#994F00', linestyle='dashed', label='$T_{\\mathrm{F}}$')
T_deg, = ax.plot(1e0-q_a, temperature_a, linewidth='2', color='#994F00', label='$T_{\\mathrm{env}}$')

T_ferm_nondeg, = ax.plot(1e0-q_b, T_fermi_b, linewidth='2',  color='#006CD1', linestyle='dashed', label='$T_{\\mathrm{F}}$')
T_nondeg, = ax.plot(1e0-q_b, temperature_b, linewidth='2',  color='#006CD1', label='$T_{\\mathrm{env}}$')

ax.set_xlabel('Normalised envelope mass coordinate $\\mathrm{Log_{10}}\\left(1-\\dfrac{m}{M_{\\mathrm{env}}}\\right)$', fontsize=20)
ax.set_ylabel('$\\mathrm{Log_{10}\\left(Temperature\,/\,K\\right)}$', fontsize=20)

ax.set_xscale('log')
ax.set_yscale('log')

ax.set_xlim(1e-3,1)
ax.set_ylim(1e6,1.2e8)

ax.set_xticks([1e-3,1e-2,1e-1,1]) #replaces tick labels from '10^x' to 'x'
ax.set_xticklabels(['-3','-2','-1','0']) #replaces tick labels from '10^x' to 'x'

ax.set_yticks([1e6,1e7,1e8]) #replaces tick labels from '10^x' to 'x'
ax.set_yticklabels(['6','7','8']) #replaces tick labels from '10^x' to 'x'

fig.tight_layout()
# plt.setp(ax.get_xticklabels(), visible=False)
ax.tick_params(axis='x', labelsize=16)
ax.tick_params(axis='y', labelsize=16)

# ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.grid(which='major', linestyle=':', linewidth='0.8')
ax.grid(which='minor', linestyle=':', linewidth='0.3')

leg_deg = ax.legend(handles=[T_ferm_deg, T_deg], frameon=False, fontsize=20, loc='lower right', title='Degenerate envelope: \n $T_{\\mathrm{env,max}}=1.6\\times10^{7}\,\\mathrm{K}$ \n $M_{\\mathrm{env}}=7.33\\times10^{-5}\,\\mathrm{M_{\\odot}}$')
leg_deg.get_title().set_fontsize('20')
leg_deg.get_title().set_color('#994F00')
for line, text in zip(leg_deg.get_lines(), leg_deg.get_texts()):
    text.set_color(line.get_color())
ax.add_artist(leg_deg)
leg_nondeg = ax.legend(handles=[T_ferm_nondeg, T_nondeg], frameon=False, fontsize=20, loc='upper left', title='Non-degenerate envelope: \n $T_{\\mathrm{env,max}}=10^{8}\,\\mathrm{K}$ \n $M_{\\mathrm{env}}=8.23\\times10^{-5}\,\\mathrm{M_{\\odot}}$',)
leg_nondeg.get_title().set_fontsize('20')
leg_nondeg.get_title().set_color('#006CD1')
for line, text in zip(leg_nondeg.get_lines(), leg_nondeg.get_texts()):
    text.set_color(line.get_color())

ax.annotate("Envelope surface", xy=(5e-2, 1.3e6), xytext=(1.01e-2, 1.3e6), fontsize=14)
ax.annotate("", xy=(1e-2, 1.2e6), xytext=(2.7e-2, 1.2e6), arrowprops={"arrowstyle":"->", "color":"black", 'lw':3})

ax.annotate("Envelope base", xy=(1e-1, 1.3e6), xytext=(4.1e-2, 1.3e6), fontsize=14)
ax.annotate("", xy=(1e-1, 1.2e6), xytext=(3.9e-2, 1.2e6), arrowprops={"arrowstyle":"->", "color":"black", 'lw':3})


# plt.show()
plt.savefig('envelope_fermi_temperatue_comparison_before_and_during_TNR.pdf', dpi=500)
