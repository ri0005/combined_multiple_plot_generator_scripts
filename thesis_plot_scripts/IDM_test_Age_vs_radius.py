import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr

plotting_tings = 'star_age radius_cm star_mass model_number max_T v_surf max_eps_nuc time_step_sec surf_escape_v'

a = fmr.FastMesaReader('LOGS_MLT/history.data', plotting_tings)
b = fmr.FastMesaReader('LOGS_0p5_1_msun/history.data', plotting_tings)
c = fmr.FastMesaReader('LOGS_0p6_1_msun/history.data', plotting_tings)
d = fmr.FastMesaReader('LOGS_0p7_1_msun/history.data', plotting_tings)
e = fmr.FastMesaReader('LOGS_0p8_1_msun/history.data', plotting_tings)
f = fmr.FastMesaReader('LOGS_0p9_1_msun/history.data', plotting_tings)

a_star_age = a.star_age
b_star_age = b.star_age
c_star_age = c.star_age
d_star_age = d.star_age
e_star_age = e.star_age
f_star_age = f.star_age

val = 5e6
a_star_age[a_star_age < val] = np.nan
b_star_age[b_star_age < val] = np.nan
c_star_age[c_star_age < val] = np.nan
d_star_age[d_star_age < val] = np.nan
e_star_age[e_star_age < val] = np.nan
f_star_age[f_star_age < val] = np.nan

fig = plt.figure(figsize=(12.8,9.6))
ax = fig.add_subplot(111)	
# ax = fig.add_subplot(111, title='1$\,\\mathrm{M_{\\odot}}$ $\\mathrm{Z=0.02}$ star from $4\\times10^{9}$ to $9\\times10^{9}$ yr past ZAMS')

CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']

ax.plot(a_star_age/1e9, a.radius_cm/6.957e10, linewidth='2', color=CB_color_cycle[0], label='MLT')
ax.plot(b_star_age/1e9, b.radius_cm/6.957e10, linewidth='2', color=CB_color_cycle[1], label='0.5-1$\,\\mathrm{M_{\odot}}$')
ax.plot(c_star_age/1e9, c.radius_cm/6.957e10, linewidth='2', color=CB_color_cycle[2], label='0.6-1$\,\\mathrm{M_{\odot}}$')
ax.plot(d_star_age/1e9, d.radius_cm/6.957e10, linewidth='2', color=CB_color_cycle[3], label='0.7-1$\,\\mathrm{M_{\odot}}$')
ax.plot(e_star_age/1e9, e.radius_cm/6.957e10, linewidth='2', color=CB_color_cycle[4], label='0.8-1$\,\\mathrm{M_{\odot}}$')
ax.plot(f_star_age/1e9, f.radius_cm/6.957e10, linewidth='2', color=CB_color_cycle[5], label='0.9-1$\,\\mathrm{M_{\odot}}$')

# ax.set_xscale('log')
# ax.set_yscale('log')
ax.ticklabel_format(axis='both', style='plain', scilimits=(0,0), useOffset=False)


legend = ax.legend(title='Imposed diffusive mixing region',fontsize=18, ncol=2) #(loc='upper left')
legend.get_title().set_fontsize('20')

ax.set_xlabel('Star age [Gyr]', fontsize=20)
# ax.set_xlabel('$\\mathrm{Log_{10}\\left(Age \,/\, Gyr\\right)}$', fontsize=20)
ax.set_ylabel('Radius [$\\mathrm{R_{\\odot}}$]', fontsize=20)

fig.tight_layout()
# plt.setp(ax.get_xticklabels(), visible=False)
ax.tick_params(axis='x', labelsize=16)
ax.tick_params(axis='y', labelsize=16)

# ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
ax.grid(which='major', linestyle=':', linewidth='0.8')
ax.grid(which='minor', linestyle=':', linewidth='0.3')

# ax.set_xticks([1e-2,1e-1,1,1e1]) #replaces tick labels from '10^x' to 'x'
# ax.set_xticklabels(['-2','-1','0','1']) #replaces tick labels from '10^x' to 'x'

plt.show()
# plt.savefig('IDM_test_Age_vs_radius.pdf', dpi=500)
