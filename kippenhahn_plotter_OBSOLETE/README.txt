__kippenhan_plotter__ 
The kippenhahn_plotter uses the fastmesareader principle of MESA data reading to create Kippenhahn plots. 

Use Instructions:
Write, but similar to fastmesareader in instance calling capabilities and comments on user interface make pretty self explanatory.

To Do:
convective, semiconvective, thermohaline, rotational mixed zones; nuclear-burning zones; a way of showing the different epsilons (nuc, thermal, gravitational).
Ability to be fed several directories from which to take files from for one same plot : keeping that is really key for my grids and it would be good if you have that with mkipp you have to provide directories in ascending order of time elapsed, maybe having an optional sorting bit of code would be useful, especially if you can change the x-axis variable it would be nice to change the colourbar when calling the plotting routine instead, depending on what you plot and what you want to compare it to
