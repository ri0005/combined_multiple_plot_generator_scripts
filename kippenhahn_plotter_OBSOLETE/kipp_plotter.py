import numpy as np
import Kippenhahn_data_extractor_and_plotter as extr
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
# from matplotlib.colors import LogNorm
# import matplotlib.ticker as mticker
import fastmesareader as fmr
# from benedict import benedict
# import math
# pd.set_option("precision", 18)

paths = ['../LOGS_v1=v2']

a = extr.Kippenhahn_Plotter(
            profiles_paths=paths,
            x_axis = 'star_age',              #either model_number or star_age
            y_axis = 'mass',
            z_axis = 'velocity',
            z_axis_divider = None, #'csound', #division by this is done first, then all below is carried out next. So if log_z_axis_data_for_dataframe is true, is is carried out on z_axis/z_axis_divider.
            read_profiles_index = True,
            # profile_number_start = 100, #1500,
            # profile_number_end = 300, #1530, #1530,
            auto_save_extracted_data_to_file = False, #if True save data extracted from profiles in one .data file in three columns: x_data, y_data, z_data respectively
            name_extracted_data_file = None, #'', #if not given, default name is 'profiles_profile_number_start-profile_number_end_x_axis_name-y_axis_name-z_axis_name.data'
            save_dataframe_as_csv = False,
            name_dataframe_csv = None, # '', #if not given, default name is 'dataframe_for_profiles_profile_number_start-profile_number_end_x_axis_name-y_axis_name-z_axis_name.csv'
            x_axis_divide_by = 1/525600,
            y_axis_divide_by = None,
            z_axis_data_min_cutoff = None, #0e0, #if any of the z_axis data elements are < z_axis_data_min_cutoff, then that z_axis data element = z_axis_data_min_cutoff_replacement
            z_axis_data_min_cutoff_replacement = None, #1e0, # read z_axis_data_min_cutoff for context, must have value if z_axis_data_min_cutoff has value
            z_axis_data_max_cutoff = None, #if any of the z_axis data elements are > z_axis_data_min_cutoff, then that z_axis data element = z_axis_data_max_cutoff_replacement
            z_axis_data_max_cutoff_replacement = None, # read z_axis_data_max_cutoff for context, must have value if z_axis_data_max_cutoff has value
            log_z_axis_data_for_dataframe = True, # If set true, then linear data will be logged. If z_axis_divider is used, then division will be logged.
            num_y_axis_elements_for_heatmap = 3000, #sets the resolution of the y_axis
            plot_heatmap = True,
            x_axis_label_heatmap = None,
            y_axis_label_heatmap = None,
            colorbar_label_heatmap = None,
            colorbar_min_heatmap = None,
            colorbar_max_heatmap = None,
            color_bar_scheme_heatmap = None, # If None default is 'viridis'
            save_heatmap_as_png = None # If you want heatmap saved as a png file, change this to name of the PNG file you want saved, else if None it won't be saved
            )


# # df = pd.read_csv('dataframe_for_profiles_1-200_model_number-mass-temperature.csv', index_col=0) #index_col=0 necessary when readind in a csv to ignore first numbered column
# a = fmr.FastMesaReader('../LOGS/history.data', 'model_number mass_conv_core')

# fig = plt.figure()
# ax = fig.add_subplot(111)

# # ax = sns.heatmap(data = df,
# #                 # norm=log_norm,
# #                 # cbar_kws={"ticks": cbar_ticks}
# #                 # vmin=8, 
# #                 # vmax=16,
# #                 cmap = 'viridis',
# #                 # mask=df.isnull(),
# #                 xticklabels='auto',
# #                 yticklabels='auto',
# #                 annot=False)
# # ax.invert_yaxis()

# ax.plot(a.model_number, a.mass_conv_core, linewidth='10') #, label='v_surf')

# plt.show()
# # plt.savefig('heatmap.png', dpi=500)





        # For x_axis_divide_by, use values below to convert x_axis into desired units.
        # "seconds" = 1/31536000
        # "minutes" = 1/525600
        # "hours" = 1/8760
        # "days" = 1/365
        # "Kyr" = 1e3
        # "Myr" = 1e6
        # "Gyr" = 1e9




















'''
# star_age = np.asarray(a.star_age)
# x_axis = star_age*525600
# y_axis = a.mass
# z_axis = a.eps_nuc

# np.savetxt('prof_1-200_star_age_min-mass-eps_nuc.data', np.c_[x_axis,y_axis,z_axis])
'''


'''
temp_data = []
x_axis = []
y_axis = []
z_axis = []

file = open("prof_1-800_star_age_min-mass-eps_nuc.data", 'r')
for line in file:
    temp_data = line.split()
    x_axis.append(float(temp_data[0]))
    y_axis.append(float(temp_data[1]))
    z_axis.append(float(temp_data[2]))

x_axis = np.array(x_axis)
y_axis = np.array(y_axis)
z_axis = np.array(z_axis)
#If logging y_axis:
z_axis[z_axis < 1e0] = 1e0 #np.nan
z_axis = np.log10(z_axis)
#else if linear y axis:
# z_axis[z_axis < 1e0] = 0e0 

len_of_ordered_x_axis = len(list(set(x_axis)))
ordered_y_points = sorted(list(set(y_axis)),reverse=False) #set() removes repeats, list() turns back into list, sorted(reverse=True) orders them in descending size 

num_y_ax_elements = 2000
lin_selector = math.ceil(len(ordered_y_points)/num_y_ax_elements)
reduced_y_points = ordered_y_points[::lin_selector] #creates a list with every nth element of ordered_y_points, nth = lin_selector
print('len(reduced_y_points)', len(reduced_y_points))
x_points_list = []
temp_y_list = []
temp_z_list = []
data = benedict() 
len_x_axis = len(x_axis)

for i, xdata in enumerate(x_axis):
    if xdata not in x_points_list:
        x_points_list.append(float(xdata))
        print('dataframe data creation', len(x_points_list), '/', len_of_ordered_x_axis)
    temp_y_list.append(float(y_axis[i]))
    temp_z_list.append(float(z_axis[i]))

    if i+1 == len_x_axis: #i+1 == len_x_axis means reached end of x_axis array
        interped_z_array = np.interp(reduced_y_points, temp_y_list[::-1], temp_z_list) #need to do temp_y_list[::-1] so that array values are increasing (reverses order)
        temp_y_list.clear()
        temp_z_list.clear()
        for k, ryp in enumerate(reduced_y_points):      
            data[float(xdata), float(ryp)] = float(interped_z_array[k])

    elif x_axis[i+1] != x_axis[i]:
        interped_z_array = np.interp(reduced_y_points, temp_y_list[::-1], temp_z_list)
        temp_y_list.clear()
        temp_z_list.clear()
        for k, ryp in enumerate(reduced_y_points):      
            data[float(xdata), float(ryp)] = float(interped_z_array[k])


df = pd.DataFrame.from_dict(data)
# df_transposed = df.transpose()
# print('transposed')
# df_transposed.to_csv('dataframe_1-200_nans.csv') #('dataframe_1-800_logged_data_transposed.csv')
df.to_csv('dataframe_1-800_low_res.csv') #('dataframe_1-800_logged_data_transposed.csv')
print('CSV file creation complete')
'''







'''
If the data could be laded in this way (it can't), then this would be how to do it. Above, when the pandas dataframe is constructed, the mass/model_number
is the index, the star_age/radius is columns, and the z axis is the dataframe values, so in the orientation that the pandas pivot would make it for the heatmap 
anyway, so the dataframe is enough with the nested dictionary to create a heatmap

# df = pd.DataFrame.from_dict(np.array([star_age,mass,eps_nuc]).T)
# df.columns = ['star_age','mass','eps_nuc']
# # pivotted= df.pivot(index='mass',columns='star_age',values='eps_nuc')
# ax.invert_yaxis()
'''













# star_age, radius_cm, eps_nuc = np.loadtxt("columns_for_seaborn.data", unpack=True)

'''
temp_data = []
star_age = []
radius_cm = []
eps_nuc = []

file = open("prof_1-20_star_age-radius_cm_eps-nuc.data", 'r')
for line in file:
    temp_data = line.split()
    star_age.append(float(temp_data[0]))
    radius_cm.append(float(temp_data[1]))
    eps_nuc.append(float(temp_data[2]))

star_age = np.array(star_age)
radius_cm = np.array(radius_cm)
eps_nuc = np.array(eps_nuc)
eps_nuc[eps_nuc < 1e0] = 0.0

df = pd.DataFrame.from_dict(np.array([star_age,radius_cm,eps_nuc]).T)
df.columns = ['star_age','radius_cm','eps_nuc']
# print(df)

pivotted= df.pivot(index='star_age',columns='radius_cm',values='eps_nuc')
print(pivotted)
# ax = sns.heatmap(pivotted,cmap='RdBu')
# ax = invert_yaxis()
'''



'''
WORKS BUT HAVE TO IMPROVE IT

len_of_ordered_x_axis = len(list(set(x_axis)))
ordered_y_points = sorted(list(set(y_axis)),reverse=True) #set() removes repeats, list() turns back into list, sorted(reverse=True) orders them in descending size 

x_points_list = []
data = benedict() 

for i, xdata in enumerate(x_axis):
    if xdata not in x_points_list:
        x_points_list.append(float(xdata))
        for oyp in ordered_y_points:        
            data[float(xdata), float(oyp)] = np.nan #float(0.0)
        print('dataframe data creation', len(x_points_list), '/', len_of_ordered_x_axis)
    data[float(xdata), float(y_axis[i])] = float(z_axis[i])

df = pd.DataFrame.from_dict(data)
df.to_csv('dataframe_1-800_logged_data.csv')
print('CSV file creation complete')

'''