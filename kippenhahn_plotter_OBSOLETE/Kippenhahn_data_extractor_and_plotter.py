from os.path import join
import numpy as np
import fastmesareader as fmr
from collections import defaultdict
import sys
import pandas as pd
import seaborn as sns
import matplotlib.ticker as mticker
import matplotlib.pyplot as plt
from benedict import benedict
import math
import bisect

class Kippenhahn_Plotter:

    def __init__(self, 
            profiles_paths=join('.', 'LOGS'),
            x_axis = 'star_age',              #either model_number or star_age
            y_axis = 'mass',
            z_axis = 'luminosity',
            z_axis_divider = None,
            read_profiles_index = True,    # If true, then then profile_number_start/end are not used
            profile_number_start = None,   # To use, set read_profiles_index false
            profile_number_end = None,
            auto_save_extracted_data_to_file = True,
            name_extracted_data_file = None, 
            save_dataframe_as_csv = True,
            name_dataframe_csv = None,
            x_axis_divide_by = None,
            y_axis_divide_by = None,
            z_axis_data_min_cutoff = None,
            z_axis_data_min_cutoff_replacement = None,
            z_axis_data_max_cutoff = None,
            z_axis_data_max_cutoff_replacement = None,
            log_z_axis_data_for_dataframe = False,
            num_y_axis_elements_for_heatmap = 2000,
            plot_heatmap = True,
            x_axis_label_heatmap = None,
            y_axis_label_heatmap = None,
            colorbar_label_heatmap = None,
            colorbar_min_heatmap = None,
            colorbar_max_heatmap = None,
            color_bar_scheme_heatmap = None,
            save_heatmap_as_png = None):

        self.profiles_paths = profiles_paths
        self.x_axis = x_axis
        self.y_axis = y_axis
        self.z_axis = z_axis
        self.z_axis_divider = z_axis_divider
        self.read_profiles_index = read_profiles_index
        self.profile_number_start = profile_number_start
        self.profile_number_end = profile_number_end
        self.auto_save_extracted_data_to_file = auto_save_extracted_data_to_file
        self.name_extracted_data_file = name_extracted_data_file
        self.save_dataframe_as_csv = save_dataframe_as_csv
        self.name_dataframe_csv = name_dataframe_csv
        self.x_axis_divide_by = x_axis_divide_by
        self.y_axis_divide_by = y_axis_divide_by
        self.z_axis_data_min_cutoff = z_axis_data_min_cutoff
        self.z_axis_data_min_cutoff_replacement = z_axis_data_min_cutoff_replacement
        self.z_axis_data_max_cutoff = z_axis_data_max_cutoff
        self.z_axis_data_max_cutoff_replacement = z_axis_data_max_cutoff_replacement
        self.log_z_axis_data_for_dataframe = log_z_axis_data_for_dataframe
        self.num_y_axis_elements_for_heatmap = num_y_axis_elements_for_heatmap
        self.plot_heatmap = plot_heatmap
        self.x_axis_label_heatmap = x_axis_label_heatmap 
        self.y_axis_label_heatmap = y_axis_label_heatmap 
        self.colorbar_label_heatmap = colorbar_label_heatmap 
        self.colorbar_min_heatmap = colorbar_min_heatmap 
        self.colorbar_max_heatmap = colorbar_max_heatmap 
        self.color_bar_scheme_heatmap = color_bar_scheme_heatmap 
        self.save_heatmap_as_png = save_heatmap_as_png

        self.find_paths()
        self.save_extrac_data()
        self.csv_creation()
        #heatmap plotting is called from csv_creation()



    def find_paths(self):
        self.all_col_names_for_attr = []
        self.plot_columns_names = []
        self.header_names = []
        self.header_vals = []
        self.header_data = {}
        x_axis_val = None

        self.bulk_numbers = []
        self.bulk_names = []
        self.col_numb = {}
        self.bulk_data = {}

        self.profile_numbers_array = []

        if self.z_axis_divider is not None:
            self.all_col_names = self.x_axis+' '+self.y_axis+' '+self.z_axis+' '+self.z_axis_divider
        else:
            self.all_col_names = self.x_axis+' '+self.y_axis+' '+self.z_axis
        self.all_col_names_for_attr = self.all_col_names.split()

        if self.z_axis_divider is not None:
            self.plot_columns = self.y_axis+' '+self.z_axis+' '+self.z_axis_divider
        else:
            self.plot_columns = self.y_axis+' '+self.z_axis
        self.plot_columns_names = self.plot_columns.split()  #The inputted "plot_columns" string is split into its constituent parts
        self.bulk_data[self.x_axis] = []                     #done this way as when checking plot_columns_names exist in the profile, x_axis is in header not columns.
        for name in self.plot_columns_names:                    #by user, -1 as python counts from zero
            self.bulk_data[name] = []

        if isinstance(self.profiles_paths,str):
            self.find_profiles(self.profiles_paths)
        elif isinstance(self.profiles_paths,list):
            for prof_path in self.profiles_paths:
                #reset the appropriate lists!
                self.profile_numbers_array.clear()
                self.find_profiles(prof_path)
        else:
            exit('profiles_paths must be a single string or a list (of paths)!')


    def find_profiles(self,path):
        if self.read_profiles_index is False and self.profile_number_start is None and self.profile_number_end is None:
            exit('If setting read_profiles_index = False then set profile_number_start and profile_number_end')
        if self.read_profiles_index is True:
            self.read_the_profiles_index(path) #read the profiles indexes
        else:
            self.profile_numbers_array = list(range(self.profile_number_start,self.profile_number_end+1))
        if self.profile_number_start is not None:
            if self.profile_number_start < max(self.profile_numbers_array):
                print('input profile_number_start val is greater than max profile.data in', path)
                exit()
            self.profile_numbers_array = self.profile_numbers_array[bisect.bisect_left(self.profile_numbers_array, self.profile_number_start):]
        if self.profile_number_end is not None:
            self.profile_numbers_array = self.profile_numbers_array[:bisect.bisect_right(self.profile_numbers_array, self.profile_number_end)]
        pr = 'profile'
        dat = '.data'
        for profile_num in self.profile_numbers_array:
            num = str(profile_num)
            cur_prof = pr+num+dat
            prof_and_path = join(path,cur_prof)
            print('reading ', prof_and_path)
            self.read_profile_data(prof_and_path)

    def read_the_profiles_index(self,index_path):
        prof_index = 'profiles.index'
        index_and_path = join(index_path,prof_index)
        try:
            index_file = open(index_and_path, 'r')
        except FileNotFoundError:
            print(index_and_path, 'not found, does it exist? Is the path correct? Remembered ../ if applicable?')
            exit()
        index_file.readline()                                    #line 1 is headers
        temp_data = []
        for line in index_file:                         #loads each line of data file into memory one at a time 
            temp_data = line.split()
            self.profile_numbers_array.append(int(temp_data[2])) #the profilex.data x number in that directory


    def read_profile_data(self, file_name_and_path):
        temp_data = []

        #header stuff
        file = open(file_name_and_path, 'r')                   #open log file 
        file.readline()                                    #line 1 is just numbers, don't need
        self.header_names = file.readline().split()        #line 2 log file header names 
        self.header_vals = file.readline().split()         #line 3 log file header values 
        self.header_data = dict(zip(self.header_names, self.header_vals))  #make a dictionary containg header info

        if self.x_axis not in self.header_names:     #otherwise atribute error is flagged early on
            raise AttributeError(x_axis)
        x_axis_val = self.header_data[self.x_axis]                   #find x axis val from header data

        file.readline()                                    #line 4 blank
        self.bulk_numbers = file.readline().split()        #line 5 data column numbers
        self.bulk_names = file.readline().split()          #line 6 data column headers

        for name in self.plot_columns_names:    #loop to make sure atributes inputted by the user to plot exist, 
            if name not in self.bulk_names:     #otherwise atribute error is flagged early on
                raise AttributeError(name)

        # self.bulk_data[x_axis] = []
        for i, name in enumerate(self.bulk_names):                 #Find the column numbers of the "plot_columns" inputted
            if name in self.plot_columns_names:                    #by user, -1 as python counts from zero
                self.col_numb[name] = int(self.bulk_numbers[i])-1   
                # self.bulk_data[name] = []         #assigns key of dictionary as plot_column value input by 
                                                  #user, and assigns value of the key as a list to store data columns in
        for line in file:                         #loads each line of data file into memory one at a time 
            temp_data = line.split()              #splits line into constituent data column values
            self.bulk_data[self.x_axis].append(float(x_axis_val))
            for name in self.plot_columns_names:  #for each of the inputted "plot_column" names, retrieve data from line 
                self.bulk_data[name].append(float(temp_data[self.col_numb[name]])) #once correct index found append to dict lists

        file.close()    #finished reading file, not necessary but good practice 


    def __getattr__(self, method_name):
        if self.in_bulk_data(method_name):
            return self.bulk_data_extraction_in_np_array(method_name)
        else:
            raise AttributeError(method_name)

    def in_bulk_data(self, key):
        return key in self.all_col_names_for_attr

    def bulk_data_extraction_in_np_array(self, key):
        # return np.asarray(self.bulk_data[key])
        return self.bulk_data[key]


    def save_extrac_data(self):
        if self.auto_save_extracted_data_to_file == True:
            if self.name_extracted_data_file is not None:
                np.savetxt(self.name_extracted_data_file, np.c_[self.bulk_data[self.x_axis],self.bulk_data[self.y_axis],self.bulk_data[self.z_axis]])
                print('Saved extracted data to file:', self.name_extracted_data_file)
            else:
                prs = 'profiles'
                unsc = '_'
                hphn = '-'
                dot_data = '.data'
                save_name = prs+unsc+str(self.profile_number_start)+hphn+str(self.profile_number_end)+unsc+self.x_axis+hphn+self.y_axis+hphn+self.z_axis+dot_data
                np.savetxt(save_name, np.c_[self.bulk_data[self.x_axis],self.bulk_data[self.y_axis],self.bulk_data[self.z_axis]])
                print('Saved extracted data to file:', save_name)


    def csv_creation(self):
        # temp_data = []
        # x_axis = []
        # y_axis = []
        # z_axis = []

        # file = open("prof_1-800_star_age_min-mass-eps_nuc.data", 'r')
        # for line in file:
        #     temp_data = line.split()
        #     x_axis.append(float(temp_data[0]))
        #     y_axis.append(float(temp_data[1]))
        #     z_axis.append(float(temp_data[2]))

        # x_axis = np.array(x_axis)
        # y_axis = np.array(y_axis)
        # z_axis = np.array(z_axis)

        x_axis = np.array(self.bulk_data[self.x_axis])
        y_axis = np.array(self.bulk_data[self.y_axis])
        z_axis = np.array(self.bulk_data[self.z_axis])
        if self.z_axis_divider is not None:
            z_axis_divider = np.array(self.bulk_data[self.z_axis_divider])
            z_axis = np.divide(z_axis,z_axis_divider)

        #If logging y_axis:
        if self.log_z_axis_data_for_dataframe == True:
            if self.z_axis_data_min_cutoff is not None:
                if self.z_axis_data_min_cutoff_replacement < 1e0:
                    self.z_axis_data_min_cutoff_replacement = 1e0
                z_axis[z_axis < self.z_axis_data_min_cutoff] = self.z_axis_data_min_cutoff_replacement
            else:
                z_axis[z_axis < 0e0] = 1e0
            if self.z_axis_data_max_cutoff is not None:
                z_axis[z_axis > self.z_axis_data_max_cutoff_replacement] = self.z_axis_data_max_cutoff_replacement
            z_axis = np.log10(z_axis)
        #else if linear y axis:
        else:
            if self.z_axis_data_min_cutoff is not None:
                z_axis[z_axis < self.z_axis_data_min_cutoff] = self.z_axis_data_min_cutoff_replacement
            if self.z_axis_data_max_cutoff is not None:
                z_axis[z_axis > self.z_axis_data_max_cutoff_replacement] = self.z_axis_data_max_cutoff_replacement

        if self.x_axis_divide_by is not None:
            x_axis = [x/self.x_axis_divide_by for x in x_axis]
        if self.y_axis_divide_by is not None:
            y_axis = [y/self.y_axis_divide_by for y in y_axis]

        len_of_ordered_x_axis = len(list(set(x_axis)))
        ordered_y_points = sorted(list(set(y_axis)),reverse=False) #set() removes repeats, list() turns back into list, sorted(reverse=False) orders them in ascending size 

        num_y_ax_elements = self.num_y_axis_elements_for_heatmap
        lin_selector = math.ceil(len(ordered_y_points)/num_y_ax_elements)
        reduced_y_points = ordered_y_points[::lin_selector] #creates a list with every nth element of ordered_y_points, nth = lin_selector
        x_points_list = []
        temp_y_list = []
        temp_z_list = []
        data = benedict() 
        len_x_axis = len(x_axis)

        for i, xdata in enumerate(x_axis):
            if xdata not in x_points_list:
                x_points_list.append(float(xdata))
                print('dataframe data creation', len(x_points_list), '/', len_of_ordered_x_axis)
            temp_y_list.append(float(y_axis[i]))
            temp_z_list.append(float(z_axis[i]))

            if i+1 == len_x_axis: #i+1 == len_x_axis means reached end of x_axis array
                interped_z_array = np.interp(reduced_y_points, temp_y_list[::-1], temp_z_list[::-1]) #need to do temp_y_list[::-1] so that array values are increasing (reverses order)
                temp_y_list.clear()
                temp_z_list.clear()
                for k, ryp in enumerate(reduced_y_points):      
                    data[float(xdata), float(ryp)] = float(interped_z_array[k])

            elif x_axis[i+1] != x_axis[i]:
                interped_z_array = np.interp(reduced_y_points, temp_y_list[::-1], temp_z_list[::-1])
                temp_y_list.clear()
                temp_z_list.clear()
                for k, ryp in enumerate(reduced_y_points):      
                    data[float(xdata), float(ryp)] = float(interped_z_array[k])


        df = pd.DataFrame.from_dict(data)
        print('DataFrame Creation Complete')
        if self.save_dataframe_as_csv == True:
            if self.name_dataframe_csv is not None:
                df.to_csv(self.name_dataframe_csv) 
                print('Saved dataframe as CSV file named:', self.name_dataframe_csv)
            else:
                first = 'dataframe_for_profiles'
                unsc = '_'
                hphn = '-'
                dot_csv = '.csv'
                csv_save_name = first+unsc+str(self.profile_number_start)+hphn+str(self.profile_number_end)+unsc+self.x_axis+hphn+self.y_axis+hphn+self.z_axis+dot_csv
                df.to_csv(csv_save_name)                 
                print('Saved dataframe as CSV file named:', csv_save_name)

        if self.plot_heatmap ==True:
            self.plot_the_heatmap(df)


    def plot_the_heatmap(self, dataframe):
        fig = plt.figure()
        ax = fig.add_subplot(111)

        if self.color_bar_scheme_heatmap is not None:
            bar_scheme = self.color_bar_scheme_heatmap
        else:
            bar_scheme = 'viridis'

        if self.colorbar_label_heatmap is not None:
            bar_label = self.colorbar_label_heatmap
        else:
            if self.z_axis_divider is not None:
                bar_label = self.z_axis+'/'+self.z_axis_divider
            else:
                bar_label = self.z_axis

        ax = sns.heatmap(data = dataframe,
                        # norm=log_norm,
                        # cbar_kws={"ticks": cbar_ticks}
                        vmin = self.colorbar_min_heatmap, 
                        vmax = self.colorbar_max_heatmap,
                        cmap = bar_scheme,
                        # mask=df.isnull(),
                        xticklabels = 'auto',
                        yticklabels = 'auto',
                        cbar_kws={'label': bar_label},
                        annot=False)

        if self.x_axis_label_heatmap is not None:
            ax.set_xlabel(self.x_axis_label_heatmap)
        else:
            ax.set_xlabel(self.x_axis)

        if self.y_axis_label_heatmap is not None:
            ax.set_ylabel(self.y_axis_label_heatmap)
        else:
            ax.set_ylabel(self.y_axis)

        if self.save_heatmap_as_png is not None:
            plt.savefig(self.save_heatmap_as_png, dpi=500)
            print('Saved Heatmap as PNG file named:', self.save_heatmap_as_png)

        ax.invert_yaxis()

        plt.show()