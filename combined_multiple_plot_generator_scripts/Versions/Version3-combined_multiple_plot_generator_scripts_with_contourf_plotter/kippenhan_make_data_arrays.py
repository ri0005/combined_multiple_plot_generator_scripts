from os.path import join
import numpy as np
from collections import defaultdict
import sys
import pandas as pd
import math

class Kippenhan_Make_Data_Arrays:

    def __init__(self, 
            x_axis_array = None,
            y_axis_array = None,
            z_axis_array = None,
            y_axis_min_limit = None,
            y_axis_max_limit = None,
            format_x_axis = None,
            format_y_axis = None,
            x_axis_divide_by = None,
            y_axis_divide_by = None,     
            z_axis_data_min_cutoff = None,
            z_axis_data_min_cutoff_replacement = None,
            z_axis_data_max_cutoff = None,
            z_axis_data_max_cutoff_replacement = None,
            dataframe_z_axis_data_scale = 'linear',
            num_y_axis_elements_for_heatmap = 2000,
            ):

        self.x_axis_array = x_axis_array
        self.y_axis_array = y_axis_array
        self.z_axis_array = z_axis_array
        self.y_axis_min_limit = y_axis_min_limit
        self.y_axis_max_limit = y_axis_max_limit
        self.format_x_axis = format_x_axis
        self.format_y_axis = format_y_axis
        self.x_axis_divide_by = x_axis_divide_by
        self.y_axis_divide_by = y_axis_divide_by
        self.z_axis_data_min_cutoff = z_axis_data_min_cutoff
        self.z_axis_data_min_cutoff_replacement = z_axis_data_min_cutoff_replacement
        self.z_axis_data_max_cutoff = z_axis_data_max_cutoff
        self.z_axis_data_max_cutoff_replacement = z_axis_data_max_cutoff_replacement
        self.dataframe_z_axis_data_scale = dataframe_z_axis_data_scale
        self.num_y_axis_elements_for_heatmap = num_y_axis_elements_for_heatmap

        self.make_arrays()


    def make_arrays(self):
        x_axis = np.array(self.x_axis_array)
        y_axis = np.array(self.y_axis_array)
        z_axis = np.array(self.z_axis_array)

        #If logging y_axis:
        if self.dataframe_z_axis_data_scale == 'log':
            if self.z_axis_data_min_cutoff != 'None':
                if self.z_axis_data_min_cutoff_replacement < 1e0:
                    self.z_axis_data_min_cutoff_replacement = 1e0
                z_axis[z_axis < self.z_axis_data_min_cutoff] = self.z_axis_data_min_cutoff_replacement
            else:
                z_axis[z_axis < 0e0] = 1e0
            if self.z_axis_data_max_cutoff != 'None':
                z_axis[z_axis > self.z_axis_data_max_cutoff_replacement] = self.z_axis_data_max_cutoff_replacement
            z_axis = np.log10(z_axis)
        #else if linear y axis:
        else:
            if self.z_axis_data_min_cutoff != 'None':
                z_axis[z_axis < self.z_axis_data_min_cutoff] = self.z_axis_data_min_cutoff_replacement
            if self.z_axis_data_max_cutoff != 'None':
                z_axis[z_axis > self.z_axis_data_max_cutoff_replacement] = self.z_axis_data_max_cutoff_replacement

        if self.x_axis_divide_by != 'None':
            x_axis = [x/self.x_axis_divide_by for x in x_axis]
        if self.y_axis_divide_by != 'None':
            y_axis = [y/self.y_axis_divide_by for y in y_axis]

        all_ordered_y_points = sorted(list(set(y_axis)),reverse=False) #set() removes repeats, list() turns back into list, sorted(reverse=False) orders them in ascending size 
        if self.y_axis_min_limit != 'None' and self.y_axis_max_limit == 'None':
            limited_y_points = [val for val in all_ordered_y_points if val >= self.y_axis_min_limit]
        elif self.y_axis_max_limit != 'None' and self.y_axis_min_limit == 'None':
            limited_y_points = [val for val in all_ordered_y_points if val <= self.y_axis_max_limit]
        elif self.y_axis_min_limit != 'None' and self.y_axis_max_limit != 'None':
            limited_y_points = [val for val in all_ordered_y_points if val >= self.y_axis_min_limit and val <= self.y_axis_max_limit]
        else:
            limited_y_points = all_ordered_y_points
        num_y_ax_elements = int(self.num_y_axis_elements_for_heatmap)
        lin_selector = math.ceil(len(limited_y_points)/num_y_ax_elements)
        reduced_y_points = limited_y_points[::lin_selector] #creates a list with every nth element of limited_y_points, nth = lin_selector

        if self.format_x_axis != 'None':
            self.xlist = list(dict.fromkeys(np.round(x_axis, int(self.format_x_axis)))) 
        else: 
            self.xlist = list(dict.fromkeys(x_axis))
        if self.format_y_axis != 'None':
            self.ylist = np.round(reduced_y_points, int(self.format_y_axis)) 
        else:
            self.ylist = reduced_y_points 
        len_ylist = len(self.ylist)

        temp_y_list = []
        temp_z_list = []
        data = np.zeros([len(self.ylist),len(self.xlist)])
        len_x_axis = len(x_axis)
        l = 0

        for i, xdata in enumerate(x_axis):
            temp_y_list.append(float(y_axis[i]))
            temp_z_list.append(float(z_axis[i]))

            if i+1 == len_x_axis: #i+1 == len_x_axis means reached end of x_axis array
                interped_z_array = np.interp(reduced_y_points, temp_y_list[::-1], temp_z_list[::-1]) #need to do temp_y_list[::-1] so that array values are increasing (reverses order)
                if reduced_y_points[-1] > temp_y_list[0]:
                    element_target = next(j[0] for j in enumerate(reduced_y_points) if j[1] > temp_y_list[0]) # j[0] is the array element position, j[1] is the array element value
                    interped_z_array[element_target:] = np.nan #sets FROM element element_target to last element to nans
                    # Above is necessary as interpolator assigns values to y_axis points where mass doesn't exist anymore (due to mass loss) for some reason, therefore set those point to nan
                temp_y_list.clear()
                temp_z_list.clear()
                data[:,l] = interped_z_array
                l+=1

            elif x_axis[i+1] != x_axis[i]:
                interped_z_array = np.interp(reduced_y_points, temp_y_list[::-1], temp_z_list[::-1])
                if reduced_y_points[-1] > temp_y_list[0]: #need to do element zero of temp_y_list as as temp_y_list is in decreasing order 
                    element_target = next(j[0] for j in enumerate(reduced_y_points) if j[1] > temp_y_list[0]) # j[0] is the array element position, j[1] is the array element value
                    interped_z_array[element_target:] = np.nan #sets FROM element element_target to last element to nans
                    # Above is necessary as interpolator assigns values to y_axis points where mass doesn't exist anymore (due to mass loss) for some reason, therefore set those point to nan
                temp_y_list.clear()
                temp_z_list.clear()
                data[:,l] = interped_z_array
                l+=1

        self.zdata = data


    def __getattr__(self, method_name):
        if method_name == 'xlist':
            return self.xlist_return()
        elif method_name == 'ylist':
            return self.ylist_return()
        elif method_name == 'zdata':
            return self.zdata_return()
        else:
            raise AttributeError(method_name)


    def xlist_return(self):
        return self.xlist

    def ylist_return(self):
        return self.ylist

    def zdata_return(self):
        return self.zdata
