'''
TO DO:

- Make Y-axis coordinates more sensible and linear in increments. 
- Incorporate David's bookmarkign feature for the PDFs.
- Change global_properties_x_axis Kippenhahn_properties dicts key names to be clearer and better.
- Incorporate keywaord args to replace positional function call args.
- Make a external text file with the profile variables as keys and associated values with publishable definitions. 
'''

import os
import sys
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr
import kippenhan_data_read as kipp_data_read
import kippenhan_make_data_arrays as kipp_data_arrays
import seaborn as sns #to delete


def general_plot_routine(input_directory, output_directory, two_D_combined_pdf_filename, three_D_combined_pdf_filename, 
    global_properties_x_axis_args, global_properties_plot_vars, 
    multi_line_plots_x_axis_args, multi_line_plots_vars,
    only_plot_two_D_graphs_bool, 
    Kippenhahn_args, Kippenhahn_plot_vars, comparing_at_unity_plots_vars):

    os.makedirs(output_directory, exist_ok=True) # Make output dir if necessary:
    history_path = os.path.join(input_directory, 'history.data')
    twoD_out_path_and_name = os.path.join(output_directory, two_D_combined_pdf_filename)
    threeD_out_path_and_name = os.path.join(output_directory, three_D_combined_pdf_filename)

    global_properties_x_axis_args['x_axis_divider'] = set_divider_value(global_properties_x_axis_args['x_axis_divider'])
    multi_line_plots_x_axis_args['x_axis_divider'] = set_divider_value(multi_line_plots_x_axis_args['x_axis_divider'])
    Kippenhahn_args['x_axis_divide_by'] = set_divider_value(Kippenhahn_args['x_axis_divide_by'])

    global_properties_y_axis_variables = list(global_properties_plot_vars.keys())
    global_properties_y_axis_scale_type = list(global_properties_plot_vars.values())

    multi_line_plots_variables = [kys for d in multi_line_plots_vars.values() for kys in d.keys()]

    twoD_all_plotting_vars = [global_properties_x_axis_args['plot_variable']] + [multi_line_plots_x_axis_args['plot_variable']] + global_properties_y_axis_variables + multi_line_plots_variables
    twoD_all_plotting_vars = list(dict.fromkeys(twoD_all_plotting_vars)) # Removes repeats from list
    a = fmr.FastMesaReader(history_path, twoD_all_plotting_vars) 

    pdf = PdfPages(twoD_out_path_and_name)
    # Make 2D graphs
    if len(global_properties_y_axis_variables) > 0:
        for i in range(len(global_properties_y_axis_variables)):
            fig = make_2d_plot(a.__getattr__(global_properties_x_axis_args['plot_variable']), 
                a.__getattr__(global_properties_y_axis_variables[i]), global_properties_x_axis_args, 
                global_properties_y_axis_scale_type[i], global_properties_y_axis_variables[i])
            pdf.savefig(fig)

    # Make multiplot graphs 
    if len(multi_line_plots_variables) > 0:
        for ttls in multi_line_plots_vars.keys():
            fig = make_2d_multiline_plot(a, ttls, multi_line_plots_vars, multi_line_plots_x_axis_args)
            pdf.savefig(fig)
    pdf.close()
    print('2D Plots PDF Created')

    if only_plot_two_D_graphs_bool is True:
        sys.exit()

    Kippenhahn_z_axis_variables = list(Kippenhahn_plot_vars.keys())
    comparing_at_unity_list_of_lists = [lst for lst in comparing_at_unity_plots_vars.values()] #this produces a list of lists, for example 
    comparing_at_unity_z_axis_variables = list(dict.fromkeys([item for sublist in comparing_at_unity_list_of_lists for item in sublist])) # this makes one list from the list of lists
    comparing_unity_plot_titles = list(comparing_at_unity_plots_vars.keys())
    all_z_axis_variables = Kippenhahn_z_axis_variables + comparing_at_unity_z_axis_variables
    if len(all_z_axis_variables) == 0:
        sys.exit()

    Kippenhahn_z_axis_scale_type = list(Kippenhahn_plot_vars.values())
    b = kipp_data_read.Kipp_Data_Reader(
        profiles_paths = input_directory,
        x_axis_var = Kippenhahn_args['x_axis'],
        y_axis_var = Kippenhahn_args['y_axis'],
        z_axis_vars = all_z_axis_variables,
        )

    pdf = PdfPages(threeD_out_path_and_name)
    # Make standard Kippenhahn plots
    if len(Kippenhahn_z_axis_variables) > 0:
        for i in range(len(Kippenhahn_z_axis_variables)):
            fig = make_3d_plot(input_directory, Kippenhahn_args, 
                b.x_axis_array, b.y_axis_array, b.__getattr__(Kippenhahn_z_axis_variables[i]),
                Kippenhahn_z_axis_variables[i], Kippenhahn_z_axis_scale_type[i])
            pdf.savefig(fig)
            print('Kippenhahn Plot Created for ', Kippenhahn_z_axis_variables[i])

    # Make unity comparison Kippenhahn plots
    if len(comparing_at_unity_z_axis_variables) > 0:
        for i, lst in enumerate(comparing_at_unity_plots_vars.values()):
            fig = make_unity_comparison_plot(b, input_directory, Kippenhahn_args, 
                comparing_unity_plot_titles[i], b.x_axis_array, b.y_axis_array, lst)
            pdf.savefig(fig)
            print('Unity Comparison Kippenhahn Plot Created for ', lst)
    pdf.close()
    print('Kippenhahn Plots PDF Created')


def set_divider_value(divider_value):
    if divider_value == 'None':
        return 1
    elif divider_value == 'sec':
        return 1/31536000
    elif divider_value == 'min':
        return 1/525600
    elif divider_value == 'hr':
        return 8760
    elif divider_value == 'day':
        return 1/365
    elif divider_value == 'yr':
        return 1
    elif divider_value == 'kyr':
        return 1e3
    elif divider_value == 'myr':
        return 1e6
    elif divider_value == 'gyr':
        return 1e9
    elif divider_value == 1:
        return 1
    else:
        print('X-axis divider {unk_div_val} is not recognised.'.format(unk_div_val=divider_value))
        print(' Options are: None, sec, min, hr, day, yr, kyr, myr, gyr.')
        print('Stopping')
        sys.exit()


def make_2d_plot(x_array, y_array, x_axis_options, y_axis_scale, y_axis_name):

    fig = plt.figure(figsize=(12.8,9.6))
    ax = fig.add_subplot(111)

    ax.plot(x_array/x_axis_options['x_axis_divider'], y_array, linewidth='1.5')
    ax.set_xscale(x_axis_options['x_axis_scale_type'])
    ax.set_yscale(y_axis_scale)
    if x_axis_options['x_axis_scale_type'] == 'linear':
        ax.ticklabel_format(axis='x', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    if y_axis_scale == 'linear':
        ax.ticklabel_format(axis='y', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    ax.set_xlabel(x_axis_options['x_axis_name_for_plots'], fontsize=18)
    ax.set_ylabel(y_axis_name, fontsize=18) 

    ax.tick_params(axis='x', labelsize=14)
    ax.tick_params(axis='y', labelsize=14)

    fig.tight_layout()

    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.grid(which='major', linestyle=':', linewidth='0.8')
    plt.minorticks_on()
    ax.grid(which='minor', linestyle=':', linewidth='0.4')
    # ax.ticklabel_format(axis='both', style='plain', scilimits=(0,0), useOffset=False)

    return fig


def make_2d_multiline_plot(a_attribute, plot_title, multi_line_plots_dict, x_axis_options):
    
    fig = plt.figure(figsize=(12.8,9.6))
    ax = fig.add_subplot(111)

    y_scale_list = [vals for vals in multi_line_plots_dict[plot_title].values()]
    y_scale_check = all(ele == y_scale_list[0] for ele in y_scale_list) # Check to see if all plotting args have the same scale, return True if all are same and false if not.
    
    colors = plt.cm.viridis(np.linspace(0,1,len(y_scale_list))) # use y_scale_list for len as it already exists - its just convenience

    if y_scale_check: # If all plotting args have the same y scale, ax.set_yscale is just set to first elements scale, and checks are done for plotting linear or log
        for i, vrbls in enumerate(multi_line_plots_dict[plot_title].keys()):
            ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], a_attribute.__getattr__(vrbls), linewidth='1.5', label=vrbls, color=colors[i])
        ax.set_yscale(y_scale_list[0])
        if y_scale_list[0] == 'linear':
            ax.ticklabel_format(axis='y', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    else:
        for i, vrbls in enumerate(multi_line_plots_dict[plot_title].keys()):
            if multi_line_plots_dict[plot_title][vrbls] == 'log':
                ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], np.log10(a_attribute.__getattr__(vrbls)), linewidth='1.5', label='$\\mathrm{LOG_{10}}$('+vrbls+')', color=colors[i]) 
            else:
                ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], a_attribute.__getattr__(vrbls), linewidth='1.5', label=vrbls, color=colors[i])
        ax.set_yscale('linear')
        ax.ticklabel_format(axis='y', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    ax.set_xscale(x_axis_options['x_axis_scale_type'])
    if x_axis_options['x_axis_scale_type'] == 'linear':
        ax.ticklabel_format(axis='x', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    if x_axis_options['x_axis_scale_type'] == 'log':
        ax.set_xlabel('$\\mathrm{LOG_{10}}$('+x_axis_options['x_axis_name_for_plots']+')', fontsize=18)
    else:
        ax.set_xlabel(x_axis_options['x_axis_name_for_plots'], fontsize=18)

    ax.set_title(plot_title, fontsize=22)

    ax.tick_params(axis='x', labelsize=14)
    ax.tick_params(axis='y', labelsize=14)

    ax.legend(prop={"size":14})
    fig.tight_layout()
    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.grid(which='major', linestyle=':', linewidth='0.8')
    plt.minorticks_on()
    ax.grid(which='minor', linestyle=':', linewidth='0.4')

    return fig


def make_3d_plot(input_dir, Kippenhahn_properties, 
    x_axis_array, y_axis_array, Kippenhahn_z_axis_variables_array,
    Kippenhahn_z_axis_variable, Kippenhahn_z_axis_scale):

    c = kipp_data_arrays.Kippenhan_Make_Data_Arrays(
        x_axis_array = x_axis_array,
        y_axis_array = y_axis_array,
        z_axis_array = Kippenhahn_z_axis_variables_array,
        y_axis_min_limit = Kippenhahn_properties['y_axis_min_limit'],
        y_axis_max_limit = Kippenhahn_properties['y_axis_max_limit'],
        format_x_axis = Kippenhahn_properties['format_x_axis'],
        format_y_axis = Kippenhahn_properties['format_y_axis'],
        x_axis_divide_by = Kippenhahn_properties['x_axis_divide_by'], 
        y_axis_divide_by = Kippenhahn_properties['y_axis_divide_by'],
        z_axis_data_min_cutoff = Kippenhahn_properties['z_axis_data_min_cutoff'],
        z_axis_data_min_cutoff_replacement = Kippenhahn_properties['z_axis_data_min_cutoff_replacement'],
        z_axis_data_max_cutoff = Kippenhahn_properties['z_axis_data_max_cutoff'],
        z_axis_data_max_cutoff_replacement = Kippenhahn_properties['z_axis_data_max_cutoff_replacement'],
        z_axis_data_scale = Kippenhahn_z_axis_scale,
        num_y_axis_elements_for_heatmap = Kippenhahn_properties['num_y_axis_elements_for_heatmap'],
        )

    fig = plt.figure(figsize=(16,9.6))
    ax = fig.add_subplot(111)

    bar_scheme = Kippenhahn_properties['color_bar_scheme_heatmap']
    if Kippenhahn_z_axis_scale == 'log':
        bar_label = '$\\mathrm{LOG_{10}}$('+Kippenhahn_z_axis_variable+')'
    elif Kippenhahn_z_axis_scale == 'linear':
        bar_label = Kippenhahn_z_axis_variable

    X, Y = np.meshgrid(c.xlist, c.ylist)
    cs = ax.contourf(X, Y, c.zdata, levels=Kippenhahn_properties['levels_for_contourf_plot'], cmap=bar_scheme, antialiased=True)
    for c in cs.collections:
        c.set_rasterized(True)
    bar = fig.colorbar(cs, pad=0.02)
    bar.set_label(bar_label)
    bar.outline.set_visible(False)

    ax.set_xlabel(Kippenhahn_properties['x_axis_label_heatmap'], fontsize=18)
    ax.set_ylabel(Kippenhahn_properties['y_axis_label_heatmap'], fontsize=18)
    ax.figure.axes[-1].yaxis.label.set_size(18) # Colourbar label size
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    fig.tight_layout(rect=[0.04, 0.02, 1.07, 0.98])

    ax.ticklabel_format(axis='x', style=Kippenhahn_properties['x_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    ax.ticklabel_format(axis='y', style=Kippenhahn_properties['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    if Kippenhahn_properties['overlay_history_line'] != 'None':
        hist = fmr.FastMesaReader(os.path.join(input_dir, 'history.data'), [Kippenhahn_properties['overlay_history_line'], Kippenhahn_properties['x_axis']]) 
        ax.plot(hist.__getattr__(Kippenhahn_properties['x_axis'])/Kippenhahn_properties['x_axis_divide_by'], hist.__getattr__(Kippenhahn_properties['overlay_history_line']), linewidth='2', color=Kippenhahn_properties['overlay_history_line_colour'], label=Kippenhahn_properties['overlay_history_line_label'])
        ax.legend()

    return fig


def make_unity_comparison_plot(b_attribute, input_dir, Kippenhahn_properties, 
    plot_title, x_axis_array, y_axis_array, list_for_comparison):

    if len(list_for_comparison) == 1:
        Kippenhahn_z_axis_variables_array = np.array(b_attribute.__getattr__(list_for_comparison[0]))
        bar_label = list_for_comparison[0]
    elif len(list_for_comparison) == 2:
        Kippenhahn_z_axis_variables_array = np.divide(b_attribute.__getattr__(list_for_comparison[0]),b_attribute.__getattr__(list_for_comparison[1]))
        bar_label = list_for_comparison[0]+'/'+list_for_comparison[1]
    else:
        print('*********** comparing_at_unity_plots dict values must be lists of either 1 or 2 elements only. ***********')
        print('*********** Pleae correct dict value:', list_for_comparison, '***********')
        return plt.figure()
    Kippenhahn_z_axis_variables_array[Kippenhahn_z_axis_variables_array < 0e0] = 0e0  # Makes any value in Z-array greater than 2 equal to 2
    Kippenhahn_z_axis_variables_array[Kippenhahn_z_axis_variables_array > 2e0] = 2e0  # Makes any value in Z-array less than 0 equal to zero

    c = kipp_data_arrays.Kippenhan_Make_Data_Arrays(
        x_axis_array = x_axis_array,
        y_axis_array = y_axis_array,
        z_axis_array = Kippenhahn_z_axis_variables_array,
        y_axis_min_limit = Kippenhahn_properties['y_axis_min_limit'],
        y_axis_max_limit = Kippenhahn_properties['y_axis_max_limit'],
        format_x_axis = Kippenhahn_properties['format_x_axis'],
        format_y_axis = Kippenhahn_properties['format_y_axis'],
        x_axis_divide_by = Kippenhahn_properties['x_axis_divide_by'], 
        y_axis_divide_by = Kippenhahn_properties['y_axis_divide_by'],
        z_axis_data_min_cutoff = 'None',
        z_axis_data_min_cutoff_replacement = 'None',
        z_axis_data_max_cutoff = 'None',
        z_axis_data_max_cutoff_replacement = 'None',
        z_axis_data_scale = 'linear',
        num_y_axis_elements_for_heatmap = Kippenhahn_properties['num_y_axis_elements_for_heatmap'],
        )

    fig = plt.figure(figsize=(16,9.6))
    ax = fig.add_subplot(111)

    bar_scheme = Kippenhahn_properties['color_bar_scheme_heatmap']

    X, Y = np.meshgrid(c.xlist, c.ylist)
    cs = ax.contourf(X, Y, c.zdata, levels=np.array([0,1,2]), cmap='seismic', antialiased=True)
    for c in cs.collections:
        c.set_rasterized(True)
    bar = fig.colorbar(cs, pad=0.02)
    bar.set_label(bar_label)
    bar.outline.set_visible(False)

    ax.set_xlabel(Kippenhahn_properties['x_axis_label_heatmap'], fontsize=18)
    ax.set_ylabel(Kippenhahn_properties['y_axis_label_heatmap'], fontsize=18)
    ax.figure.axes[-1].yaxis.label.set_size(18) # Colourbar label size
    ax.set_title(plot_title, fontsize=22)
    ax.tick_params(axis='x', labelsize=12)
    ax.tick_params(axis='y', labelsize=12)
    fig.tight_layout(rect=[0.04, 0.02, 1.07, 0.98])  # rect=[left, bottom, right, top] specifies the bounding box that the plots will be fit inside. The coordinates must be in normalized figure coordinates and the default is (0, 0, 1, 1).

    ax.ticklabel_format(axis='x', style=Kippenhahn_properties['x_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    ax.ticklabel_format(axis='y', style=Kippenhahn_properties['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    if Kippenhahn_properties['overlay_history_line'] != 'None':
        hist = fmr.FastMesaReader(os.path.join(input_dir, 'history.data'), [Kippenhahn_properties['overlay_history_line'], Kippenhahn_properties['x_axis']]) 
        ax.plot(hist.__getattr__(Kippenhahn_properties['x_axis'])/Kippenhahn_properties['x_axis_divide_by'], hist.__getattr__(Kippenhahn_properties['overlay_history_line']), linewidth='2', color='k', label=Kippenhahn_properties['overlay_history_line_label'])
        ax.legend()

    return fig