from os.path import join
import numpy as np
from collections import defaultdict
import sys
import pandas as pd
from benedict import benedict
import math

class Kipp_Data_Reader:

    def __init__(self, 
            profiles_paths=join('.', 'LOGS'),
            x_axis_var = 'star_age',              #either model_number or star_age
            y_axis_var = 'mass',
            z_axis_vars = 'luminosity',    
            ):

        self.profiles_paths = profiles_paths
        self.x_axis_var = x_axis_var
        self.y_axis_var = y_axis_var
        self.z_axis_vars = z_axis_vars

        self.find_paths()


    def find_paths(self):
        self.all_col_names_for_attr = []
        self.plot_columns_names = []
        self.header_names = []
        self.header_vals = []
        self.header_data = {}
        x_axis_val = None
        self.total_num_zones = 0
        self.profile_num_zones = {}

        self.bulk_numbers = []
        self.bulk_names = []
        self.col_numb = {}
        self.bulk_data = {}

        self.profile_numbers_array = []
        self.model_number_of_profiles = []

        self.all_col_names = self.x_axis_var+' '+self.y_axis_var
        self.all_col_names_for_attr = self.all_col_names.split()
        self.all_col_names_for_attr += self.z_axis_vars

        if isinstance(self.profiles_paths,str):
            self.find_num_zones(self.profiles_paths)
        elif isinstance(self.profiles_paths,list):
            for prof_path in self.profiles_paths:
                #reset the appropriate lists!
                self.profile_numbers_array.clear()
                self.find_num_zones(prof_path)
                # NEED TO IMPLEMENT THIS FEATURE LATER
        else:
            exit('profiles_paths must be a single string or a list (of paths)!')


        self.plot_columns_names = self.y_axis_var.split() + self.z_axis_vars  
        self.bulk_data[self.x_axis_var] = []                             #done this way as when checking plot_columns_names exist in the profile, x_axis is in header not columns.
        for name in self.plot_columns_names:                         #by user, -1 as python counts from zero
            self.bulk_data[name] = [0]*self.total_num_zones

        if isinstance(self.profiles_paths,str):
            self.iterate_through_profiles(self.profiles_paths)
        elif isinstance(self.profiles_paths,list):
            for prof_path in self.profiles_paths:
                #reset the appropriate lists!
                self.profile_numbers_array.clear()
                self.iterate_through_profiles(prof_path)


    def find_num_zones(self,path):
        self.read_the_profiles_index(path) #read the profiles indexes
        pr = 'profile'
        dat = '.data'
        for profile_num in self.profile_numbers_array:
            num = str(profile_num)
            cur_prof = pr+num+dat
            prof_and_path = join(path,cur_prof)
            self.read_num_zones_in_each_profile(prof_and_path,num)
        print('Memory Assigned for bulk_data')


    def read_num_zones_in_each_profile(self, file_name_and_path, profile_number):
        file = open(file_name_and_path, 'r')
        file.readline()           
        temp_header_names = file.readline().split()
        temp_header_vals = file.readline().split()
        temp_header_data = dict(zip(temp_header_names, temp_header_vals))
        prof_num_zones_and_cumil_num_zones = [int(temp_header_data['num_zones']), self.total_num_zones] # list containing [the number of zones for profile x, the bulk_data array element position number to start adding data from this profile into]
        self.profile_num_zones[profile_number] = prof_num_zones_and_cumil_num_zones
        self.total_num_zones += int(temp_header_data['num_zones'])


    def iterate_through_profiles(self,path):
        # self.read_the_profiles_index(path) #read the profiles indexes #NOW DO FOR NUM_ZONES SO COMMENTED OUT HERE
        pr = 'profile'
        dat = '.data'
        for profile_num in self.profile_numbers_array:
            num = str(profile_num)
            cur_prof = pr+num+dat
            prof_and_path = join(path,cur_prof)
            self.read_profile_data(prof_and_path, num)
            if profile_num%100 == 0e0:
                print('read up to:', prof_and_path)
        print('All Profiles Read for Data Columns ', self.z_axis_vars)


    def read_the_profiles_index(self,index_path):
        prof_index = 'profiles.index'
        index_and_path = join(index_path,prof_index)
        try:
            index_file = open(index_and_path, 'r')
        except FileNotFoundError:
            print(index_and_path, 'not found, does it exist? Is the path correct? Remembered ../ if applicable?')
            exit()
        index_file.readline()                                    #line 1 is headers
        temp_data = []
        for line in index_file:                         #loads each line of data file into memory one at a time 
            temp_data = line.split()
            self.profile_numbers_array.append(int(temp_data[2])) #the profilex.data x number in that directory
            self.model_number_of_profiles.append(int(temp_data[0]))


    def read_profile_data(self, file_name_and_path, profile_number):
        temp_data = []

        #header stuff
        file = open(file_name_and_path, 'r')                   #open log file 
        file.readline()                                    #line 1 is just numbers, don't need
        self.header_names = file.readline().split()        #line 2 log file header names 
        self.header_vals = file.readline().split()         #line 3 log file header values 
        self.header_data = dict(zip(self.header_names, self.header_vals))  #make a dictionary containg header info

        if self.x_axis_var not in self.header_names:     #otherwise atribute error is flagged early on
            raise AttributeError(x_axis)
        x_axis_val = self.header_data[self.x_axis_var]                   #find x axis val from header data

        file.readline()                                    #line 4 blank
        self.bulk_numbers = file.readline().split()        #line 5 data column numbers
        self.bulk_names = file.readline().split()          #line 6 data column headers

        for name in self.plot_columns_names:    #loop to make sure atributes inputted by the user to plot exist, 
            if name not in self.bulk_names:     #otherwise atribute error is flagged early on
                raise AttributeError(name)

        # self.bulk_data[x_axis] = []
        for i, name in enumerate(self.bulk_names):                 #Find the column numbers of the "plot_columns" inputted
            if name in self.plot_columns_names:                    #by user, -1 as python counts from zero
                self.col_numb[name] = int(self.bulk_numbers[i])-1   


        # num_zones = self.profile_num_zones[profile_number][0]
        element_position = self.profile_num_zones[profile_number][1]
        for line in file:                         #loads each line of data file into memory one at a time 
            temp_data = line.split()              #splits line into constituent data column values
            self.bulk_data[self.x_axis_var].append(float(x_axis_val))
            for name in self.plot_columns_names:  #for each of the inputted "plot_column" names, retrieve data from line 
                self.bulk_data[name][element_position] = float(temp_data[self.col_numb[name]])
            element_position +=1

        file.close()    #finished reading file, not necessary but good practice 


    def __getattr__(self, method_name):
        if self.in_bulk_data(method_name):
            return self.bulk_data_extraction_in_np_array(method_name)
        elif method_name == 'x_axis_array':
            return self.x_axis_array_return()
        elif method_name == 'y_axis_array':
            return self.y_axis_array_return()
        else:
            raise AttributeError(method_name)

    def in_bulk_data(self, key):
        return key in self.all_col_names_for_attr

    def bulk_data_extraction_in_np_array(self, key):
        # return np.asarray(self.bulk_data[key])
        return self.bulk_data[key]

    def x_axis_array_return(self):
        return np.array(self.bulk_data[self.x_axis_var])
        
    def y_axis_array_return(self):
        return np.array(self.bulk_data[self.y_axis_var])


