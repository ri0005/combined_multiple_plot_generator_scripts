'''
TO DO:

- Make Y-axis coordinates more sensible and linear in increments. 
- Incorporate David's bookmarkign feature for the PDFs.
- Change global_properties_x_axis Kippenhahn_properties dicts key names to be clearer and better.
- Incorporate keywaord args to replace positional function call args.
'''

import os
import sys
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import fastmesareader as fmr
import kippenhan_data_read as kipp_data_read
import kippenhan_make_data_arrays as kipp_data_arrays
import seaborn as sns #to delete


def general_plot_routine(input_directory, output_directory, two_D_combined_pdf_filename, three_D_combined_pdf_filename, 
    global_properties_x_axis_args, global_properties_plot_vars, 
    multi_line_plots_x_axis_args, multi_line_plots_vars,
    only_plot_two_D_graphs_bool, 
    Kippenhahn_args, Kippenhahn_plot_vars, comparing_at_unity_plots_vars):

    os.makedirs(output_directory, exist_ok=True) # Make output dir if necessary:
    history_path = os.path.join(input_directory, 'history.data')
    twoD_out_path_and_name = os.path.join(output_directory, two_D_combined_pdf_filename)
    threeD_out_path_and_name = os.path.join(output_directory, three_D_combined_pdf_filename)

    global_properties_x_axis_args['x_axis_divider'] = set_divider_value(global_properties_x_axis_args['x_axis_divider'])
    multi_line_plots_x_axis_args['x_axis_divider'] = set_divider_value(multi_line_plots_x_axis_args['x_axis_divider'])
    Kippenhahn_args['x_axis_divide_by'] = set_divider_value(Kippenhahn_args['x_axis_divide_by'])

    global_properties_y_axis_variables = list(global_properties_plot_vars.keys())
    global_properties_y_axis_scale_type = list(global_properties_plot_vars.values())

    multi_line_plots_variables = [kys for d in multi_line_plots_vars.values() for kys in d.keys()]

    all_plotting_vars = [global_properties_x_axis_args['plot_variable']] + [multi_line_plots_x_axis_args['plot_variable']] + global_properties_y_axis_variables + multi_line_plots_variables
    all_plotting_vars = list(dict.fromkeys(all_plotting_vars)) # Removes repeats from list
    a = fmr.FastMesaReader(history_path, all_plotting_vars) 

    pdf = PdfPages(twoD_out_path_and_name)
    if len(global_properties_y_axis_variables) > 0:
        for i in range(len(global_properties_y_axis_variables)):
            fig = make_2d_plot(a.__getattr__(global_properties_x_axis_args['plot_variable']), 
                a.__getattr__(global_properties_y_axis_variables[i]), global_properties_x_axis_args, 
                global_properties_y_axis_scale_type[i], global_properties_y_axis_variables[i])
            pdf.savefig(fig)

    if len(multi_line_plots_variables) > 0:
        for ttls in multi_line_plots_vars.keys():
            fig = make_2d_multiline_plot(a, ttls, multi_line_plots_vars, multi_line_plots_x_axis_args)
            pdf.savefig(fig)
    pdf.close()
    print('2D Plots PDF Created')

    if only_plot_two_D_graphs_bool is True:
        sys.exit()

    Kippenhahn_z_axis_variables = list(Kippenhahn_plot_vars.keys())
    if len(Kippenhahn_z_axis_variables) == 0:
        sys.exit()

    Kippenhahn_z_axis_scale_type = list(Kippenhahn_plot_vars.values())
    b = kipp_data_read.Kipp_Data_Reader(
        profiles_paths = input_directory,
        x_axis = Kippenhahn_args['x_axis'],
        y_axis = Kippenhahn_args['y_axis'],
        z_axis = Kippenhahn_z_axis_variables,
        )

    pdf = PdfPages(threeD_out_path_and_name)
    for i in range(len(Kippenhahn_z_axis_variables)):
        fig = make_3d_plot(b, input_directory, Kippenhahn_args, 
            b.x_axis_array, b.linearized_y_axis, b.__getattr__(Kippenhahn_z_axis_variables[i]),
            Kippenhahn_z_axis_variables[i], Kippenhahn_z_axis_scale_type[i])
        pdf.savefig(fig)
        print('Plot Created for ', Kippenhahn_z_axis_variables[i])

        fig = make_3d_plot_2(input_directory, Kippenhahn_args, 
            b.x_axis_array, b.linearized_y_axis, b.__getattr__(Kippenhahn_z_axis_variables[i]),
            Kippenhahn_z_axis_variables[i], Kippenhahn_z_axis_scale_type[i])
        pdf.savefig(fig)
        print('Contourf Plot Created for ', Kippenhahn_z_axis_variables[i])

    pdf.close()
    print('Kippenhahn Plots PDF Created')


def set_divider_value(divider_value):
    if divider_value == 'None':
        return 1
    elif divider_value == 'sec':
        return 1/31536000
    elif divider_value == 'min':
        return 1/525600
    elif divider_value == 'hr':
        return 8760
    elif divider_value == 'day':
        return 1/365
    elif divider_value == 'yr':
        return 1
    elif divider_value == 'kyr':
        return 1e3
    elif divider_value == 'myr':
        return 1e6
    elif divider_value == 'gyr':
        return 1e9
    elif divider_value == 1:
        return 1
    else:
        print('X-axis divider {unk_div_val} is not recognised.'.format(unk_div_val=divider_value))
        print(' Options are: None, sec, min, hr, day, yr, kyr, myr, gyr.')
        print('Stopping')
        sys.exit()


def make_2d_plot(x_array, y_array, x_axis_options, y_axis_scale, y_axis_name):

    fig = plt.figure(figsize=(12.8,9.6))
    ax = fig.add_subplot(111)

    ax.plot(x_array/x_axis_options['x_axis_divider'], y_array, linewidth='1.5')
    ax.set_xscale(x_axis_options['x_axis_scale_type'])
    ax.set_yscale(y_axis_scale)

    ax.set_xlabel(x_axis_options['x_axis_name_for_plots'], fontsize=18)
    ax.set_ylabel(y_axis_name, fontsize=18) 

    ax.tick_params(axis='x', labelsize=14)
    ax.tick_params(axis='y', labelsize=14)

    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.grid(which='major', linestyle=':', linewidth='0.8')
    plt.minorticks_on()
    ax.grid(which='minor', linestyle=':', linewidth='0.4')
    # ax.ticklabel_format(axis='both', style='plain', scilimits=(0,0), useOffset=False)

    return fig


def make_2d_multiline_plot(a_attribute, plot_title, multi_line_plots_dict, x_axis_options):
    
    fig = plt.figure(figsize=(12.8,9.6))
    ax = fig.add_subplot(111)

    y_scale_list = [vals for vals in multi_line_plots_dict[plot_title].values()]
    y_scale_check = all(ele == y_scale_list[0] for ele in y_scale_list) # Check to see if all plotting args have the same scale, return True if all are same and false if not.
    
    colors = plt.cm.viridis(np.linspace(0,1,len(y_scale_list))) # use y_scale_list for len as it already exists - its just convenience

    if y_scale_check: # If all plotting args have the same y scale, ax.set_yscale is just set to first elements scale, and checks are done for plotting linear or log
        for i, vrbls in enumerate(multi_line_plots_dict[plot_title].keys()):
            ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], a_attribute.__getattr__(vrbls), linewidth='1.5', label=vrbls, color=colors[i])
        ax.set_yscale(y_scale_list[0])
    else:
        for i, vrbls in enumerate(multi_line_plots_dict[plot_title].keys()):
            if multi_line_plots_dict[plot_title][vrbls] == 'log':
                ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], np.log10(a_attribute.__getattr__(vrbls)), linewidth='1.5', label='$\\mathrm{LOG_{10}}$('+vrbls+')', color=colors[i]) 
            else:
                ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], a_attribute.__getattr__(vrbls), linewidth='1.5', label=vrbls, color=colors[i])
        ax.set_yscale('linear')

    ax.set_xscale(x_axis_options['x_axis_scale_type'])

    if x_axis_options['x_axis_scale_type'] == 'log':
        ax.set_xlabel('$\\mathrm{LOG_{10}}$('+x_axis_options['x_axis_name_for_plots']+')', fontsize=18)
    else:
        ax.set_xlabel(x_axis_options['x_axis_name_for_plots'], fontsize=18)

    ax.set_title(plot_title, fontsize=20)

    ax.tick_params(axis='x', labelsize=14)
    ax.tick_params(axis='y', labelsize=14)

    ax.legend(prop={"size":14})

    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.grid(which='major', linestyle=':', linewidth='0.8')
    plt.minorticks_on()
    ax.grid(which='minor', linestyle=':', linewidth='0.4')

    return fig


def make_3d_plot(b_attribute, input_dir, Kippenhahn_properties, 
    x_axis_array, linearized_y_axis_array, Kippenhahn_z_axis_variables_array,
    Kippenhahn_z_axis_variable, Kippenhahn_z_axis_scale):

    c = kipp_data_arrays.Kippenhan_Make_Data_Arrays(
        x_axis_array = x_axis_array,
        y_axis_array = linearized_y_axis_array,
        z_axis_array = Kippenhahn_z_axis_variables_array,
        y_axis_min_limit = Kippenhahn_properties['y_axis_min_limit'],
        y_axis_max_limit = Kippenhahn_properties['y_axis_max_limit'],
        format_x_axis = Kippenhahn_properties['format_x_axis'],
        format_y_axis = Kippenhahn_properties['format_y_axis'],
        x_axis_divide_by = Kippenhahn_properties['x_axis_divide_by'], 
        y_axis_divide_by = Kippenhahn_properties['y_axis_divide_by'],
        z_axis_data_min_cutoff = Kippenhahn_properties['z_axis_data_min_cutoff'],
        z_axis_data_min_cutoff_replacement = Kippenhahn_properties['z_axis_data_min_cutoff_replacement'],
        z_axis_data_max_cutoff = Kippenhahn_properties['z_axis_data_max_cutoff'],
        z_axis_data_max_cutoff_replacement = Kippenhahn_properties['z_axis_data_max_cutoff_replacement'],
        dataframe_z_axis_data_scale = Kippenhahn_z_axis_scale,
        num_y_axis_elements_for_heatmap = Kippenhahn_properties['num_y_axis_elements_for_heatmap'],
        )
    print('DataFrame Creation Complete For ', Kippenhahn_z_axis_variable)

    fig = plt.figure(figsize=(16,9.6))
    sns.set_context('paper')
    ax = fig.add_subplot(111)

    bar_scheme = Kippenhahn_properties['color_bar_scheme_heatmap']
    if Kippenhahn_z_axis_scale == 'log':
        bar_label = '$\\mathrm{LOG_{10}}$('+Kippenhahn_z_axis_variable+')'
    elif Kippenhahn_z_axis_scale == 'linear':
        bar_label = Kippenhahn_z_axis_variable
    ax = sns.heatmap(data = c.df,
                    cmap = bar_scheme,
                    # mask=df.isnull(),
                    xticklabels = 'auto',
                    yticklabels = 'auto',
                    cbar_kws={'label': bar_label},
                    annot=False)

    ax.set_xlabel(Kippenhahn_properties['x_axis_label_heatmap'], fontsize=12)
    ax.set_ylabel(Kippenhahn_properties['y_axis_label_heatmap'], fontsize=12)
    ax.figure.axes[-1].yaxis.label.set_size(12) # Colourbar label size
    # ax.tick_params(axis='x', labelsize=14)
    # ax.tick_params(axis='y', labelsize=14)
    ax.invert_yaxis()        
    fig.tight_layout()

    if Kippenhahn_properties['overlay_history_line'] != 'None':
        history_path = os.path.join(input_dir, 'history.data')
        hist = fmr.FastMesaReader(history_path, Kippenhahn_properties['overlay_history_line']) 
        line_array = hist.__getattr__(Kippenhahn_properties['overlay_history_line']) #str(self.overlay_history_line)
        line_array_for_plotting = []
        mnop = b_attribute.model_number_of_profiles
        increasing_x_axis = range(len(mnop))

        for k, mod_num in enumerate(mnop): 
            line_array_element = line_array[mod_num-1] # minus one as python arrays start at zero
            difference_array = np.absolute(c.dataframe_index_array-line_array_element)
            index = difference_array.argmin()
            line_array_for_plotting.append(float(index))

        ax.plot(increasing_x_axis, line_array_for_plotting, linewidth='2', color=Kippenhahn_properties['overlay_history_line_colour'], label=Kippenhahn_properties['overlay_history_line_label'])
        ax.legend()

    return fig


def make_3d_plot_2(input_dir, Kippenhahn_properties, 
    x_axis_array, linearized_y_axis_array, Kippenhahn_z_axis_variables_array,
    Kippenhahn_z_axis_variable, Kippenhahn_z_axis_scale):

    c = kipp_data_arrays.Kippenhan_Make_Data_Arrays(
        x_axis_array = x_axis_array,
        y_axis_array = linearized_y_axis_array,
        z_axis_array = Kippenhahn_z_axis_variables_array,
        y_axis_min_limit = Kippenhahn_properties['y_axis_min_limit'],
        y_axis_max_limit = Kippenhahn_properties['y_axis_max_limit'],
        format_x_axis = Kippenhahn_properties['format_x_axis'],
        format_y_axis = Kippenhahn_properties['format_y_axis'],
        x_axis_divide_by = Kippenhahn_properties['x_axis_divide_by'], 
        y_axis_divide_by = Kippenhahn_properties['y_axis_divide_by'],
        z_axis_data_min_cutoff = Kippenhahn_properties['z_axis_data_min_cutoff'],
        z_axis_data_min_cutoff_replacement = Kippenhahn_properties['z_axis_data_min_cutoff_replacement'],
        z_axis_data_max_cutoff = Kippenhahn_properties['z_axis_data_max_cutoff'],
        z_axis_data_max_cutoff_replacement = Kippenhahn_properties['z_axis_data_max_cutoff_replacement'],
        dataframe_z_axis_data_scale = Kippenhahn_z_axis_scale,
        num_y_axis_elements_for_heatmap = Kippenhahn_properties['num_y_axis_elements_for_heatmap'],
        )

    fig = plt.figure(figsize=(16,9.6))
    ax = fig.add_subplot(111)

    bar_scheme = Kippenhahn_properties['color_bar_scheme_heatmap']
    if Kippenhahn_z_axis_scale == 'log':
        bar_label = '$\\mathrm{LOG_{10}}$('+Kippenhahn_z_axis_variable+')'
    elif Kippenhahn_z_axis_scale == 'linear':
        bar_label = Kippenhahn_z_axis_variable

    X, Y = np.meshgrid(c.xlist, c.ylist)
    cs = ax.contourf(X, Y, c.zdata, levels=Kippenhahn_properties['levels_for_contourf_plot'], cmap=bar_scheme, antialiased=True)
    for c in cs.collections:
        c.set_rasterized(True)
    bar = fig.colorbar(cs)
    bar.set_label(bar_label)
    bar.outline.set_visible(False)

    ax.set_xlabel(Kippenhahn_properties['x_axis_label_heatmap'], fontsize=12)
    ax.set_ylabel(Kippenhahn_properties['y_axis_label_heatmap'], fontsize=12)
    ax.figure.axes[-1].yaxis.label.set_size(12) # Colourbar label size
    # ax.tick_params(axis='x', labelsize=14)
    # ax.tick_params(axis='y', labelsize=14)
    fig.tight_layout()

    if Kippenhahn_properties['overlay_history_line'] != 'None':
        hist = fmr.FastMesaReader(os.path.join(input_dir, 'history.data'), [Kippenhahn_properties['overlay_history_line'], Kippenhahn_properties['x_axis']]) 
        ax.plot(hist.__getattr__(Kippenhahn_properties['x_axis']), hist.__getattr__(Kippenhahn_properties['overlay_history_line']), linewidth='2', color=Kippenhahn_properties['overlay_history_line_colour'], label=Kippenhahn_properties['overlay_history_line_label'])
        ax.legend()

    return fig