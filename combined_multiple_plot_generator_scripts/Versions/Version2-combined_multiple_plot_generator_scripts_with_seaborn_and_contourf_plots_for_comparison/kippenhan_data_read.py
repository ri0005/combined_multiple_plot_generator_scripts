from os.path import join
import numpy as np
from collections import defaultdict
import sys
import pandas as pd
from benedict import benedict
import math

class Kipp_Data_Reader:

    def __init__(self, 
            profiles_paths=join('.', 'LOGS'),
            x_axis = 'star_age',              #either model_number or star_age
            y_axis = 'mass',
            z_axis = 'luminosity',    
            ):

        self.profiles_paths = profiles_paths
        self.x_axis = x_axis
        self.y_axis = y_axis
        self.z_axis = z_axis

        self.find_paths()


    def find_paths(self):
        self.all_col_names_for_attr = []
        self.plot_columns_names = []
        self.header_names = []
        self.header_vals = []
        self.header_data = {}
        x_axis_val = None

        self.bulk_numbers = []
        self.bulk_names = []
        self.col_numb = {}
        self.bulk_data = {}

        self.profile_numbers_array = []
        self.model_number_of_profiles = []

        self.all_col_names = self.x_axis+' '+self.y_axis
        self.all_col_names_for_attr = self.all_col_names.split()
        self.all_col_names_for_attr += self.z_axis

        self.plot_columns_names = self.y_axis.split() + self.z_axis  
        self.bulk_data[self.x_axis] = []                             #done this way as when checking plot_columns_names exist in the profile, x_axis is in header not columns.
        for name in self.plot_columns_names:                         #by user, -1 as python counts from zero
            self.bulk_data[name] = []

        if isinstance(self.profiles_paths,str):
            self.find_profiles(self.profiles_paths)
        elif isinstance(self.profiles_paths,list):
            for prof_path in self.profiles_paths:
                #reset the appropriate lists!
                self.profile_numbers_array.clear()
                self.find_profiles(prof_path)
        else:
            exit('profiles_paths must be a single string or a list (of paths)!')


    def find_profiles(self,path):
        self.read_the_profiles_index(path) #read the profiles indexes
        pr = 'profile'
        dat = '.data'
        for profile_num in self.profile_numbers_array:
            num = str(profile_num)
            cur_prof = pr+num+dat
            prof_and_path = join(path,cur_prof)
            self.read_profile_data(prof_and_path)
        print('All Profiles Read for Data Columns ', self.z_axis)


    def read_the_profiles_index(self,index_path):
        prof_index = 'profiles.index'
        index_and_path = join(index_path,prof_index)
        try:
            index_file = open(index_and_path, 'r')
        except FileNotFoundError:
            print(index_and_path, 'not found, does it exist? Is the path correct? Remembered ../ if applicable?')
            exit()
        index_file.readline()                                    #line 1 is headers
        temp_data = []
        for line in index_file:                         #loads each line of data file into memory one at a time 
            temp_data = line.split()
            self.profile_numbers_array.append(int(temp_data[2])) #the profilex.data x number in that directory
            self.model_number_of_profiles.append(int(temp_data[0]))


    def read_profile_data(self, file_name_and_path):
        temp_data = []

        #header stuff
        file = open(file_name_and_path, 'r')                   #open log file 
        file.readline()                                    #line 1 is just numbers, don't need
        self.header_names = file.readline().split()        #line 2 log file header names 
        self.header_vals = file.readline().split()         #line 3 log file header values 
        self.header_data = dict(zip(self.header_names, self.header_vals))  #make a dictionary containg header info

        if self.x_axis not in self.header_names:     #otherwise atribute error is flagged early on
            raise AttributeError(x_axis)
        x_axis_val = self.header_data[self.x_axis]                   #find x axis val from header data

        file.readline()                                    #line 4 blank
        self.bulk_numbers = file.readline().split()        #line 5 data column numbers
        self.bulk_names = file.readline().split()          #line 6 data column headers

        for name in self.plot_columns_names:    #loop to make sure atributes inputted by the user to plot exist, 
            if name not in self.bulk_names:     #otherwise atribute error is flagged early on
                raise AttributeError(name)

        # self.bulk_data[x_axis] = []
        for i, name in enumerate(self.bulk_names):                 #Find the column numbers of the "plot_columns" inputted
            if name in self.plot_columns_names:                    #by user, -1 as python counts from zero
                self.col_numb[name] = int(self.bulk_numbers[i])-1   
                # self.bulk_data[name] = []         #assigns key of dictionary as plot_column value input by 
                                                  #user, and assigns value of the key as a list to store data columns in
        for line in file:                         #loads each line of data file into memory one at a time 
            temp_data = line.split()              #splits line into constituent data column values
            self.bulk_data[self.x_axis].append(float(x_axis_val))
            for name in self.plot_columns_names:  #for each of the inputted "plot_column" names, retrieve data from line 
                self.bulk_data[name].append(float(temp_data[self.col_numb[name]])) #once correct index found append to dict lists

        file.close()    #finished reading file, not necessary but good practice 

        self.make_x_axis_and_reduced_linear_y_axis()

    def make_x_axis_and_reduced_linear_y_axis(self):

        # x_axis = np.array(self.bulk_data[self.x_axis])
        # y_axis = np.array(self.bulk_data[self.y_axis])

        self.x_axis_array = np.array(self.bulk_data[self.x_axis])
        self.linearized_y_axis = np.array(self.bulk_data[self.y_axis])


    def __getattr__(self, method_name):
        if self.in_bulk_data(method_name):
            return self.bulk_data_extraction_in_np_array(method_name)
        elif method_name == 'x_axis_array':
            return self.final_x_axis_return()
        elif method_name == 'linearized_y_axis':
            return self.linearized_y_axis_return()
        else:
            raise AttributeError(method_name)

    def in_bulk_data(self, key):
        return key in self.all_col_names_for_attr

    def bulk_data_extraction_in_np_array(self, key):
        # return np.asarray(self.bulk_data[key])
        return self.bulk_data[key]

    def linearized_y_axis_return(self):
        return self.linearized_y_axis

    def final_x_axis_return(self):
        return self.x_axis_array

