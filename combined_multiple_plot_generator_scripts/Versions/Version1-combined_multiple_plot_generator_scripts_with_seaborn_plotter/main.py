from general_plot_routine import general_plot_routine

# input_dir = '../LOGS/'
input_dir = '../LOGS_normal_functions/'
output_dir = 'pdfs'
twoD_plots_output_pdf_name = '2d_combined_output.pdf'
threeD_plots_output_pdf_name = '3d_combined_output.pdf'

global_properties_x_axis = {'plot_variable':'model_number', 'x_axis_divider':'None', 'x_axis_scale_type':'linear', 'x_axis_name_for_plots':'Model Number'}
global_properties_plots = {
    'photosphere_cell_T' : 'log',
    'photosphere_cell_density' : 'log',
    'photosphere_cell_opacity' : 'log',
    'photosphere_L' : 'log',
    'photosphere_r' : 'linear',
    'photosphere_m' : 'linear',
    'photosphere_v_km_s' : 'linear',
    'photosphere_v_div_cs' : 'linear',
    'star_mass': 'linear',
    'luminosity': 'log',
    'v_surf': 'log',
    'v_surf_div_escape_v': 'linear',
    'v_div_csound_surf': 'linear',
    'log_LH': 'linear',
    'log_LHe': 'linear',
    'log_LZ': 'linear',
    'log_Lnuc': 'linear',
    'pp': 'log',
    'cno': 'log',
    'tri_alfa': 'log',
    'total_mass_h1': 'linear',
    'total_mass_he4': 'linear',
    'total_mass_c12': 'linear',
    'total_mass_o16': 'linear',
    'max_T': 'log',
    'total_energy': 'linear',
    'total_angular_momentum': 'log',
    'dt_div_dt_cell_collapse': 'linear',
    'dt_div_max_tau_conv': 'linear',
    'min_t_eddy': 'log',
    'num_retries': 'linear',
    'num_iters': 'linear',
}

multi_line_plots_x_axis = {'plot_variable':'model_number', 'x_axis_divider':'None', 'x_axis_scale_type':'linear', 'x_axis_name_for_plots':'Model Number'}
multi_line_plots = {
    'plot1': 
        {'dynamic_timescale' : 'log', 'kh_timescale':'log', 'mdot_timescale':'log', 'nuc_timescale':'log'},
    'plot2': 
        {'total_mass_h1' : 'linear', 'total_mass_he4':'linear', 'total_mass_c12':'linear', 'total_mass_o16':'linear'},
    # 'plot3': 
    #     {'star_mass' : 'log', 'log_Teff': 'linear'},
}

only_plot_two_D_graphs = False

Kippenhahn_properties = {
            'x_axis' : 'model_number',
            'y_axis' : 'mass',
            'overlay_history_line' : 'wd_surf_mass_coord',
            'overlay_history_line_label' : 'WD Surface',
            'overlay_history_line_colour' : 'red',
            'y_axis_min_limit': 0.99998,
            'y_axis_max_limit': 'None' ,
            'format_x_axis' : 0,
            'format_y_axis' : 6,
            'x_axis_divide_by' : 'None', #'day'
            'y_axis_divide_by' : 'None',
            'z_axis_data_min_cutoff' : 'None', 
            'z_axis_data_min_cutoff_replacement' : 'None',
            'z_axis_data_max_cutoff' : 'None',
            'z_axis_data_max_cutoff_replacement' : 'None',
            'num_y_axis_elements_for_heatmap' : 1500,
            'x_axis_label_heatmap' : 'Model Number',
            'y_axis_label_heatmap' : 'Mass [M$_{\\odot}$]',
            'color_bar_scheme_heatmap' : 'viridis',
            }

Kippenhahn_plots = {
    'logT' : 'linear',
    'logRho' : 'linear',
    'logP' : 'linear',
    'eps_nuc' : 'log',
    'opacity' : 'linear',
    'energy' : 'log',
    'entropy' : 'log',
    'radius' : 'log',
    'v' : 'log',
    'log_v_escape' : 'linear',
    'v_div_vesc' : 'linear',
    'csound' : 'log',
    # 'v_div_cs': 'linear',
    'luminosity' : 'log',
    'log_Ledd' : 'linear',
    'log_L_div_Ledd' : 'linear',
    'grada' : 'linear',
    'gradT' : 'linear',
    'gradr' : 'linear',
    'conv_vel' : 'log',
    'log_D_mix' : 'linear',
    'conv_L_div_L' : 'log',
    'dm' : 'log',
    'dr' : 'log',
    'pressure_scale_height' : 'log',
    'prad' : 'log',
    'pgas' : 'log',
    'pp' : 'log',
    'cno' : 'log',
    'eta' : 'linear',
    'x_mass_fraction_H' : 'linear',
    'y_mass_fraction_He' : 'linear',
    'z_mass_fraction_metals' : 'linear',
}

comparing_at_unity_plots_x_axis = {'plot_variable':'model_number', 'x_axis_divider':'None', 'x_axis_scale_type':'linear', 'x_axis_name_for_plots':'Model Number'}
comparing_at_unity_plots = {
    'plot1': 
        ['v', 'csound'],
    'plot2': 
        ['v_div_vesc'],
}

general_plot_routine(input_dir, output_dir, twoD_plots_output_pdf_name, threeD_plots_output_pdf_name,
    global_properties_x_axis, global_properties_plots, 
    multi_line_plots_x_axis, multi_line_plots,
    only_plot_two_D_graphs, 
    Kippenhahn_properties, Kippenhahn_plots
    comparing_at_unity_plots_x_axis, comparing_at_unity_plots)


'''
Instructions:

Set the data input directory (the LOGS file) to the 'input_dir' variable.

Set the output directory for the generated PDFs to the 'output_dir' variable.

Set the name of the generated PDF files for the 2- and 3-D plots to the 'twoD_plots_output_pdf_name' and 'threeD_plots_output_pdf_name' variables respectively.

The dictionaries 'global_properties_x_axis' and 'multi_line_plots_x_axis' set the X-axis arguments for the generated 2-D plots. The keys:
    'plot_variable' Sets the X-axis variable. e.g. star_age or model_number.
    'x_axis_divider' Allows the units of the X-axis to be manipulated. Options are: None, sec, min, hr, day, yr, kyr, myr, gyr. Example: for 'plot_variable':'star_age' setting 'x_axis_divider':'day' will change the X-axis units to days. Set to None if not in use.
    'x_axis_scale_type' Allows the scale of the X-axis to be set to linear or log.
    'x_axis_name_for_plots' Sets the X-axis label.

The dictionary 'global_properties_plots' takes the Y-axis variables to be plotted as keys and the corresponding Y-axis scale types as the key value. These plots are single line plots.

The dictionary 'multi_line_plots' creates plots with multiple lines for the purpose of comparison. This is a nested dictionary. The keys take the plot titles as their arguments.
The key values take nested dictionaries as their arguments. The nested dictionaries take the Y-axis variables to be plotted as keys, and the corresponding Y-axis scale types 
as their keys values. There are no limits on the number of key-value pairs that can be given within the nested dictionaries. Multi-line plots are then created with the nested dicts.
The same multi-line plot can have both linear- and log-scale variables plotted on the same axes if desired by the user.

The boolean 'only_plot_two_D_graphs' only plots 2-D graphs if set to true, and skips plotting the Kippenhahn graphs.

The dictionary 'Kippenhahn_properties' Takes the arguments for the generated Kippenhahn plots. The keys:
    'x_axis' Sets the X-axis variable. e.g. star_age or model_number.
    'y_axis' Sets the Y-axis variable. e.g. star_mass or radius.
    'overlay_history_line' Allows a line from history.data to be overlayed the heatmap. Set to the variable name in history.data,  e.g. 'mass_conv_core. Set to None if not in use.
    'overlay_history_line_label' Sets the legend label for the overlayed history line (if used). Set to None if not in use. 
    'overlay_history_line_colour' Sets the colour of the overlayed history line (if used). Set to None if not in use.
    'y_axis_min_limit': Sets the minimum Y-axis value to start from. e.g if Y-axis is mass, but you want to plot from mass value 0.9 Msun to the surface, set 'y_axis_min_limit': 0.9 
    'y_axis_max_limit': Sets the maximum Y-axis value to be plotted. e.g if Y-axis is mass, but you want to plot from the core to mass value 0.8 Msun, set 'y_axis_max_limit': 0.8 
    'format_x_axis' Formats the X-axis ticks to a certain number of decimal places. e.g. 'format_x_axis':3 allows ticks to have 3 decimal places. Set to None if not in use.
    'format_y_axis' Formats the Y-axis ticks to a certain number of decimal places. e.g. 'format_y_axis':7 allows ticks to have 7 decimal places. Set to None if not in use.
    'x_axis_divide_by' Allows the units of the X-axis to be manipulated. Options are: None, sec, min, hr, day, yr, kyr, myr, gyr. Example: for 'plot_variable':'star_age' setting 'x_axis_divider':'day' will change the X-axis units to days. Set to None if not in use.
    'y_axis_divide_by' Allows the units of the Y-axis to be manipulated. e.g. for 'Y_axis':'radius_cm' setting 'y_axis_divide_by':1e-2 will change the Y-axis units to metres. Set to None if not in use.
    'z_axis_data_min_cutoff' : If any of the Z-axis data elements are less than 'z_axis_data_min_cutoff', then those Z-axis data elements are set to 'z_axis_data_min_cutoff_replacement'. e.g if 'z_axis_data_min_cutoff':0e0 and 'z_axis_data_min_cutoff_replacement':0e0, then any Z-axis data elements below zero will be set to zero. Set to None if not in use.
    'z_axis_data_min_cutoff_replacement' : Sets the value a Z-axis element will be replaced with if its value is less than 'z_axis_data_min_cutoff'. Set to None if not in use.
    'z_axis_data_max_cutoff' If any of the Z-axis data elements are greater than 'z_axis_data_max_cutoff', then those Z-axis data elements are set to 'z_axis_data_max_cutoff_replacement'. e.g if 'z_axis_data_max_cutoff':1e2 and 'z_axis_data_min_cutoff_replacement':1e2, then any Z-axis data elements greater than 100 will be set to 100. Set to None if not in use.
    'z_axis_data_max_cutoff_replacement' Sets the value a Z-axis element will be replaced with if its value is greater than 'z_axis_data_max_cutoff'. Set to None if not in use.
    'num_y_axis_elements_for_heatmap' The number of data points the Y-axis will have on the heatmap. A greater number will give more resolution to the heatmap.
    'x_axis_label_heatmap' The label for the heatmaps x-axis. e.g. 'Star Age [day]'.
    'y_axis_label_heatmap' The label for the heatmaps Y-axis. e.g. 'Mass [M$_{\\odot}$]'.
    'color_bar_scheme_heatmap' Sets the colour scheme of the heatmaps colour bar . For matplotlib colours, go to https://matplotlib.org/stable/tutorials/colors/colormaps.html

The dictionary 'Kippenhahn_plots' takes the Y-axis variables to be plotted as the key and the Y-axis scale type as the corresponding key value. 
'''