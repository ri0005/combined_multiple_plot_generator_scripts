'''
TO DO:
- Add parallelisation to data reading section.
- Update find_num_zones in kippenhahn_data_read to accomodate multiple directory input paths.
- Incorporate David's bookmarkign feature for the PDFs.
- Change global_properties_x_axis Kippenhahn_properties dicts key names to be clearer and better.
- Incorporate keywaord args to replace positional function call args.
- Make a external text file with the profile variables as keys and associated values with publishable definitions. 
'''

import os
import sys
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as mticker
import historyreader as hr
import kippenhan_data_read as kipp_data_read
import kippenhan_make_data_arrays as kipp_data_arrays
# import seaborn as sns #to delete
import math as math
import multiprocessing as mp
from matplotlib.colors import TwoSlopeNorm
import traceback

def general_plot_routine(input_directory, output_directory, two_D_combined_pdf_filename, three_D_combined_pdf_filename, 
    global_properties_x_axis_args, global_properties_plot_vars, 
    multi_line_plots_x_axis_args, multi_line_plots_vars,
    only_plot_two_D_graphs_bool, 
    Kippenhahn_args, Kippenhahn_plot_vars, comparing_at_unity_plots_vars):

    os.makedirs(output_directory, exist_ok=True) # Make output dir if necessary:
    history_path = os.path.join(input_directory, 'history.data')
    twoD_out_path_and_name = os.path.join(output_directory, two_D_combined_pdf_filename)
    threeD_out_path_and_name = os.path.join(output_directory, three_D_combined_pdf_filename)

    global_properties_x_axis_args['x_axis_divider'] = set_divider_value(global_properties_x_axis_args['x_axis_divider'])
    multi_line_plots_x_axis_args['x_axis_divider'] = set_divider_value(multi_line_plots_x_axis_args['x_axis_divider'])
    Kippenhahn_args['x_axis_divide_by'] = set_divider_value(Kippenhahn_args['x_axis_divide_by'])

    multi_line_plots_variables = [kys for d in multi_line_plots_vars.values() for kys in d.keys()]

# y_axis_dict[variable]['divide_by']

    global_properties_y_axis_variables = list(global_properties_plot_vars.keys())
    global_properties_y_axis_divide_by_variables = [a['divide_by'] for a in global_properties_plot_vars.values() if a['divide_by'].lower() != 'none']
    twoD_all_plotting_vars = [global_properties_x_axis_args['plot_variable']] + [multi_line_plots_x_axis_args['plot_variable']] + global_properties_y_axis_variables + global_properties_y_axis_divide_by_variables + multi_line_plots_variables
    twoD_all_plotting_vars = list(dict.fromkeys(twoD_all_plotting_vars)) # Removes repeats from list

    if len(global_properties_plot_vars) > 0 or len(multi_line_plots_variables) > 0:
        a = hr.HistoryReader(history_path, 'None', 'None', twoD_all_plotting_vars) 

    pdf = PdfPages(twoD_out_path_and_name)
    # Make 2D graphs
    if len(global_properties_plot_vars) > 0:
        for key in global_properties_plot_vars:
            fig = make_2d_plot(a, global_properties_x_axis_args, global_properties_plot_vars, key)
            pdf.savefig(fig)

    # Make multiplot graphs 
    if len(multi_line_plots_variables) > 0:
        for ttls in multi_line_plots_vars.keys():
            fig = make_2d_multiline_plot(a, ttls, multi_line_plots_vars, multi_line_plots_x_axis_args)
            pdf.savefig(fig)
    pdf.close()
    print('2D Plots PDF Created')

    if only_plot_two_D_graphs_bool is True:
        sys.exit()

    Kippenhahn_z_axis_variables = list(Kippenhahn_plot_vars.keys())
    comparing_at_unity_list_of_lists = [lst for vals in comparing_at_unity_plots_vars.values() for lst in vals['plot_variables']] #this produces a list of lists, for example 
    # comparing_at_unity_z_axis_variables = list(dict.fromkeys([item for sublist in comparing_at_unity_list_of_lists for item in sublist])) # this makes one list from the list of lists
    comparing_unity_plot_titles = list(comparing_at_unity_plots_vars.keys())
    all_z_axis_variables = Kippenhahn_z_axis_variables + comparing_at_unity_list_of_lists #+ comparing_at_unity_z_axis_variables
    print(all_z_axis_variables)
    if len(all_z_axis_variables) == 0:
        sys.exit()

    # if Kippenhahn_args['CPUs_for_multiprocessing'] == 'all':
    #     pool = mp.Pool(mp.cpu_count())
    # elif isinstance(Kippenhahn_args['CPUs_for_multiprocessing'], int):
    #     pool = mp.Pool(Kippenhahn_args['CPUs_for_multiprocessing'])
    # else:
    #     print('Set CPUs_for_multiprocessing var as an integer or as all')
    #     sys.exit()

    b = kipp_data_read.Kipp_Data_Reader(
        profiles_paths = input_directory,
        x_axis_var = Kippenhahn_args['x_axis'],
        y_axis_var = Kippenhahn_args['y_axis'],
        z_axis_vars = all_z_axis_variables,
        profile_read_interval = Kippenhahn_args['profile_read_interval'],
        model_number_start = Kippenhahn_args['model_number_start'],
        model_number_end = Kippenhahn_args['model_number_end'],
        )

    pdf = PdfPages(threeD_out_path_and_name)
    # Make standard Kippenhahn plots
    if len(Kippenhahn_z_axis_variables) > 0:
        if Kippenhahn_args['overlay_history_line'].lower() != 'none':
            hist_attr = hr.HistoryReader(os.path.join(input_directory, 'history.data'), Kippenhahn_args['model_number_start'], Kippenhahn_args['model_number_end'], [Kippenhahn_args['overlay_history_line'], Kippenhahn_args['x_axis']]) 
        for var in Kippenhahn_plot_vars.keys():
            try:
                fig = make_3d_plot(input_directory, Kippenhahn_args, 
                    b.x_axis_array, b.y_axis_array, b.__getattr__(var),
                    var, Kippenhahn_plot_vars, hist_attr)
                # fig = pool.apply_async(make_3d_plot, args=(input_directory, Kippenhahn_args, 
                #     b.x_axis_array, b.y_axis_array, b.__getattr__(Kippenhahn_z_axis_variables[i]),
                #     Kippenhahn_z_axis_variables[i], Kippenhahn_z_axis_scale_type[i])).get()
                pdf.savefig(fig)
                print('Kippenhahn Plot Created for ', var)
            except KeyboardInterrupt:
                sys.exit()
            except:
                print('***AN EXCEPTION OCCURED FOR ', var, '*****')
                print(' see traceback error below ::')
                traceback.print_exc()
                print(' ')

    # pool.close()
    # pool.join()

    # Make unity comparison Kippenhahn plots
    if len(comparing_at_unity_list_of_lists) > 0:
        if Kippenhahn_args['overlay_history_line'].lower() != 'none':
            hist_attr = hr.HistoryReader(os.path.join(input_directory, 'history.data'), Kippenhahn_args['model_number_start'], Kippenhahn_args['model_number_end'], [Kippenhahn_args['overlay_history_line'], Kippenhahn_args['x_axis']]) 
        for i, lst in enumerate(comparing_at_unity_plots_vars.values()):
            try:
                plot_vars = lst['plot_variables']
                colour_bar_name = lst['colour_bar_name']
                set_title = lst['set_title']
                fig = make_unity_comparison_plot(b, input_directory, Kippenhahn_args, 
                    comparing_unity_plot_titles[i], b.x_axis_array, b.y_axis_array, plot_vars, colour_bar_name, set_title, hist_attr)
                pdf.savefig(fig)
                print('Unity Comparison Kippenhahn Plot Created for ', plot_vars)
            except KeyboardInterrupt:
                sys.exit()
            except:
                print('***AN EXCEPTION OCCURED FOR ', lst, '*****')
                print(' see traceback error below ::')
                traceback.print_exc()
                print(' ')
    pdf.close()
    print('Kippenhahn Plots PDF Created')



def set_divider_value(divider_value):
    if str(divider_value).lower() == 'none':
        return 1
    elif str(divider_value).lower() == 'sec':
        return 1/31536000
    elif str(divider_value).lower() == 'min':
        return 1/525600
    elif str(divider_value).lower() == 'hr':
        return 1/8760
    elif str(divider_value).lower() == 'day':
        return 1/365
    elif str(divider_value).lower() == 'yr':
        return 1
    elif str(divider_value).lower() == 'kyr':
        return 1e3
    elif str(divider_value).lower() == 'myr':
        return 1e6
    elif str(divider_value).lower() == 'gyr':
        return 1e9
    elif divider_value.lower() == 1:
        return 1
    else:
        print('X-axis divider {unk_div_val} is not recognised.'.format(unk_div_val=divider_value))
        print(' Options are: None, sec, min, hr, day, yr, kyr, myr, gyr.')
        print('Stopping')
        sys.exit()


def make_2d_plot(a_attribute, x_axis_options, y_axis_dict, variable):

    fig = plt.figure(figsize=(12.8,9.6))
    ax = fig.add_subplot(111)

    if str(y_axis_dict[variable]['yaxis_label']).lower() == 'none':
        y_axis_label = variable
    else:
        y_axis_label = y_axis_dict[variable]['yaxis_label']
    y_axis_scale = y_axis_dict[variable]['scale_type']

    if y_axis_dict[variable]['divide_by'].lower() == 'none':
        y_array = a_attribute.__getattr__(variable)
    elif y_axis_dict[variable]['divide_by'].lower() != 'none':
        y_array = a_attribute.__getattr__(variable)/a_attribute.__getattr__(y_axis_dict[variable]['divide_by'])
    x_array = a_attribute.__getattr__(x_axis_options['plot_variable'])

    ax.plot(x_array/x_axis_options['x_axis_divider'], y_array, linewidth='2')
    ax.set_xscale(x_axis_options['x_axis_scale_type'])
    ax.set_yscale(y_axis_scale)
    if x_axis_options['x_axis_scale_type'] == 'linear':
        ax.ticklabel_format(axis='x', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    if y_axis_scale == 'linear':
        ax.ticklabel_format(axis='y', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    ax.set_xlabel(x_axis_options['x_axis_name_for_plots'], fontsize=20)
    ax.set_ylabel(y_axis_label, fontsize=20)

    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)

    fig.tight_layout()

    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.grid(which='major', linestyle=':', linewidth='0.8')
    plt.minorticks_on()
    ax.grid(which='minor', linestyle=':', linewidth='0.4')
    # ax.ticklabel_format(axis='both', style='plain', scilimits=(0,0), useOffset=False)

    return fig


def make_2d_multiline_plot(a_attribute, plot_title, multi_line_plots_dict, x_axis_options):
    
    fig = plt.figure(figsize=(12.8,9.6))
    ax = fig.add_subplot(111)

    y_scale_list = [vals for vals in multi_line_plots_dict[plot_title].values()]
    y_scale_check = all(ele == y_scale_list[0] for ele in y_scale_list) # Check to see if all plotting args have the same scale, return True if all are same and false if not.
    
    colors = plt.cm.viridis(np.linspace(0,1,len(y_scale_list))) # use y_scale_list for len as it already exists - its just convenience

    if y_scale_check: # If all plotting args have the same y scale, ax.set_yscale is just set to first elements scale, and checks are done for plotting linear or log
        for i, vrbls in enumerate(multi_line_plots_dict[plot_title].keys()):
            ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], a_attribute.__getattr__(vrbls), linewidth='1.5', label=vrbls, color=colors[i])
        ax.set_yscale(y_scale_list[0])
        if y_scale_list[0] == 'linear':
            ax.ticklabel_format(axis='y', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    else:
        for i, vrbls in enumerate(multi_line_plots_dict[plot_title].keys()):
            if multi_line_plots_dict[plot_title][vrbls] == 'log':
                ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], np.log10(a_attribute.__getattr__(vrbls)), linewidth='1.5', label='$\\mathrm{LOG_{10}}$('+vrbls+')', color=colors[i]) 
            else:
                ax.plot(a_attribute.__getattr__(x_axis_options['plot_variable'])/x_axis_options['x_axis_divider'], a_attribute.__getattr__(vrbls), linewidth='1.5', label=vrbls, color=colors[i])
        ax.set_yscale('linear')
        ax.ticklabel_format(axis='y', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    ax.set_xscale(x_axis_options['x_axis_scale_type'])
    if x_axis_options['x_axis_scale_type'] == 'linear':
        ax.ticklabel_format(axis='x', style=x_axis_options['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    if x_axis_options['x_axis_scale_type'] == 'log':
        ax.set_xlabel('$\\mathrm{LOG_{10}}$('+x_axis_options['x_axis_name_for_plots']+')', fontsize=18)
    else:
        ax.set_xlabel(x_axis_options['x_axis_name_for_plots'], fontsize=18)

    ax.set_title(plot_title, fontsize=22)

    ax.tick_params(axis='x', labelsize=14)
    ax.tick_params(axis='y', labelsize=14)

    ax.legend(prop={"size":14})
    fig.tight_layout()
    ax.xaxis.set_minor_locator(mticker.AutoMinorLocator())
    ax.grid(which='major', linestyle=':', linewidth='0.8')
    plt.minorticks_on()
    ax.grid(which='minor', linestyle=':', linewidth='0.4')

    return fig


def make_3d_plot(input_dir, Kippenhahn_properties, 
    x_axis_array, y_axis_array, Kippenhahn_z_axis_variables_array,
    Kippenhahn_z_axis_variable, Kippenhahn_plot_vars, hist_attr):

    Kippenhahn_z_axis_scale = Kippenhahn_plot_vars[Kippenhahn_z_axis_variable]['scale_type']
    c = kipp_data_arrays.Kippenhan_Make_Data_Arrays(
        x_axis_array = x_axis_array,
        y_axis_array = y_axis_array,
        z_axis_array = Kippenhahn_z_axis_variables_array,
        format_x_axis = Kippenhahn_properties['format_x_axis'],
        format_y_axis = Kippenhahn_properties['format_y_axis'],
        x_axis_divide_by = Kippenhahn_properties['x_axis_divide_by'], 
        y_axis_divide_by = Kippenhahn_properties['y_axis_divide_by'],
        z_axis_data_min_cutoff = Kippenhahn_properties['z_axis_data_min_cutoff'],
        z_axis_data_min_cutoff_replacement = Kippenhahn_properties['z_axis_data_min_cutoff_replacement'],
        z_axis_data_max_cutoff = Kippenhahn_properties['z_axis_data_max_cutoff'],
        z_axis_data_max_cutoff_replacement = Kippenhahn_properties['z_axis_data_max_cutoff_replacement'],
        z_axis_data_scale = Kippenhahn_z_axis_scale,
        num_y_axis_elements_for_heatmap = Kippenhahn_properties['num_y_axis_elements_for_heatmap'],
        )

    fig = plt.figure(figsize=(16,9.6))
    ax = fig.add_subplot(111)

    bar_scheme = Kippenhahn_properties['color_bar_scheme_heatmap']
    if str(Kippenhahn_plot_vars[Kippenhahn_z_axis_variable]['colour_bar_name']).lower() == 'none':
        if Kippenhahn_z_axis_scale == 'log':
            bar_label = '$\\mathrm{LOG_{10}}$('+Kippenhahn_z_axis_variable+')'
        else: #elif Kippenhahn_z_axis_scale == 'linear':
            bar_label = Kippenhahn_z_axis_variable
    else:
        bar_label = Kippenhahn_plot_vars[Kippenhahn_z_axis_variable]['colour_bar_name']

    X, Y = np.meshgrid(c.xlist, c.ylist)
    if str(Kippenhahn_properties['colorbar_min_val']).lower() != 'none':
        colorbar_min_val = Kippenhahn_properties['colorbar_min_val']
    else:
        colorbar_min_val = None
    if str(Kippenhahn_properties['colorbar_max_val']).lower() != 'none':
        colorbar_max_val = Kippenhahn_properties['colorbar_max_val']
    else:
        colorbar_max_val = None

    cs = ax.contourf(X, Y, c.zdata, levels=Kippenhahn_properties['levels_for_contourf_plot'], vmin=colorbar_min_val, vmax=colorbar_max_val, cmap=bar_scheme, antialiased=True)
    if colorbar_max_val == None and colorbar_min_val == None:
        bar = fig.colorbar(cs, pad=0.015)
    else:
        bar = clippedcolorbar(cs, fig, Kippenhahn_properties['levels_for_contourf_plot'], extend='neither', pad=0.02) #clips the colourbar to vmin and vmax
    for c in cs.collections:
        c.set_rasterized(True)
    bar.set_label(bar_label)
    bar.outline.set_visible(False)
    bar.ax.tick_params(labelsize=16) 
    ax.figure.axes[-1].yaxis.label.set_size(20) # Colourbar label size

    ax.set_xlabel(Kippenhahn_properties['x_axis_label_heatmap'], fontsize=20)
    ax.set_ylabel(Kippenhahn_properties['y_axis_label_heatmap'], fontsize=20)
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    fig.tight_layout(rect=[0.04, 0.02, 1.05, 0.98])

    ax.ticklabel_format(axis='x', style=Kippenhahn_properties['x_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    ax.ticklabel_format(axis='y', style=Kippenhahn_properties['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    if str(Kippenhahn_properties['x_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['x_axis_max_limit']).lower() == 'none':
        ax.set_xlim(left=Kippenhahn_properties['x_axis_min_limit'])
    elif str(Kippenhahn_properties['x_axis_max_limit']).lower() != 'none' and str(Kippenhahn_properties['x_axis_min_limit']).lower() == 'none':
        ax.set_xlim(right=Kippenhahn_properties['x_axis_max_limit'])
    elif str(Kippenhahn_properties['x_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['x_axis_max_limit']).lower() != 'none':
        ax.set_xlim(left=Kippenhahn_properties['x_axis_min_limit'], right=Kippenhahn_properties['x_axis_max_limit'])

    if str(Kippenhahn_properties['y_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['y_axis_max_limit']).lower() == 'none':
        ax.set_ylim(bottom=Kippenhahn_properties['y_axis_min_limit'])
    elif str(Kippenhahn_properties['y_axis_max_limit']).lower() != 'none' and str(Kippenhahn_properties['y_axis_min_limit']).lower() == 'none':
        ax.set_ylim(top=Kippenhahn_properties['y_axis_max_limit'])
    elif str(Kippenhahn_properties['y_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['y_axis_max_limit']).lower() != 'none':
        ax.set_ylim(bottom=Kippenhahn_properties['y_axis_min_limit'], top=Kippenhahn_properties['y_axis_max_limit'])

    if Kippenhahn_properties['overlay_history_line'].lower() != 'none':
        ax.plot(hist_attr.__getattr__(Kippenhahn_properties['x_axis'])/Kippenhahn_properties['x_axis_divide_by'], hist_attr.__getattr__(Kippenhahn_properties['overlay_history_line']), linewidth='2', color=Kippenhahn_properties['overlay_history_line_colour'], label=Kippenhahn_properties['overlay_history_line_label'])
        ax.legend(fontsize=18,frameon=False)

    return fig


def make_unity_comparison_plot(b_attribute, input_dir, Kippenhahn_properties, 
    plot_title, x_axis_array, y_axis_array, list_for_comparison, colour_bar_name, set_title, hist_attr):

    if len(list_for_comparison) == 1:
        Kippenhahn_z_axis_variables_array = np.array(b_attribute.__getattr__(list_for_comparison[0]))
        bar_label = list_for_comparison[0]
    elif len(list_for_comparison) == 2:
        Kippenhahn_z_axis_variables_array = np.divide(b_attribute.__getattr__(list_for_comparison[0]),b_attribute.__getattr__(list_for_comparison[1]))
        bar_label = list_for_comparison[0]+'/'+list_for_comparison[1]
    else:
        print('*********** comparing_at_unity_plots dict values must be lists of either 1 or 2 elements only. ***********')
        print('*********** Pleae correct dict value:', list_for_comparison, '***********')
        return plt.figure()
    # Kippenhahn_z_axis_variables_array[Kippenhahn_z_axis_variables_array < 0e0] = 0e0  # Makes any value in Z-array greater than 2 equal to 2
    # Kippenhahn_z_axis_variables_array[Kippenhahn_z_axis_variables_array > 2e0] = 2e0  # Makes any value in Z-array less than 0 equal to zero
    # Kippenhahn_z_axis_variables_array[Kippenhahn_z_axis_variables_array < 1e0] = 0e0  # Makes any value in Z-array greater than 2 equal to 2

    # min_z_val = math.floor(min(Kippenhahn_z_axis_variables_array))
    # max_z_val = math.ceil(max(Kippenhahn_z_axis_variables_array))+1
    # levels_list = np.arange(min_z_val,max_z_val)

    c = kipp_data_arrays.Kippenhan_Make_Data_Arrays(
        x_axis_array = x_axis_array,
        y_axis_array = y_axis_array,
        z_axis_array = Kippenhahn_z_axis_variables_array,
        format_x_axis = Kippenhahn_properties['format_x_axis'],
        format_y_axis = Kippenhahn_properties['format_y_axis'],
        x_axis_divide_by = Kippenhahn_properties['x_axis_divide_by'], 
        y_axis_divide_by = Kippenhahn_properties['y_axis_divide_by'],
        z_axis_data_min_cutoff = 'None',
        z_axis_data_min_cutoff_replacement = 'None',
        z_axis_data_max_cutoff = 'None',
        z_axis_data_max_cutoff_replacement = 'None',
        z_axis_data_scale = 'linear',
        num_y_axis_elements_for_heatmap = Kippenhahn_properties['num_y_axis_elements_for_heatmap'],
        )
    interval = 0.1
    min_z_val = math.floor(min(map(min, c.zdata))*10e0)/10 #done this way to round to lowest decimal place 
    max_z_val = (math.ceil(max(map(max, c.zdata))*10e0)/10)+interval #done this way to round to highest decimal place, + 0.1 as arrange below will iterate to max_z_val-0.1
    if np.log10(max_z_val - min_z_val) >= 2e0:
        log_the_data = True
        zdata = np.log10(c.zdata)
        min_z_val = math.floor(min(map(min, zdata))*10e0)/10 #done this way to round to lowest decimal place 
        max_z_val = (math.ceil(max(map(max, zdata))*10e0)/10)+interval #done this way to round to highest decimal place, + 0.1 as arrange below will iterate to max_z_val-0.1
    else:        
        log_the_data = False
        zdata = c.zdata

    levels_list = np.arange(min_z_val,max_z_val,interval)

    fig = plt.figure(figsize=(16,9.6))
    ax = fig.add_subplot(111)

    bar_scheme = Kippenhahn_properties['color_bar_scheme_heatmap']

    X, Y = np.meshgrid(c.xlist, c.ylist)
    if str(Kippenhahn_properties['colorbar_min_val']).lower() != 'none':
        colorbar_min_val = Kippenhahn_properties['colorbar_min_val']
    else:
        colorbar_min_val = None
    if str(Kippenhahn_properties['colorbar_max_val']).lower() != 'none':
        colorbar_max_val = Kippenhahn_properties['colorbar_max_val']
    else:
        colorbar_max_val = None
    cs = ax.contourf(X, Y, zdata, levels=levels_list, vmin=colorbar_min_val, vmax=colorbar_max_val, cmap='seismic', norm=TwoSlopeNorm(0), antialiased=True)
    if colorbar_max_val == None and colorbar_min_val == None:
        bar = fig.colorbar(cs, pad=0.015)
    else:
        bar = clippedcolorbar(cs, fig, Kippenhahn_properties['levels_for_contourf_plot'], extend='neither', pad=0.015) #clips the colourbar to vmin and vmax
    bar.outline.set_visible(False)
    for c in cs.collections:
        c.set_rasterized(True)
    if log_the_data:
        bar_label = '$\\mathrm{LOG_{10}}$('+bar_label+')'
    else:
        cs2 = ax.contour(X, Y, zdata, [1e0], colors='green') # https://matplotlib.org/2.0.2/examples/pylab_examples/contourf_demo.html
        ax.clabel(cs2, inline=True, fontsize=10)
        bar.add_lines(cs2)
    
    if str(colour_bar_name).lower() != 'none':
        bar_label = colour_bar_name

    bar.set_label(bar_label)
    bar.ax.tick_params(labelsize=16) 
    ax.figure.axes[-1].yaxis.label.set_size(20) # Colourbar label size

    ax.set_xlabel(Kippenhahn_properties['x_axis_label_heatmap'], fontsize=20)
    ax.set_ylabel(Kippenhahn_properties['y_axis_label_heatmap'], fontsize=20)
    if set_title:
        ax.set_title(plot_title, fontsize=22)
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    fig.tight_layout(rect=[0.04, 0.02, 1.05, 0.98])  # rect=[left, bottom, right, top] specifies the bounding box that the plots will be fit inside. The coordinates must be in normalized figure coordinates and the default is (0, 0, 1, 1).

    ax.ticklabel_format(axis='x', style=Kippenhahn_properties['x_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)
    ax.ticklabel_format(axis='y', style=Kippenhahn_properties['y_axis_ticklabel_format'], scilimits=(0,0), useOffset=False)

    if str(Kippenhahn_properties['x_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['x_axis_max_limit']).lower() == 'none':
        ax.set_xlim(left=Kippenhahn_properties['x_axis_min_limit'])
    elif str(Kippenhahn_properties['x_axis_max_limit']).lower() != 'none' and str(Kippenhahn_properties['x_axis_min_limit']).lower() == 'none':
        ax.set_xlim(right=Kippenhahn_properties['x_axis_max_limit'])
    elif str(Kippenhahn_properties['x_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['x_axis_max_limit']).lower() != 'none':
        ax.set_xlim(left=Kippenhahn_properties['x_axis_min_limit'], right=Kippenhahn_properties['x_axis_max_limit'])

    if str(Kippenhahn_properties['y_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['y_axis_max_limit']).lower() == 'none':
        ax.set_ylim(bottom=Kippenhahn_properties['y_axis_min_limit'])
    elif str(Kippenhahn_properties['y_axis_max_limit']).lower() != 'none' and str(Kippenhahn_properties['y_axis_min_limit']).lower() == 'none':
        ax.set_ylim(top=Kippenhahn_properties['y_axis_max_limit'])
    elif str(Kippenhahn_properties['y_axis_min_limit']).lower() != 'none' and str(Kippenhahn_properties['y_axis_max_limit']).lower() != 'none':
        ax.set_ylim(bottom=Kippenhahn_properties['y_axis_min_limit'], top=Kippenhahn_properties['y_axis_max_limit'])

    if Kippenhahn_properties['overlay_history_line'].lower() != 'none':
        ax.plot(hist_attr.__getattr__(Kippenhahn_properties['x_axis'])/Kippenhahn_properties['x_axis_divide_by'], hist_attr.__getattr__(Kippenhahn_properties['overlay_history_line']), linewidth='2', color='k', label=Kippenhahn_properties['overlay_history_line_label'])
        ax.legend(fontsize=18,frameon=False)

    return fig


def clippedcolorbar(CS, fig, levels, **kwargs):
    from matplotlib.cm import ScalarMappable
    from numpy import arange, floor, ceil
    # fig = CS.ax.get_figure()
    vmin = CS.get_clim()[0]
    vmax = CS.get_clim()[1]
    m = ScalarMappable(cmap=CS.get_cmap())
    m.set_array(CS.get_array())
    m.set_clim(CS.get_clim())
    boundaries = np.linspace(vmin, vmax, levels)
    kwargs['boundaries'] = boundaries
    return fig.colorbar(m, **kwargs)

