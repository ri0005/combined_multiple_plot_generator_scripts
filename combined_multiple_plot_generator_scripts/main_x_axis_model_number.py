from general_plot_routine import general_plot_routine

# input_dir = '../LOGS_to_model_2000/'
input_dir = '../LOGS/'
output_dir = 'pdfs_x_axis_model_number'
twoD_plots_output_pdf_name = '2d_combined_output.pdf'
threeD_plots_output_pdf_name = '3d_combined_output.pdf'

global_properties_x_axis = {'plot_variable':'model_number', 'x_axis_divider':'None', 'x_axis_scale_type':'linear', 'x_axis_name_for_plots':'Model Number', 'x_axis_ticklabel_format': 'plain', 'y_axis_ticklabel_format': 'plain'}
global_properties_plots = {
    'star_mass': {'yaxis_label':'Star mass [$\\mathrm{M_{\odot}}$]', 'scale_type':'linear', 'divide_by':'None'},
    'luminosity': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Luminosity \\,/\\, L_{\\odot} \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    'total_extra_energy_erg': {'yaxis_label':'$\\sum E_{\\mathrm{inject}}$ / $\\sum \\epsilon_{\\mathrm{nuc}}$', 'scale_type':'log', 'divide_by':'total_energy_erg'},
    # 'v_surf': 'log',
    # 'v_surf_div_escape_v': 'linear',
    # 'v_div_csound_surf': 'linear',
    'log_LH': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Integrated \\ luminosity \\ from \\ H \\ burning \\,/\\, L_{\\odot} \\right)}$', 'scale_type':'linear', 'divide_by':'None'},
    'log_LHe': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Integrated \\ luminosity \\ from \\ He \\ burning \\,/\\, L_{\\odot} \\right)}$', 'scale_type':'linear', 'divide_by':'None'},
    'log_LZ': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Integrated \\ luminosity \\ from \\ Z \\ burning \\,/\\, L_{\\odot} \\right)}$', 'scale_type':'linear', 'divide_by':'None'},
    'log_Lnuc': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Integrated \\ nuclear \\ luminosity \\,/\\, L_{\\odot} \\right)}$', 'scale_type':'linear', 'divide_by':'None'},
    'pp': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Integrated \\ luminosity \\ from \\ pp \\ reactions \\,/\\, L_{\\odot} \\right)}$', 'scale_type':'symlog', 'divide_by':'None'},
    'cno': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Integrated \\ luminosity \\ from \\ CNO \\ reactions \\,/\\, L_{\\odot} \\right)}$', 'scale_type':'symlog', 'divide_by':'None'},
    # 'tri_alfa': 'log',
    'photosphere_cell_T' : {'yaxis_label':'$\\mathrm{Log_{10}\\left(Photosphere \\ temperature \\,/\\, K \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    'photosphere_cell_density' : {'yaxis_label':'$\\mathrm{Log_{10}\\left(Photosphere \\ density \\,/\\, g \\  cm^{-3} \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    'photosphere_cell_opacity' : {'yaxis_label':'$\\mathrm{Log_{10}\\left(Photosphere \\ opacity \\,/\\, cm^{2} \\, g^{-1} \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    # 'photosphere_L' : {'yaxis_label':'None', 'scale_type':'log'},
    'photosphere_r' : {'yaxis_label':'$\\mathrm{Log_{10}\\left(Photosphere \\ radius \\,/\\, R_{\odot} \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    # 'photosphere_v_km_s' : 'linear',
    # 'photosphere_v_div_cs' : 'linear',
    'total_mass_h1': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Total \\ mass \\ ^{1}H \\,/\\, M_{\\odot} \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    'total_mass_he4': {'yaxis_label':'$\\mathrm{Log_{10}\\left( Total \\ mass \\ ^{4}He \\,/\\, M_{\\odot} \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    'total_mass_c12': {'yaxis_label':'$\\mathrm{Total \\ mass \\ ^{12}C \\ [M_{\\odot}}]$', 'scale_type':'linear', 'divide_by':'None'},
    'total_mass_o16': {'yaxis_label':'$\\mathrm{Total \\ mass \\ ^{16}O \\ [M_{\\odot}}]$', 'scale_type':'linear', 'divide_by':'None'},
    'max_T': {'yaxis_label':'$\\mathrm{Log_{10}\\left(Maximum \\ star \\ temperature \\,/\\, K \\right)}$', 'scale_type':'log', 'divide_by':'None'},
    # 'total_energy': 'linear',
    # 'total_angular_momentum': 'log',
    # 'dt_div_dt_cell_collapse': 'linear',
    # 'dt_div_max_tau_conv': {'yaxis_label':'None', 'scale_type':'log'},
    # 'min_t_eddy': {'yaxis_label':'None', 'scale_type':'log'},
    'num_retries': {'yaxis_label':'Number of retries', 'scale_type':'linear', 'divide_by':'None'},
    'num_iters': {'yaxis_label':'Number of Newton-Raphson iterations', 'scale_type':'linear', 'divide_by':'None'},
}

multi_line_plots_x_axis = {'plot_variable':'model_number', 'x_axis_divider':'None', 'x_axis_scale_type':'linear', 'x_axis_name_for_plots':'Model Number', 'x_axis_ticklabel_format': 'plain', 'y_axis_ticklabel_format': 'plain'}
multi_line_plots = {
    # 'plot1': 
    #     {'dynamic_timescale' : 'log', 'kh_timescale':'log', 'mdot_timescale':'log', 'nuc_timescale':'log'},
    # 'plot2': 
    #     {'total_mass_h1' : 'linear', 'total_mass_he4':'linear', 'total_mass_c12':'linear', 'total_mass_o16':'linear'},
}

only_plot_two_D_graphs = False

Kippenhahn_properties = {
            'x_axis' : 'model_number',
            'y_axis' : 'mass',
            'CPUs_for_multiprocessing' : 10, #options can be 'all' or a number
            'profile_read_interval' : 10,
            'model_number_start' : 'None',
            'model_number_end' : 'None',
            'overlay_history_line' : 'wd_surf_mass_coord', #'Bernoulli_0_const_mass', 
            'overlay_history_line_label' : 'WD Surface', #'B>0',
            'overlay_history_line_colour' : 'red',
            'y_axis_min_limit': 0.99999,
            'y_axis_max_limit': 'None',
            'x_axis_min_limit': 'None',
            'x_axis_max_limit': 'None' ,
            'format_x_axis' : 'None',
            'format_y_axis' : 'None',
            'x_axis_divide_by' : 'None', #'day'
            'y_axis_divide_by' : 'None',
            'z_axis_data_min_cutoff' : 'None', 
            'z_axis_data_min_cutoff_replacement' : 'None',
            'z_axis_data_max_cutoff' : 'None',
            'z_axis_data_max_cutoff_replacement' : 'None',
            'num_y_axis_elements_for_heatmap' : 2500,
            'levels_for_contourf_plot': 100,
            'colorbar_min_val': 'None',
            'colorbar_max_val': 'None',
            'x_axis_label_heatmap' : 'Model Number',
            'y_axis_label_heatmap' : 'Mass [M$_{\\odot}$]',
            'x_axis_ticklabel_format': 'plain',
            'y_axis_ticklabel_format': 'plain',
            'color_bar_scheme_heatmap' : 'viridis',
            }

Kippenhahn_plots = {
    # 'logT' : {'scale_type':'linear', 'colour_bar_name':' $\\mathrm{Log_{10} \\left( Temperature \\,/\\, K \\right)}$'},
    # 'logRho' : {'scale_type':'linear', 'colour_bar_name':'$\\mathrm{Log_{10} \\left(Density \\,/\\,g\\,cm^{-3} \\right)}$'},
    # 'logP' : {'scale_type':'linear', 'colour_bar_name':'$\\mathrm{Log_{10} \\left( Pressure \\,/\\, dyne \\, cm^{-2} \\right)}$'},
    # 'eps_nuc' : {'scale_type':'log', 'colour_bar_name':'$\\mathrm{Log_{10} \\left( \\epsilon_{nuc} \\,/\\, erg\\,g^{-1}\\,s^{-1} \\right)}$'},
    # 'opacity' : {'scale_type':'log', 'colour_bar_name':'$\\mathrm{Log_{10} \\left( Opacity \\,/\\, cm^{2}\\,g^{-1} \\right)}$'},

    # 'energy' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'entropy' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'radius' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'v' : {'scale_type':'symlog', 'colour_bar_name':'$\\mathrm{Log_{10} \\left( Velocity \\,/\\, cm\\,s^{-1} \\right)}$'},
    'c12' : {'scale_type':'log', 'colour_bar_name':'$\\mathrm{Log_{10} \\left( ^{12}C \\right)}$'},
    # 'inject_energy' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'log_v_escape' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'v_div_vesc' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'csound' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'v_div_cs': {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'luminosity' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'log_Ledd' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'log_L_div_Ledd' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'grada' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'gradT' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'gradr' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'conv_vel' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'log_D_mix' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'conv_L_div_L' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'dm' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'dr' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'pressure_scale_height' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'prad' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'pgas' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'pp' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'cno' : {'scale_type':'log', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'eta' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'x_mass_fraction_H' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'y_mass_fraction_He' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
    # 'z_mass_fraction_metals' : {'scale_type':'linear', 'colour_bar_name':'None'}, #'$\\mathrm{Log_{10} \\left(  \\,/\\,  \\right)}$'},
}

comparing_at_unity_plots = {
    # 'Bernoulli Parameter': {'plot_variables':['bernoulli_param'], 'colour_bar_name':'$\\mathrm{Log_{10} \\left( \\right)}$ Bernoulli parameter', 'set_title':False},
    '$\\mathrm{V \ / \  V_{esc}}$': {'plot_variables':['v_div_vesc'], 'colour_bar_name':'$v \ / \  v_{\\mathrm{esc}}$', 'set_title':False},
    '$\\mathrm{V \ / \  c_{sound}}$':  {'plot_variables':['v', 'csound'], 'colour_bar_name':'$v \ / \  C_{\\mathrm{sound}}$', 'set_title':False},
}

general_plot_routine(input_dir, output_dir, twoD_plots_output_pdf_name, threeD_plots_output_pdf_name,
    global_properties_x_axis, global_properties_plots, 
    multi_line_plots_x_axis, multi_line_plots,
    only_plot_two_D_graphs, 
    Kippenhahn_properties, Kippenhahn_plots, comparing_at_unity_plots)


'''
Instructions:

Set the data input directory (the LOGS file) to the 'input_dir' variable.

Set the output directory for the generated PDFs to the 'output_dir' variable.

Set the name of the generated PDF files for the 2- and 3-D plots to the 'twoD_plots_output_pdf_name' and 'threeD_plots_output_pdf_name' variables respectively.

The dictionaries 'global_properties_x_axis' and 'multi_line_plots_x_axis' set the X-axis arguments for the generated 2-D plots. The keys:
    'plot_variable' Sets the X-axis variable. e.g. star_age or model_number.
    'x_axis_divider' Allows the units of the X-axis to be manipulated. Options are: None, sec, min, hr, day, yr, kyr, myr, gyr. Example: for 'plot_variable':'star_age' setting 'x_axis_divider':'day' will change the X-axis units to days. Set to None if not in use.
    'x_axis_scale_type' Allows the scale of the X-axis to be set to linear or log.
    'x_axis_name_for_plots' Sets the X-axis label.
    'x_axis_ticklabel_format' Sets the style of the X-axis ticklabels. Can be 'plain' or 'sci' or 'scientific'.
    'y_axis_ticklabel_format' Sets the style of the X-axis ticklabels. Can be 'plain' or 'sci' or 'scientific'.

The dictionary 'global_properties_plots' takes the Y-axis variables to be plotted as keys and the corresponding Y-axis scale types as the key value. These plots are single line plots.

The dictionary 'multi_line_plots' creates plots with multiple lines for the purpose of comparison. This is a nested dictionary. The keys take the plot titles as their arguments.
The key values take nested dictionaries as their arguments. The nested dictionaries take the Y-axis variables to be plotted as keys, and the corresponding Y-axis scale types 
as their keys values. There are no limits on the number of key-value pairs that can be given within the nested dictionaries. Multi-line plots are then created with the nested dicts.
The same multi-line plot can have both linear- and log-scale variables plotted on the same axes if desired by the user.

The boolean 'only_plot_two_D_graphs' only plots 2-D graphs if set to true, and skips plotting the Kippenhahn graphs.

The dictionary 'Kippenhahn_properties' Takes the arguments for the generated Kippenhahn plots. The keys:
    'x_axis' Sets the X-axis variable. e.g. star_age or model_number.
    'y_axis' Sets the Y-axis variable. e.g. star_mass or radius.
    'overlay_history_line' Allows a line from history.data to be overlayed the heatmap. Set to the variable name in history.data,  e.g. 'mass_conv_core. Set to None if not in use.
    'overlay_history_line_label' Sets the legend label for the overlayed history line (if used). Set to None if not in use. 
    'overlay_history_line_colour' Sets the colour of the overlayed history line (if used). Set to None if not in use.
    'y_axis_min_limit': Sets the minimum Y-axis value to start from. e.g if Y-axis is mass, but you want to plot from mass value 0.9 Msun to the surface, set 'y_axis_min_limit': 0.9 
    'y_axis_max_limit': Sets the maximum Y-axis value to be plotted. e.g if Y-axis is mass, but you want to plot from the core to mass value 0.8 Msun, set 'y_axis_max_limit': 0.8 
    'format_x_axis' Formats the X-axis ticks to a certain number of decimal places. e.g. 'format_x_axis':3 allows ticks to have 3 decimal places. Set to None if not in use.
    'format_y_axis' Formats the Y-axis ticks to a certain number of decimal places. e.g. 'format_y_axis':7 allows ticks to have 7 decimal places. Set to None if not in use.
    'x_axis_divide_by' Allows the units of the X-axis to be manipulated. Options are: None, sec, min, hr, day, yr, kyr, myr, gyr. Example: for 'plot_variable':'star_age' setting 'x_axis_divider':'day' will change the X-axis units to days. Set to None if not in use.
    'y_axis_divide_by' Allows the units of the Y-axis to be manipulated. e.g. for 'Y_axis':'radius_cm' setting 'y_axis_divide_by':1e-2 will change the Y-axis units to metres. Set to None if not in use.
    'z_axis_data_min_cutoff' : If any of the Z-axis data elements are less than 'z_axis_data_min_cutoff', then those Z-axis data elements are set to 'z_axis_data_min_cutoff_replacement'. e.g if 'z_axis_data_min_cutoff':0e0 and 'z_axis_data_min_cutoff_replacement':0e0, then any Z-axis data elements below zero will be set to zero. Set to None if not in use.
    'z_axis_data_min_cutoff_replacement' : Sets the value a Z-axis element will be replaced with if its value is less than 'z_axis_data_min_cutoff'. Set to None if not in use.
    'z_axis_data_max_cutoff' If any of the Z-axis data elements are greater than 'z_axis_data_max_cutoff', then those Z-axis data elements are set to 'z_axis_data_max_cutoff_replacement'. e.g if 'z_axis_data_max_cutoff':1e2 and 'z_axis_data_min_cutoff_replacement':1e2, then any Z-axis data elements greater than 100 will be set to 100. Set to None if not in use.
    'z_axis_data_max_cutoff_replacement' Sets the value a Z-axis element will be replaced with if its value is greater than 'z_axis_data_max_cutoff'. Set to None if not in use.
    'num_y_axis_elements_for_heatmap' The number of data points the Y-axis will have on the heatmap. A greater number will give more resolution to the heatmap.
    'x_axis_label_heatmap' The label for the heatmaps x-axis. e.g. 'Star Age [day]'.
    'y_axis_label_heatmap' The label for the heatmaps Y-axis. e.g. 'Mass [M$_{\\odot}$]'.
    'x_axis_ticklabel_format' Sets the style of the X-axis ticklabels. Can be 'plain' or 'sci' or 'scientific'.
    'y_axis_ticklabel_format' Sets the style of the X-axis ticklabels. Can be 'plain' or 'sci' or 'scientific'.
    'color_bar_scheme_heatmap' Sets the colour scheme of the heatmaps colour bar . For matplotlib colours, go to https://matplotlib.org/stable/tutorials/colors/colormaps.html

The dictionary 'Kippenhahn_plots' takes the Z-axis variables to be plotted as the key and the Z-axis scale type as the corresponding key value. 

The dictionary comparing_at_unity_plots compares 2 Z-axis variables and plots the result as a 2 level Kippenhahn plot that explicitly shows the result of the comparison above and below unity.
The keys take the plot titles as their arguments. The key values take lists as their arguments. The list must contain either 1 or 2 variables to be compared against each other and plotted
to show the result above and below unity. For example if the key 'plot1' has a value ['v', 'csound'], then a Kippenhahn plot of cell_velocity/cell_sound_speed will be created that compares the result above 
and below unity. Note that the first list value will be divided by the second list value. A key can also take a list with a single element as a value, this is because some MESA outputs are 
in comparison form already, such as the profile.data output 'v_div_vesc' which is the direct output of cell_velocity/cell_escape_velocity by MESA. This particular example will take the key-value pair form
of 'plot2': ['v_div_vesc']. 
'''
